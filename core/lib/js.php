<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GPL v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


$files = [
    'func',
    'params', 'main' //! must be last (it's entry point for app)
];

$folder_js = RBS_JS_FLDR;// .'js';  //! DO NOT end with "/"
$folder_cache = RBS_JS_CACHE;
$fw_file = "{$folder_js}_fw.min.js";

$_interval = $_CONFIG['update_interval'] ?? 30;


// Cache settings
$settings = @file_get_contents(RBS_JS_CACHE_DATA) ?? '';
if (!$settings) $settings = ['file' => '', 'file_d' => ''];
else $settings = json_decode($settings, true);

$mtime = 0;
$js = '';

// Find last modified time
foreach ($files as $file) {
    $f = "{$folder_js}{$file}.js";
    $mt = filemtime($f) ?? 0;
    if ($mt > $mtime) $mtime = $mt;
    //$js .= file_get_contents($fn);
}
$md5 = md5($mtime);
$d_md5 = md5('$debug'. $mtime);
$name = "{$folder_cache}.{$md5}.min.js";
$name_d = "{$folder_cache}.{$d_md5}.min.js";


// Update minified Javascript cache if neccessary
if ($settings['file'] != $name || !file_exists($name)) {  // at leas one of JavaScript files has been changed
    if ($settings['file']) {
        unlink($settings['file']);
        unlink($settings['file_d']);
    }
    foreach ($files as $f) $js .= file_get_contents("{$folder_js}{$f}.js");

    // Make some replacements
    $js = str_replace('%%RBS_KEY%%', RBS_AJAX_KEY, $js);
    $js = str_replace('%%RBS_MIN_LESSON_TIME%%', (rbs_get_calendar_options()['bhStartTime'] ?? '08:00'), $js);
    $js = str_replace('%%RBS_ICON_EDIT%%', rbs_get_icon(['name' => 'pencil-square']), $js);
    $js = str_replace('%%RBS_ICON_REPEAT%%', rbs_get_icon(['name' => 'arrow-repeat']), $js);

    // $js_config = @file_get_contents("{$folder_js}config.js") ?? '';
    $y = date('Y');
    $js_config = "/*! 
 * Resource Booking System for Schools (https://bitbucket.org/jyri78/resource_booking_system)
 * Copyright (C) 2021-{$y} Jüri Kormik
 * Licensed under GPL v3 (https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE)
 */";
    $js_config .= "\nconst RBS_PATH = \"" . ($_CONFIG['root_url'] ?? '/') .'";';
    /* $fetch = @file_get_contents("{$folder_js}fetch.min.js") ?? '';
    $fetch .= 'const DEBUG_ALERT = '. ($_CONFIG['debug']['status'] ? 'true' : 'false') .';'; */

    $js_d = "\n" . trim($js_config) . /* "\n\n" . $fetch . */ "\n" . minimizeJavascript($js);
    $js = "\n" . trim($js_config) . /* "\n\n" . $fetch . */ "\n" . minimizeJavascript($js, true);
    $fw = @file_get_contents($fw_file);
    $settings['file'] = $name;
    $settings['file_d'] = $name_d;

    file_put_contents($name, $fw . $js);
    file_put_contents($name_d, $fw . $js_d);
    file_put_contents(RBS_JS_CACHE_DATA, json_encode($settings));
}

$js = file_get_contents( isset($_SESSION['$_debug']) ? $name_d : $name )
      . ($rbs_js_page == 'home' || $rbs_settings['user']['role'] == RBS_USER_ROLES[1] ? 
            minimizeJavascript((file_get_contents("{$folder_js}{$rbs_js_page}.js") ?? '')) : '');


// Finally output result
header('Content-Type: text/javascript; charset=UTF-8');
header("Cache-Control: no-cache");
header("Pragma: no-cache");
echo $js;


function minimizeJavascript($javascript, $remove_debug = false)
{
    if ($remove_debug) $javascript = preg_replace(['/F\.\$DEBUG/', '/\$d\s*\([\s\S]+?\);?/'], ['false', ''], $javascript);

    // Algne idee: https://datayze.com/howto/minify-javascript-with-php
    return preg_replace(
        ["#[^\:][\/]{2,}.*?\n#", "#/\*[\s\S]*?\*/#", '/\s+\n/', '/\n\s+/', '/ +/', '/[\s]*([=,><\|\&\(\)\}])[\ ]*/', '/[\s]*([\{;\?\:\+\-\/\*])[\s]*/', '/;}/', '/,}/', '/,\n/'],
        ['', '', "\n", "\n", ' ', '$1', '$1', '}', '}', ','],
        $javascript
    );
}