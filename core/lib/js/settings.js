/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


const lessonTimesClick = e => {
    let idx = e.target.value;

    if (e.target.checked) {
        F.$clr(`rbs_ls_${idx}`, 'opacity-25');
        F.$clr(`rbs_le_${idx}`, 'opacity-25');
        F.$clr(`rbs_sll_${idx}`, 'text-black-50');
        F.$(`rbs_lesson_begin_${idx}`).disabled = false;
        F.$(`rbs_lesson_end_${idx}`).disabled = false;
    }
    else {
        F.$cla(`rbs_ls_${idx}`, 'opacity-25');
        F.$cla(`rbs_le_${idx}`, 'opacity-25');
        F.$cla(`rbs_sll_${idx}`, 'text-black-50');
        F.$(`rbs_lesson_begin_${idx}`).disabled = true;
        F.$(`rbs_lesson_end_${idx}`).disabled = true;
    }
}

const saveSettings = (fd, so) => {
    F.$post(RBS_PATH, fd)
        .then(r => {
            if (r.debug) console.log(r.debug);
            if (r.error) errorAlert(`Salvestamise viga: ${r.error}`);
            else if (!so) window.location.reload(true);
            else successAlert(`Saidi ${so}seaded salvestatud.`);
        })
        .catch(e => console.error(`SETTINGS_SAVE_ERROR:  ${e}`));
}

const changeTheme = e => {
    let view_navbar = e.target.value;

    if (view_navbar != F.$('rbs_view_navbar_current').value)
        F.$post(RBS_PATH, {task: 'change_navbar_theme', view_navbar})
            .then(_ =>  window.location.reload(true))
            .catch(e => console.error(`CHANGE_THEME_ERROR:  ${e}`));
}

const changeIdsVisibility = e => {
    let table_show_ids = (e.target.checked ? '1' : '0');

    if (table_show_ids != F.$('rbs_table_show_ids_current').value)
        F.$post(RBS_PATH, {task: 'change_ids_visibility', table_show_ids})
            .then(_ =>  window.location.reload(true))
            .catch(e => console.error(`CHANGE_THEME_ERROR:  ${e}`));
}


const saveSettingsCalendarOptions = () => {
    let fd = getFormData('rbs_admin_settings_calendar_options', 'save_settings_calendar_options');
    let err = [];
    
    if (fd.error) errorAlert('Palun sisesta kõik algus- ja lõpuajad.');
    else {
        if (fd.rbs_bh_start_time < fd.rbs_slot_min_time) err.push('Lahtioleku algusaeg on seatud kalendrivaate algusajast varasemaks.');
        if (fd.rbs_bh_end_time > fd.rbs_slot_max_time) err.push('Lahtioleku lõpuaeg on seatud kalendrivaate lõpuajast hilisemaks.');
        if (fd.rbs_bh_start_time >= fd.rbs_bh_end_time) err.push('Lahtioleku algusaeg peab olema väiksem lõpuajast.');

        if (err.length) errorAlert(err.join('<br>'));
        else saveSettings(fd, 'üldised kalendri');
    }
}

const saveSettingsBookingOptions = () => {
    let fd = getFormData('rbs_admin_settings_booking_options', 'save_settings_booking_options');
    
    if (fd.error) errorAlert('Palun sisesta kõik väljad.');
    else saveSettings(fd, 'broneeringu ');
}

const saveSettingsLessonTimes = () => {
    let fd = getFormData('rbs_admin_settings_lesson_times', 'save_settings_lesson_times');
    
    if (fd.error) errorAlert('Palun sisesta kõik valitud algus- ja lõpuajad.');
    else {
        let min_time = (MIN_LESSON_TIME < F.$('rbs_bh_start_time').value ? F.$('rbs_bh_start_time').value : MIN_LESSON_TIME);
        let prev = min_time
        let err = [];
        let cnt = 0;

        for (let i = 0; i < 10; i++) {
            if (fd[`rbs_select_lesson_${i}`]) {
                if (fd[`rbs_lesson_begin_${i}`] < prev) err.push((prev == min_time
                    ? `Esimene tund (${i}.) ei saa alata enne kella ${min_time}.`
                    : `${i}. tund kattub eelmise tunniga.`));

                if (fd[`rbs_lesson_begin_${i}`] >= fd[`rbs_lesson_end_${i}`])
                    err.push(`${i}. tunni algusaeg on suurem või võrdne lõpuajaga.`);

                prev = fd[`rbs_lesson_end_${i}`];
                cnt++;
            }
        }

        if (!cnt) errorAlert('Ühtegi tundi ei ole seatud.');
        else if (err.length) errorAlert(err.join('<br>'));
        else saveSettings(fd, 'tundide aegade ');
    }
}

const getHeaderBrandStyle = () => {
    let fd = getFormData('rbs_admin_settings_header', 'get_header_brand_style');

    if (fd.error) errorAlert('Palun sisesta oma asutuse nimi.');
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                F.$sa('rbs_settings_header_brand_preview', 'class', r.style);
            })
            .catch(e => console.error(`GET_BRAND_STYLE_ERROR:  ${e}`));;
    }
}

const changeHeaderBrandText = e => {
    F.$('rbs_settings_header_brand_preview').innerHTML = e.target.value;
}

const saveSettingsHeaderBrand = () => {
    let fd = getFormData('rbs_admin_settings_header', 'save_settings_header_brand');
    
    if (fd.error) errorAlert('Palun sisesta kõik väljad.');
    else saveSettings(fd, '');
}


F.$ael(e => {
    [].slice.call( F.$q('[data-bs-toggle="tooltip"]', document, true) )
        .map(el => new bootstrap.Tooltip(el));

    for (let i = 0; i < 10; i++)
        F.$ael(lessonTimesClick, 'click', `rbs_select_lesson_${i}`);

    F.$ael(saveSettingsCalendarOptions, 'click', 'rbs_settings_calendar_options_save');
    F.$ael(saveSettingsBookingOptions, 'click', 'rbs_settings_booking_options_save');
    F.$ael(saveSettingsLessonTimes, 'click', 'rbs_settings_lesson_times_save');
    F.$ael(getHeaderBrandStyle, 'change', 'rbs_settings_header_brand_style');
    F.$ael(getHeaderBrandStyle, 'change', 'rbs_settings_header_brand_color');
    F.$ael(changeHeaderBrandText, 'keyup', 'rbs_settings_header_brand_institution_name');
    F.$ael(saveSettingsHeaderBrand, 'click', 'rbs_settings_header_save');
    F.$ael(changeTheme, 'click', 'rbs_view_navbar_light');
    F.$ael(changeTheme, 'click', 'rbs_view_navbar_dark');
    F.$ael(changeIdsVisibility, 'click', 'rbs_table_show_ids');
}, 'DOMContentLoaded');
