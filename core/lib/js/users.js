/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _rbs_usersIsDirty = false;


const getRoleBadge = () => {
    let roles = ['-', 'Admin', 'Kasutaja'];
    let id = F.$('rbs_role').value;
    let badge = F.$ce('span');
    F.$cla(badge, ['small', 'badge', 'rounded-pill', (id == 1 ? 'bg-danger' : 'bg-info')]);
    badge.append(roles[id]);
    return badge;
}

const addNewUserToTable = new_id => {
    let cols = ['rbs_firstname', 'rbs_lastname', 'rbs_username', 'rbs_email'];
    let row = F.$ce('tr');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );
    row.append( createColumn( getRoleBadge() ) );

    cols.forEach(id => {
        row.append( createColumn(F.$(id).value) );
        F.$(id).value = '';
    });

    row.append( createColumn('') );
    row.append( createColumn('') );
    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    _rbs_usersIsDirty = true;
    F.$('user_table_body').prepend(row);
    F.$('rbs_password').value = '';
    F.$('rbs_role').value = 2;
    F.$('rbs_firstname').focus();
}

const saveUser = () => {
    let fd = getFormData('user_add_form', 'save_user');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    if (!F.$('rbs_user_id')) addNewUserToTable(r.id);
                    else window.location.reload(true);
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`USER_SAVE_ERROR:  ${e}`));
    }
}

const openAddChangeUserModal = (uid = 0) => {
    let p = {task: 'open_add_change_user_modal'};
    let ids = ['rbs_firstname', 'rbs_lastname', 'rbs_username', 'rbs_email'];

    if (isNumeric(uid) && uid) p.user_id = uid;
    else ids.push('rbs_password');

    ids.push('rbs_role');

    openModalExt(p, () => {
        F.$ael(e => {
            if (e.key === 'Enter') {
                if (!F.$hda(e.target, 'next-input')) saveUser();
                else F.$$(e.target, 'next-input').focus();
            }
        }, 'keyup', ids);

        F.$('rbs_firstname').focus();
    }, () => {
        if (_rbs_usersIsDirty) window.location.reload(true);
    });
}


const deleteUser = id => {
    if (confirm(`Kas oled kindel, et soovid kasutajat "${F.$(`rbs_username_${id}`).innerHTML}" (ID: ${id}) kustutada?`)) {
        F.$post(RBS_PATH, {task: 'delete_user', rbs_user_id: id})
            .then(() => window.location.reload(true))
            .catch(e => console.error(`USER_DELETE_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$ael(openAddChangeUserModal, 'click', 'add_user_modal');

    F.$('rbs_user_ids').value.split(',').forEach(id => {
        let ch_pwd_btn = F.$(`rbs_user_password_${id}`);
        let del_btn = F.$(`rbs_user_delete_${id}`);

        F.$ael(() => openAddChangeUserModal(id), 'click', `rbs_user_edit_${id}`);
        if (ch_pwd_btn) F.$ael(() => openChangePasswordModal(id), 'click', ch_pwd_btn);
        if (del_btn) F.$ael(() => deleteUser(id), 'click', del_btn);
    });
}, 'DOMContentLoaded');
