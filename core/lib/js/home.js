/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


const createDate = d => {
    let i = d.getDate();
    let m = d.getMonth() + 1;

    // return `${d.getFullYear()}-${(m<10 ? '0' : '') + m}-${(i<10 ? '0' : '') + i}`;
    return `${(i<10 ? '0' : '') + i}.${(m<10 ? '0' : '') + m}.${d.getFullYear()}`;
}

const createTime = d => {
    let h = d.getHours();
    let m = d.getMinutes();

    return `${(h<10 ? '0' : '') + h}:${(m<10 ? '0' : '') + m}`;
}

const hour2Time = hr => {
    let h = ~~hr;
    let m = F.round((hr - h) * 60, 0);

    return `${(h<10 ? '0' : '') + h}:${(m<10 ? '0' : '') + m}`;
}

const selectUnselectAllDay = v => {
    if (v) F.$cla('rbs_select_booking_lessons', 'd-none');
    else F.$clr('rbs_select_booking_lessons', 'd-none');
}

const selectUnselectRecur = v => {
    if (v) {
        F.$clr('rbs_select_booking_recur_date', 'd-none');
        F.$clr('rbs_select_booking_recur_rate', 'd-none');
    }
    else {
        F.$cla('rbs_select_booking_recur_date', 'd-none');
        F.$cla('rbs_select_booking_recur_rate', 'd-none');
    }
}

const resourceSelected = e => {
    F.$post(RBS_PATH, {task: 'get_calendar_booked_times', date: F.$('rbs_booking_date').value, object_id: F.$('rbs_resource').value})
        .then(r => {
            let all_day_chk = F.$('rbs_booking_all_day');

            r.lessons.forEach(lesson => {  //TODO: needs extra checking (for example if selected all day and vice versa)
                let l_nr = lesson.substring(0,1);

                if (r.booked_lessons.includes(lesson)) {
                    let chk = F.$(`rbs_select_booking_lesson_${l_nr}`);
                    chk.checked = false;
                    chk.disabled = true;
                }
                else F.$(`rbs_select_booking_lesson_${l_nr}`).disabled = false;
            });
            all_day_chk.disabled = !!r.booked_lessons.length;
            if (all_day_chk.disabled) selectUnselectAllDay( all_day_chk.checked = false );
        })
        .catch(e => console.error(`GET_BOOKED_TIMES_ERROR:  ${e}`));
}

const allDevicesClicked = e => {
    let dev_cnt = F.$('rbs_devices_count');

    if (e.target.checked) dev_cnt.selectedIndex = 0;
    dev_cnt.disabled = e.target.checked;
}

const modalBookingOpened = () => {
    let all_devices = F.$('rbs_all_devices');

    F.$ael(e => selectUnselectAllDay(e.target.checked), 'change', 'rbs_booking_all_day');
    F.$ael(e => selectUnselectRecur(e.target.checked), 'change', 'rbs_booking_recur');
    if (F.$gda('rbs_resource', 'add-listener') == '1') F.$ael(resourceSelected, 'change', 'rbs_resource');
    if (all_devices) F.$ael(allDevicesClicked, 'click', all_devices);
    F.$('rbs_booking_title').focus();
}

const postBookingResult = (r, alert = true) => {
    if (r.result) window.location.reload(true);
    else {
        if (r.debug) console.log(r.debug);
        if (alert) errorAlert(r.error);
        else setErrorMessage(r.error);
    }
}


const calendarFilterSelectResourceType = e => {
    // console.log(e.target.value);
    F.$post(RBS_PATH, {'task': 'calendar_filter_get_objects', 'type_id': e.target.value})
        .then(objects => {
            let sel = F.$('rbs_resource');

            sel.innerHTML = objects;

            if (!objects || objects.trim() == '') {
                sel.value = '';
                sel.disabled = true;
                F.$('rbs_filter_button').disabled = true;
            }
            else {
                sel.disabled = false;
                F.$('rbs_filter_button').disabled = false;
            }
        })
        .catch(e => console.error(`CALENDAR_FILTER_CHANGE_TYPE_ERROR:  ${e}`));
}


const calendarFilter = () => {
    let fd = getFormData('calendar_filter_form', 'calendar_filter');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(() => window.location.reload(true))
            .catch(e => console.error(`CALENDAR_FILTER_ERROR:  ${e}`));
    }
}


const rbsFilterCalendarClick = () => {
    openModal('open_calendar_filter_modal', () => {
        F.$ael(calendarFilterSelectResourceType, 'change', 'rbs_resource_type');
    });
}

const rbsSelect = i => {
    // console.log(i);return;
    if (i.start < Date.now()) {
        cal.unselect();
        errorAlert('Ajas tagasi broneeringut teha ei saa!');
    }
    else {
        let data = {
            'task': 'open_calendar_booking_modal',
            'date': createDate(i.start),
            'start': createTime(i.start), 'end': createTime(i.end),
            'all_day': i.allDay
        }
        openModalExt(data, modalBookingOpened);
    }
}

const rbsEvtClick = i => {
    // console.log(i);return;
    if (i.event.extendedProps.canChange) {
        let data = {
            'task': 'open_calendar_change_modal',
            'booking_id': i.event.extendedProps.bookingID,
            'date': createDate(i.event.start)
        }
        openModalExt(data, modalBookingOpened);
    }
}

const rbsEvtSrcFailure = () => {
    errorAlert('Kalendri broneeringute info laadimine ebaõnnestus.');
}

const rbsViewDidMount = arg => {
    let begin_hr, end_hr, top_loc, bottom_loc, axis_cell;
    let ms2hrs = 1000 * 60 * 60;
    // let cal = arg.view.calendar;
    let mintime_ms = cal.currentData.dateProfile.slotMinTime.milliseconds;
    let maxtime_ms = cal.currentData.dateProfile.slotMaxTime.milliseconds;
    let total_hrs = (maxtime_ms - mintime_ms) / ms2hrs;
    let min_hr = mintime_ms / ms2hrs;
    let lessons = cal.getOption('lessons');
    let axis_col = F.$q('div.fc-timegrid-col-frame', F.$q('td.fc-timegrid-axis', F.$q('div.fc-timegrid-cols', F.$('calendar'), false), false), false);
    // let col_height = F.$q('div.fc-timegrid-slots', F.$c('fc-timegrid-body', F.$('calendar'), false), false).clientHeight;
    let col_height = axis_col.offsetHeight + (total_hrs < 13 ? 10 : -2);  // correction found by testing
    //console.log(total_hrs);                                               //TODO: find better solution

    lessons.forEach(l => {
        begin_hr = l.begin - min_hr;
        end_hr = l.end - min_hr;
        top_loc = Math.floor(col_height / total_hrs * begin_hr);  // raamid juurde
        bottom_loc = Math.floor(col_height / total_hrs * end_hr);

        axis_cell = F.$ce('div');
        F.$cla(axis_cell, ['fc-timegrid-col-events', 'border', 'border-info', 'rounded', 'align-middle']);
        axis_cell.style.top = `${top_loc}px`;
        axis_cell.style.left = '3px';  /// alumisel real oli algselt veel klass 'fc-timegrid-event-harness'
        axis_cell.style.zIndex = 3;
        axis_cell.innerHTML = `<div class="bg-info bg-gradient bg-opacity-25 pt-2 fw-bold user-select-none"
            style="width:52px;height:${bottom_loc - top_loc}px" data-bs-toggle="tooltip" data-bs-placement="left"
            title="${hour2Time(l.begin)} - ${hour2Time(l.end)}">&nbsp;${l.title}&nbsp;t</div>`;

        axis_col.append(axis_cell);
    });
    [].slice.call(F.$q('[data-bs-toggle="tooltip"]', document, true)).map(el => new bootstrap.Tooltip(el));
}

const rbsEvtDidMount = arg => {
    // console.log(arg);
    let canChange = arg.event.extendedProps.canChange;
    let icon = (canChange ? CALENDAR_EDIT_ICON : '');
    if (arg.event.extendedProps.repeating) icon += CALENDAR_REPEAT_ICON;
    let title = (canChange ? 'success ' : 'secondary ');

    let devCount = (arg.event.extendedProps.devicesCount ?
            `&nbsp;&nbsp;<small class="badge bg-info text-dark rounded-pill">${arg.event.extendedProps.devicesCount}</small>` : '');

    arg.el.innerHTML = `<div class="fc-event-main"><div class="fc-event-main-frame"${canChange ? ' style="cursor:pointer"' : ''}>
        <!-- div class="fc-event-time">...</div -->
        <div class="fc-event-title-container"><div class="fc-event-title fc-sticky user-select-none">
            <div class="bg-${title} bg-gradient bg-opacity-75 border border-${title} rounded shadow-sm py-0 px-1 fw-bold">${arg.el.innerText}${devCount}${icon}</div>
            ${ arg.event.extendedProps.userName ? `<div class="px-1">${arg.event.extendedProps.userName}</div>` : '' }
        </div></div></div></div>`;
}

const saveBooking = () => {
    let fd = getFormData('calendar_booking_form', 'save_calendar_booking');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => postBookingResult(r))
            .catch(e => console.error(`SAVE_BOOKING_ERROR:  ${e}`));
    }
}

const deleteBooking = () => {
    if (confirm(`Kas oled kindel, et soovid selle broneeringu kustutada?`)) {
        let task = 'delete_calendar_booking';
        let booking_id = F.$('rbs_booking_id').value;

        F.$post(RBS_PATH, {task, booking_id})
            .then(r => postBookingResult(r))
            .catch(e => console.error(`DELETE_BOOKING_ERROR:  ${e}`));
    }
}

const logIn = () => {
    let fd = getFormData('login_form', 'user_login');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH,fd)
            .then(r => postBookingResult(r, false))
            .catch(e => console.error(`LOGIN_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$post(RBS_PATH, {'task':'get_calendar_options'})
        .then(r => {
            if (r.error) {
                errorAlert(r.error);
                return;
            }
            if (r.settings.logged_in) r.options.eventClick = rbsEvtClick;
            if (r.options.selectable) r.options.select = rbsSelect;
            if (r.options.headerToolbar.left) r.options.customButtons.filterCalendar.click = rbsFilterCalendarClick;

            r.options.viewDidMount = rbsViewDidMount;
            r.options.eventDidMount = rbsEvtDidMount;
            r.options.eventSources[0].failure = rbsEvtSrcFailure;
            // console.log(r.options);

            /*let*/ cal = new FullCalendar.Calendar(F.$('calendar'), r.options);
            if (r.settings.goto_date) cal.gotoDate(r.settings.goto_date);
            cal.render();
        })
        .catch(e => console.error(`GET_CAL_OPTIONS_ERROR:  ${e}`));
}, 'DOMContentLoaded');
