/* 
    Copyright (C) 2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _rbs_manufacturersIsDirty = false;


const addNewManufacturerToTable = new_id => {
    let row = F.$ce('tr');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );

    row.append( createColumn( F.$('rbs_manufacturer_name').value ) );
    row.append( createColumn('0') );
    row.append( createColumn('') );
    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    _rbs_manufacturersIsDirty = true;
    F.$('manufacturers_table_body').prepend(row);
    F.$('rbs_manufacturer_name').value = '';
    F.$('rbs_manufacturer_name').focus();
}

const saveManufacturer = () => {
    let fd = getFormData('manufacturer_add_form', 'save_manufacturer');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    if (!F.$('rbs_manufacturer_id')) addNewManufacturerToTable(r.id);
                    else window.location.reload(true);
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`MANUFACTURER_SAVE_ERROR:  ${e}`));
    }
}


const openAddChangeManufacturerModal = (mid = 0) => {
    let p = {task: 'open_add_change_manufacturer_modal'};

    if (isNumeric(mid) && mid) p.manufacturer_id = mid;

    openModalExt(p, () => {
        F.$ael(e => {
            if (e.key === 'Enter') saveManufacturer();
        }, 'keyup', 'rbs_manufacturer_name');
        
        F.$('rbs_manufacturer_name').focus();
    }, () => {
        if (_rbs_manufacturersIsDirty) window.location.reload(true);
    });
}


const deleteManufacturer = mid => {
    if (confirm(`Kas oled kindel, et soovid tootjat "${F.$(`rbs_manufacturer_name_${mid}`).innerHTML}" (ID: ${mid}) kustutada?`)) {
        F.$post(RBS_PATH, {task: 'delete_manufacturer', rbs_manufacturer_id: mid})
            .then(() => window.location.reload(true))
            .catch(e => console.error(`MANUFACTURER_DELETE_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$ael(openAddChangeManufacturerModal, 'click', 'add_manufacturer_modal');

    F.$('rbs_manufacturer_ids').value.split(',').forEach(id => {
        if (!id) return;
        let del_btn = F.$(`rbs_manufacturer_delete_${id}`);

        F.$ael(() => openAddChangeManufacturerModal(id), 'click', `rbs_manufacturer_edit_${id}`);
        if (del_btn) F.$ael(() => deleteManufacturer(id), 'click', del_btn);
    });
}, 'DOMContentLoaded');
