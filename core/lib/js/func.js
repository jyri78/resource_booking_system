/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


/**
 * Class with some helper functions
 */
class F
{
    /*
        Static methods:
        ----------------------------------------------------------------
        $(id)        - getElementById
        $$(elem, n)  - getElementByIdExt  (reads ID from elements data attribute (without prefix));
                        NOTE: `elem` is HTMLElement or string of elements ID

        $c(cls, [elem], [all])  - getElementsByClassName;   DEFAULT:  elem = document,  all = true
        $t(tag, [all])          - getElementsByTagName;     DEFAULT:  all = true
        $q(sel, [elem], [all])  - querySelector;            DEFAULT:  elem = document,  all = false

        $ga(elem, name)         - getAttribute;
        $sa(elem, name, value)  - setAttribute;
        $ra(elem, name)         - removeAttribute;

        $gda(elem, name)  - getDataAttribute;    NOTE: `name` without prefix (will be added by the code)
        $hda(elem, name)  - hasDataAttribute

        $ael(func, [evt], [elem])  - addEventListener;     DEFAULT:  evt = 'load',  elem = window;    NOTE: `elem` can also be an array of ID-s
        $rel(func, [evt], [elem])  - removeEventListener;  DEFAULT:  evt = 'load',  elem = window;    NOTE: `elem` can also be an array of ID-s

        $aels(func, evt, [ids])   - addEventListeners;     DEFAULT:  ids = []
        $rels(func, evt, [ids])   - removeEventListeners;  DEFAULT:  ids = []

        $st(func, sec, [params])   - setTimeout;   DEFAULT:  params = []
        $si(func, sec, [params])   - setInterval;  DEFAULT:  params = []
        $ct(id)                    - clearTimeout
        $ci(id)                    - clearInterval

        $ce([tag])                - createElement;  DEFAULT:  tag = 'div'
        $cla(elem, cls)           - addClass;       NOTE:  `cls` can also be an array of tokens
        $clr(elem, cls)           - removeClass;    NOTE:  `cls` can also be an array of tokens
        $clp(elem, o_cls, n_cls)  - replaceClass
        $clt(elem, cls)           - toggleClass

        $ws([onopen], [onmessage], [onerror], [onclose])  - WebSocket connection;  DEFAULT:  <ALL> = null
        async $post(url, [data])                          - post data to server;   DEFAULT:  data = {}

        rad([deg])              - degToRad;  DEFAULT:  deg = 0
        deg([rad])              - radToDeg;  DEFAULT:  rad = 0
        round(num, [dec_point])              DEFAULT:  dec_point = 2
    */


    constructor() { }

    /**
     * getElementById
     *
     * @param   {string}  id
     *
     * @return  {HTMLElement}
     */
    static $(id)
    {
        return this.$q(`#${id}`);
    }

    /**
     * getElementByIdExt (get ID from elements data attribute)
     *
     * @param   {HTMLElement|string}  el
     * @param   {string}  n
     *
     * @return  {HTMLElement}
     */
    static $$(el, n)
    {
        return this.$q(`#${F.$gda(el, n)}`);
    }

    /**
     * getElementsByClassName
     *
     * @param   {string}              cl  classname
     * @param   {HTMLElement|string}  el  element or ID
     * @param   {bool}                a   select all or not
     *
     * @return  {HTMLCollection}
     */
    static $c(cl, el = document, a = true)
    {
        if (typeof el === 'string') el = F.$(el);
        return this.$q(`.${cl}`, el, a);
    }

    /**
     * getElementsByTagName
     *
     * @param   {string}  t
     * @param   {bool}    a  select all or not
     *
     * @return  {HTMLCollection}
     */
    static $t(t, a = true)
    {
        return this.$q(t, document, a);
    }

    /**
     * querySelector
     *
     * @param   {string}              s   query
     * @param   {HTMLElement|string}  el  element or ID
     * @param   {bool}                a   select all or not
     *
     * @return  {HTMLCollection|HTMLElement}   HTMLCollection if all = true, HTMLElement otherwise
     */
    static $q(s, el = document, a = false)
    {
        if (typeof el === 'string') el = F.$(el);
        if (a) return el.querySelectorAll(s);
        return el.querySelector(s);
    }

    /**
     * getAttribute
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              n   Attribute name
     * 
     * @return  {string}
     */
    static $ga(el, n)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return '';
        return (el.hasAttribute(n) ? el.getAttribute(n) : '');
    }

    /**
     * setAttribute
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              n   Attribute name
     * @param   {string|bool}         v   Attribute value
     */
    static $sa(el, n, v)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.setAttribute(n, v);
    }

    /**
     * removeAttribute
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              n   Attribute name
     */
    static $ra(el, n)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.removeAttribute(n);
    }

    /**
     * hasDataAttribute
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              n   Data attribute name (without prefix)
     * 
     * @return  {bool}
     */
    static $hda(el, n)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return false;
        return el.hasAttribute(`data-rbs-${n}`);
    }

    /**
     * getDataAttribute
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              n   Data attribute name (without prefix)
     * 
     * @return  {string}
     */
    static $gda(el, n)
    {
        return (F.$hda(el, n) ? F.$ga(el, `data-rbs-${n}`) : '');
    }

    /**
     * addEventListener
     *
     * @param   {Function}                  func  function to call
     * @param   {string}                    evt   event
     * @param   {HTMLElement|string|array}  el    element or ID
     */
    static $ael(func, evt = 'load', el = window)
    {
        if (el.constructor === Array) return F.$aels(func, evt, el);
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.addEventListener(evt, func);
    }

    /**
     * removeEventListener
     *
     * @param   {Function}     func  function to call
     * @param   {string}       evt   event
     * @param   {HTMLElement}  el    element or ID
     */
    static $rel(func, evt = 'load', el = window)
    {
        if (el.constructor === Array) return F.$rels(func, evt, el);
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.removeEventListener(evt, func);
    }

    /**
     * addEventListeners
     *
     * @param   {Function}  func  function to call
     * @param   {string}    evt   event
     * @param   {array}     el    ID-s
     */
    static $aels(func, evt, els = [])
    {
        els.forEach(el => {
            F.$ael(func, evt, el);
        });
    }

    /**
     * removeEventListeners
     *
     * @param   {Function}     func  function to call
     * @param   {string}       evt   event
     * @param   {array}        el    ID-s
     */
    static $rels(func, evt, els = [])
    {
        els.forEach(el => {
            F.$rel(func, evt, el);
        });
    }
 
    /**
     * setTimeout
     *
     * @param   {Function}  func
     * @param   {string}    sec     time in seconds
     * @param   {array}     params  function parameters 
     */
    static $st(func, sec, params = [])
    {
        if (!params.length) return window.setTimeout(func, sec*1000);
        else return window.setTimeout(func, sec*1000, ...params);
    }

    /**
     * setInterval
     *
     * @param   {Function}  func
     * @param   {string}    sec     time in seconds
     * @param   {array}     params  function parameters 
     */
    static $si(func, sec, params = [])
    {
        if (!params.length) return window.setInterval(func, sec*1000);
        else return window.setInterval(func, sec*1000, ...params);
    }

    /**
     * clearTimeout
     *
     * @param   {number}  id  ID from setTimeout()
     */
    static $ct(id)
    {
        window.clearTimeout(id);
    }

    /**
     * clearInterval
     *
     * @param   {number}  id  ID from setInterval()
     */
    static $ci(id)
    {
        window.clearInterval(id);
    }

    /**
     * createElement
     *
     * @param   {string}  el  Element to create
     */
    static $ce(el = 'div')
    {
        return document.createElement(el);
    }

    /**
     * addClass
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string|array}        cl  Classname to add
     */
    static $cla(el, cl)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        if (cl.constructor === Array) el.classList.add(...cl);
        else el.classList.add(cl);
    }

    /**
     * removeClass
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string|array}        cl  Classname to remove
     */
    static $clr(el, cl)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        if (cl.constructor === Array) el.classList.remove(...cl);
        else el.classList.remove(cl);
    }

    /**
     * replaceClass
     *
     * @param   {HTMLElement|string}  el   Element or ID
     * @param   {string}              ocl  Classname to replace
     * @param   {string}              ocl  New classname to add
     */
    static $clp(el, ocl, ncl)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.classList.replace(ocl, ncl);
    }
 
    /**
     * toggleClass
     *
     * @param   {HTMLElement|string}  el  Element or ID
     * @param   {string}              cl  Classname to add
     */
    static $clt(el, cl)
    {
        if (typeof el === 'string') el = F.$(el);
        if (!el) return;
        el.classList.toggle(cl);
    }

    /**
     * WebSocket
     *
     * @param   {Function}  onopen
     * @param   {Function}  onmessage
     * @param   {Function}  onerror
     * @param   {Function}  onclose
     *
     * @return  {WebSocket}
     */
    static $ws(onopen = null, onmessage = null, onerror = null, onclose = null)
    {
        if (!onopen && !onmessage && !onerror && !onclose) return null;
        const ws = new WebSocket(WEB_SOCKET_ADDR);
        if (onopen instanceof Function) ws.onopen = onopen;
        if (onmessage instanceof Function) ws.onmessage = onmessage;
        if (onerror instanceof Function) ws.onerror = onerror;
        if (onclose instanceof Function) ws.onclose = onclose;
        return ws;
    }

    /**
     * AJAX post
     *
     * @param   {String}  url
     * @param   {JSON}    data
     *
     * @return  {JSON|String}
     */
    static async $post(url, data = {})
    {
        let b = POST_EXTRA_KEY;  //! POST-päringu tuvastamiseks
        let hasImg = false;
        let img;

        for (var k in data) {
            if (data[k] instanceof File) {
                if (data[k].name && data[k].size > 12) {
                    hasImg = true;
                    img = this._i(k, data[k]);
                }
            }
            else b += '&' + k + '=' + data[k];
        }

        if (hasImg) {
            return new Promise(resolve => {
                this._p(url, img, true)
                    .then(r => {
                        if (r.result) {
                            b += `&rbs_image_file=${r.img.file}`;
                            b += `&rbs_image_filename=${r.img.filename}`;
                            b += `&rbs_image_type=${r.img.type}`;

                            return resolve(F._p(url, b));
                        }
                        else return resolve(r);
                    })
                    .catch(e => resolve({result:false, error:e}));
            });
        }
        else return this._p(url, b);
    }

    /**
     * Convert degree to radian
     *
     * @param   {Number}  deg
     */
    static rad(deg = 0)
    {
        return deg * Math.PI / 180;
    }

    /**
     * Convert radian to degree
     *
     * @param   {Number}  rad
     */
    static deg(rad = 0)
    {
        return rad * 180 / Math.PI;
    }

    /**
     * Round to decimal point
     *
     * @param   {Number}  n   number
     * @param   {Number}  db  detsimal point
     */
    static round(n, dp = 2)
    {
        let m = Math.pow(10, dp);
        return Math.round(n*m)/m;
    }


    /*  -----------------------------------------------------------------------
        "Private" helper functions
     */

    static _i(k, d)
    {
        let _pek = POST_EXTRA_KEY.split('=');
        let i = new FormData();

        i.append(_pek[0], _pek[1]);
        i.append('task', 'save_image');
        i.append(k, d);

        return i;
    }

    static async _p(u, b, f = false)
    {
        let r;
        let i = {
            method:'POST', cache:'no-cache',
            mode:'cors', credentials:'same-origin',
            body: b
        };
        if (!f) i.headers = {'Content-Type':'application/x-www-form-urlencoded'};

        const response = await fetch(u, i);
        const ct = response.headers.get('content-type');

        if (response.status == 200) {
            if (!ct || !ct.includes('application/json')) r = response.text();
            else r = response.json();
        }
        else r = {result: false, error: `  ${response.status} ${response.statusText}`};

        return r;
    }
}
