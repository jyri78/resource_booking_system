/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _uploadInProgress = false;
let _rbs_deviceObjectsIsDirty;
let _deviceIds;
let _deviceCurIdx;
let _deviceChanged;
let _deviceNavInAction;
let _rbs_devicesIsDirty;



const updateButtonStatus = () => {
    if (
        !F.$('rbs_device_type').disabled &&
        !F.$('rbs_device_manufacturer').disabled &&
        !F.$('rbs_device_model').disabled
    ) {
        let btn = F.$('rbs_save_button');

        F.$ra(btn, 'disabled');
        F.$clr(btn, 'opacity-25')
    }
}

const optionNameById = id => {
    let select = F.$(id);
    let val = select.value;

    for (let i = 0; i < select.length; i++) {
        if (select[i].value == val) return select[i].text;
    }
}

const addNewDeviceToTable = new_id => {
    let cols = ['rbs_device_type', 'rbs_device_manufacturer', 'rbs_device_model'];
    let row = F.$ce('tr');
    let img = F.$('rbs_image');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );
    row.append( createColumn(F.$('rbs_device_name').value) );
    row.append( createColumn('') );

    cols.forEach(id => {
        row.append( createColumn( optionNameById(id) ) );
        F.$(id).value = F.$(id)[0].value;
    });

    row.append( createColumn(F.$('rbs_devices_count').value) );
    row.append( createColumn(optionNameById('rbs_device_location')) );
    row.append( createColumn(F.$('rbs_info').value) );

    F.$('rbs_device_name').value = '';
    F.$('rbs_devices_count').value = 1;
    F.$('rbs_device_location').value = F.$('rbs_device_location')[0].value;
    F.$('rbs_info').value = '';
    try { img.value = null; } catch(ex) { }
    if (img.value) { img.parentNode.replaceChild(img.cloneNode(true), img); }
    _rbs_deviceObjectsIsDirty = true;

    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    F.$('device_table_body').children[0].parentElement.prepend(row);
    F.$('rbs_device_name').focus();
}

const saveDeviceObject = () => {
    let fd = getFormData('device_add_form', 'save_device_object');

    if (fd.error) setErrorMessage(fd.error);
    else {
        _uploadInProgress = true;
        F.$('rbs_save_button').blur();
        F.$clr('rbs_loader', 'd-none');

        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    if (!F.$('rbs_device_object_id')) {
                        addNewDeviceToTable(r.id);

                        F.$cla('rbs_loader', 'd-none');
                        _uploadInProgress = false;
                    }
                    else window.location.reload(true);
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`DEVICE_OBJECT_SAVE_ERROR:  ${e}`));
    }
}

const openAddChangeDeviceObjectModal = (did = 0) => {
    let ids = [
        [
            'rbs_device_name', 'rbs_device_type', 'rbs_device_manufacturer', 'rbs_device_model',
            'rbs_device_location', 'rbs_devices_count', 'rbs_info', 'rbs_image'
        ],
        ['rbs_device_name', 'rbs_device_location', 'rbs_devices_count', 'rbs_info', 'rbs_image']
    ];
    let p = {task: 'open_add_change_device_object_modal'};

    if (isNumeric(did) && did) p.device_object_id = did;

    openModalExt(p, () => {
        _rbs_deviceObjectsIsDirty = false;

        F.$ael(e => {
            if (e.key === 'Enter') {
                if (!F.$hda(e.target, 'next-input')) saveDeviceObject();
                else {
                    let n_el = F.$$(e.target, 'next-input');

                    if (!n_el || n_el.disabled)
                        F.$(`${F.$gda(e.target, 'next-input')}_add`).focus();
                    else F.$$(e.target, 'next-input').focus();
                }
            }
        }, 'keyup', (!F.$('rbs_device_object_id') ? ids[0] : ids[1]));

        F.$ael(showAddObject, 'click', Array.from(F.$q('.rbs_add', 'device_add_form', true)));
        F.$ael(e => {
            if (_uploadInProgress) {
                e.preventDefault();
                // e.stopPropagation();
                return;
            }
            if (e.key === 'Enter') addObject();
        }, 'keyup', 'rbs__new_object');

        F.$('rbs_device_name').focus();
    }, () => {
        if (_rbs_deviceObjectsIsDirty) window.location.reload(true);
    });
}


const disableButtons = () => {
    [
        'rbs_name', 'rbs_asset_code', 'rbs_serial_number', 'rbs_is_broken', 'rbs_info',
        'rbs_previous_button', 'rbs_next_button', 'rbs_save_button', 'rbs_delete_button'
    ].forEach(id => { F.$(id).disabled = true; });
}
const enableButtons = () => {
    [
        'rbs_name', 'rbs_asset_code', 'rbs_serial_number', 'rbs_is_broken', 'rbs_info',
        'rbs_previous_button', 'rbs_next_button', 'rbs_save_button'
    ].forEach(id => { F.$(id).disabled = false; });
}

const getDeviceId = () => +_deviceIds[_deviceCurIdx];
const getDeviceNextId = () => +(_deviceIds[ _deviceCurIdx + 1 ] ?? 0);

const updateDeleteButton = () => {
    F.$('rbs_delete_button').disabled = _deviceCurIdx == _deviceIds.length - 1;
}

const updateDeviceCountData = () => {
    let _cnt = _deviceIds.length - 1;

    F.$('rbs_active_device').innerHTML = (_deviceCurIdx == _cnt ? '–' : _deviceCurIdx + 1);
    F.$('rbs_cnt_devices').innerHTML = _cnt;
}

// So called private function not for usage from other places
const _navigate = (next_prev) =>
{
    let device_id = F.$('rbs_device_id');

    device_id.value = getDeviceId();
    _deviceCurIdx += next_prev;

    if (_deviceCurIdx == _deviceIds.length) _deviceCurIdx = 0;
    if (_deviceCurIdx == -1) _deviceCurIdx = _deviceIds.length - 1;
    updateDeleteButton();

    if (_deviceChanged) {
        _deviceNavInAction = true;
        saveDevice( getDeviceId() );
    }
    else {
        if (_deviceCurIdx == _deviceIds.length - 1) resetDeviceInputs();
        else loadDeviceData();
    }
}
const prevDevice = () => { _navigate(-1); }
const nextDevice = () => { _navigate(1); }

const resetDeviceInputs = () => {
    [
        'rbs_name', 'rbs_asset_code', 'rbs_serial_number', 'rbs_info'
    ].forEach(id => { F.$(id).value = '' });

    updateDeviceCountData();
    F.$('rbs_is_broken').checked = false;
    F.$('rbs_name').focus();
}

const setDeviceInputs = values => {
    if (!values.name) return;

    [
        'name', 'asset_code', 'serial_number', 'info'
    ].forEach(id => { F.$(`rbs_${id}`).value = values[id] });

    if (values['is_broken']) F.$('rbs_is_broken').checked = true;
    updateDeviceCountData();
    F.$('rbs_name').focus();
}

const saveDevice = (next_prev_id = 0) => {
    let fd = getFormData('devices_edit_form', 'save_device');

    if (fd.error) setErrorMessage(fd.error);
    else {
        disableButtons();
        if (next_prev_id > 0) fd['next_prev_device_id'] = next_prev_id;

        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    let _data = {name: ''};

                    if (!r.id.new_id) {
                        if (_deviceNavInAction) resetDeviceInputs();
                    }
                    else {
                        _data = r.id.device_data;
                        r.id = r.id.new_id;
                    }

                    if (!_deviceIds.includes(r.id)) {
                        _deviceIds.addId(r.id);
                        F.$('rbs_cnt_devices').innerHTML = _deviceIds.length - 1;
                    }

                    _deviceChanged = false;
                    _deviceNavInAction = false;
                    _rbs_devicesIsDirty = true;
                    setDeviceInputs(_data);
                    enableButtons();
                    updateDeleteButton();
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`DEVICE_SAVE_ERROR:  ${e}`));
    }
}

const loadDeviceData = () => {
    let task = 'get_device_data';
    let device_id = getDeviceId();

    F.$post(RBS_PATH, {task, device_id})
        .then(r => {
            if (r.debug) console.log(r.debug);
            if (r.result) setDeviceInputs(r.data);
            else setErrorMessage(r.error);
        })
        .catch(e => console.error(`LOAD_DEVICE_ERROR:  ${e}`));
}

const deleteDevice = () => {
    let fd = {task: 'delete_device', rbs_device_id: getDeviceId()};
    let next_device_id = getDeviceNextId();

    if (next_device_id > 0) fd.rbs_next_device_id = next_device_id;

    F.$post(RBS_PATH, fd)
        .then(r => {
            if (r.debug) console.log(r.debug);
            if (r.result) {
                _deviceIds.splice(_deviceCurIdx, 1);
                console.log(r);

                if (!r.id.new_id) resetDeviceInputs();
                else setDeviceInputs(r.id.device_data);

                _rbs_devicesIsDirty = true;
            }
            else setErrorMessage(r.error);
        })
        .catch(e => console.error(`LOAD_DEVICE_ERROR:  ${e}`));
}

const _checkKey = key => {
    if (key === 'ArrowRight') nextDevice();
    else if (key === 'ArrowLeft') prevDevice();
    else if (key === 'Delete') deleteDevice();
}
const openAddChangeDeviceInGroupModal = did => {
    let ids1 = ['rbs_name', 'rbs_asset_code', 'rbs_serial_number'];
    let ids2 = ['rbs_name', 'rbs_asset_code', 'rbs_serial_number', 'rbs_info', 'rbs_is_broken']
    let p = {task: 'open_add_change_device_in_group_modal', device_object_id: did};

    openModalExt(p, () => {
        _deviceIds = F.$('rbs_device_ids_list').value.split(',');
        _deviceCurIdx = 0;
        _deviceChanged = false;
        _deviceNavInAction = false;
        _rbs_devicesIsDirty = false;

        F.$ael(e => {
            if (e.key === 'Enter') {
                if (e.shiftKey) saveDevice();
                else F.$$(e.target, 'next-input').focus();
            }
            else _checkKey(e.key);
        }, 'keyup', ids1);

        F.$ael(e => {
            if (e.key === 'Enter' && e.ctrlKey) saveDevice();
            else _checkKey(e.key);
        }, 'keyup', 'rbs_info');

        F.$ael(e => {
            _deviceChanged = true;
        }, 'change', ids2);

        F.$('rbs_name').focus();
    }, () => {
        if (_rbs_devicesIsDirty) window.location.reload(true);
    });
}


const showAddObject = e => {
    let input = F.$('rbs__new_object');

    input.value = '';
    F.$clr('rbs__new_input', 'd-none');
    F.$clr('rbs__new_button', 'd-none');

    F.$('rbs__new_target').value = e.target.id;
    input.focus();
}

const addObject = () => {
    let target_id = F.$('rbs__new_target').value;
    let obj_name = F.$('rbs__new_object').value.trim();

    if (obj_name.length < 2)
        errorAlert(`Lisatava ${F.$gda(target_id, 'object-name')} nimi peab olema vähemalt 2 märki pikk.`);
    else {
        let task_name = F.$gda(target_id, 'task');
        let fd = {task: `save_${task_name}`, rbs_return_options: 1};

        F.$cla('rbs__new_input', 'd-none');
        F.$cla('rbs__new_button', 'd-none');
        fd[`rbs_${task_name}_name`] = obj_name;

        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    let select = F.$$(F.$('rbs__new_target').value, 'select-id');

                    select.innerHTML = r.options;
                    F.$ra(select, 'disabled');
                    F.$sa(`${select.id}_add`, 'tabindex', -1);
                    select.focus();
                    updateButtonStatus();
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`DEVICE_OBJECT_SAVE_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$ael(openAddChangeDeviceObjectModal, 'click', 'add_device_modal');

    F.$('rbs_device_ids').value.split(',').forEach(id => {
        // let del_btn = F.$(`rbs_device_delete_${id}`);

        F.$ael(() => openAddChangeDeviceObjectModal(id), 'click', `rbs_device_edit_${id}`);
        F.$ael(() => openAddChangeDeviceInGroupModal(id), 'click', `rbs_devices_list_${id}`);
        // if (ch_pwd_btn) F.$ael(() => openChangePasswordModal(id), 'click', ch_pwd_btn);
        // if (del_btn) F.$ael(() => deleteUser(id), 'click', del_btn);
    });
}, 'DOMContentLoaded');
