/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _dom, _modal, _sf, _hf;  // needed for modal

const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n);

const createColumn = txt => {
    let col = F.$ce('td');
    col.append(txt);
    return col;
}

const getFormData = (id, task) => {
    let pd = {task, error:''};
    let req_ids = [];

    for (let pair of ( new FormData(F.$(id)) ).entries()) {
        let isFile = pair[1] instanceof File;
        let val = (isFile ? pair[1] : pair[1].trim());

        if (pair[0] == 'rbs' && F.$hda(pair[0], 'required-ids')) {
            req_ids = F.$gda(pair[0], 'required-ids').split(',');
            continue;
        }

        if (!isFile && !F.$hda(pair[0], 'not-required') &&
            (!val.length || val.length < 2 && !isNumeric(val))
        ) {
            pd.error += `|${pair[0]}=Väli on kohustuslik  (min 2 sümbolit).`;
            continue;
        }
        
        pd[pair[0]] = val;
    }

    req_ids.forEach(id => {
        if (!pd[id]) pd.error += `|${id}=Väli on kohustuslik.`
    });

    return pd;
}

const setErrorMessage = str => {
    let er = str.split('|');
    let s = er.shift();

    er.forEach(el => {
        let parts = el.split('=');

        for (let id of parts[0].split(',')) {
            let lbl = F.$(`${id}_lbl`);

            lbl && (lbl.innerText = parts[1]);
            F.$cla(id, 'is-invalid');
        }
    });
    if (!er.length) errorAlert(s);
}

const errorAlert = err => {
    F.$('error_text').innerHTML = err;
    bootstrap.Toast.getOrCreateInstance(F.$('error_toast')).show();
}

const successAlert = txt => {
    F.$('success_text').innerHTML = txt;
    bootstrap.Toast.getOrCreateInstance(F.$('success_toast')).show();
}

const openModalExt = (data, shown_func = null, hide_func = null) => {
    F.$post(RBS_PATH, data)
        .then(r => {
            if (r.error) {
                errorAlert(`Modaalakna avamise viga: ${r.error}`);
                return;
            }

            if (!_dom) {
                _dom = F.$ce('div');
                _dom.setAttribute('class', 'modal fade');
            }
            _dom.innerHTML = r;
            if (shown_func) { _sf = shown_func; F.$ael(_sf, 'shown.bs.modal', _dom); }
            if (hide_func) { _hf = hide_func; F.$ael(_hf, 'hide.bs.modal', _dom); }

            F.$ael(() => {
                if (_sf) { F.$rel(_sf, 'shown.bs.modal', _dom); _sf = null; _sf = undefined; }
                if (_hf) { F.$rel(_hf, 'hide.bs.modal', _dom); _hf = null; _hf = undefined; }
            }, 'hidden.bs.modal', _dom);

            /// removes error status and resets labels
            (F.$q('label[data-rbs-label]', _dom, true)).forEach(el => { 
                let input = F.$(el.htmlFor);

                el.innerText = F.$gda(el, 'label');
                if (input) F.$clr(input, 'is-invalid');
            });

            _modal = bootstrap.Modal.getOrCreateInstance(_dom);
            _modal.show();
        })
        .catch(e => console.error(`OPEN_MODAL_ERROR:  ${e}`));
}

const openModal = (task, shown_func = null, hide_func = null) => {
    openModalExt({task}, shown_func, hide_func);
}

const openLoginModal = () => {
    openModal('open_login_modal', () => {
        F.$ael(e => {
            if (e.key === 'Enter') logIn();
        }, 'keyup', ['rbs_user', 'rbs_password']);

        F.$('rbs_user').focus();
    });
}

const openAboutModal = () => { openModal('open_about_modal'); }

const openHelpModal = () => {
    let task = 'open_help_modal';
    let page = F.$gda('rbs_help', 'page');

    openModalExt({task, page});
}

const openChangePasswordModal = (uid = 0) => {
    let p = {task: 'open_change_password_modal'};
    if (isNumeric(uid) && uid) p.user_id = uid;

    openModalExt(p, () => {
        F.$ael(e => {
            if (e.key === 'Enter') {
                if (!F.$hda(e.target, 'next-input')) changePassword();
                else F.$$(e.target, 'next-input').focus();
            }
        }, 'keyup', ['rbs_password', 'rbs_password_repeat']);

        F.$('rbs_password').focus();
    });
}

const openImageModal = el => {
    let task = 'view_image_modal';
    let image_id = F.$gda(el, 'image-id');

    openModalExt({task, image_id});
}

const changePassword = () => {
    let fd = getFormData('change_password_form', 'user_change_password');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.result) {
                    _modal.hide();
                    successAlert('Parool edukalt muudetud.');
                }
                else {
                    F.$('rbs_password').value = '';
                    F.$('rbs_password_repeat').value = '';
                    setErrorMessage(r.error);
                    F.$('rbs_password').focus();
                }
            })
            .catch(e => console.error(`CHANGE_PASSWORD_ERROR:  ${e}`));
    }
}

const setDeviceFilter = type => {
    let fd = {task: 'filter_device_table', type, value: F.$(`rbs_filter_device_${type}`).value};
    console.log(fd);

    F.$post(RBS_PATH, fd)
        .then(_ => window.location.reload(true))
        .catch(e => console.error(`FILTERING_DEVICE_TABLE_ERROR:  ${e}`));
}

const logOut = () => {
    F.$post(RBS_PATH, {'task':'user_logout'})
        .then(() => window.location.reload(true))
        .catch(e => console.error(`LOG_OUT_ERROR:  ${e}`));
}


F.$ael(e => {
    let _devices = F.$('rbs_devices_table') ?? false;
    let _about = F.$('rbs_about') ?? false;
    let _help =  F.$('rbs_help') ?? false;
    let _login = F.$('person_login') ?? false;

    if (_devices) {
        [].slice.call( F.$q('[data-bs-toggle="tooltip"]', 'rbs_devices_table', true) )
            .map(el => new bootstrap.Tooltip(el));

        ['type', 'manufacturer', 'model', 'room'].forEach(t => {
            F.$ael(_ => setDeviceFilter(t), 'change', `rbs_filter_device_${t}`);
        });
        F.$ael(_ => setDeviceFilter('reset'), 'click', 'rbs_filter_device_reset');

        F.$c('img-thumbnail', _devices).forEach(el => {
            F.$ael(_ => openImageModal(el), 'click', el);
        });
    }
    if (_about) F.$ael(openAboutModal, 'click', _about);
    if (_help) F.$ael(openHelpModal, 'click', _help);

    if (_login) F.$ael(openLoginModal, 'click', _login);
    else {
        F.$ael(openChangePasswordModal, 'click', 'person_change_password');
        F.$ael(logOut, 'click', 'person_logout');
    }
}, 'DOMContentLoaded');