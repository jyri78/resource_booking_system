/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _rbs_roomsIsDirty = false;
let _rbs_editableId = 0;


const addNewRoomToTable = new_id => {
    let cols = ['rbs_room_name', 'rbs_room_description'];
    let row = F.$ce('tr');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );

    cols.forEach(id => {
        row.append( createColumn(F.$(id).value) );
        F.$(id).value = '';
    });

    row.append( createColumn('') );

    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    _rbs_roomsIsDirty = true;
    F.$('rooms_table_body').prepend(row);
    F.$('rbs_room_description').value = '';
    F.$('rbs_room_name').focus();
}

const makeRowEditable = id => {
    F.$ra(`rbs_room_name_${id}`, 'disabled');
    F.$ra(`rbs_room_name_${id}`, 'readonly');
    F.$ra(`rbs_room_description_${id}`, 'disabled');
    F.$ra(`rbs_room_description_${id}`, 'readonly');
    F.$ra(`rbs_room_not_for_booking_${id}`, 'disabled');
    F.$ra(`rbs_room_not_for_booking_${id}`, 'readonly');
    F.$clr(`rbs_room_save_change_${id}`, 'invisible');
    _rbs_editableId = id;
    F.$(`rbs_room_name_${id}`).focus();
}

const makeRowUneditable = id => {
    F.$sa(`rbs_room_name_${id}`, 'disabled', true);
    F.$sa(`rbs_room_name_${id}`, 'readonly', true);
    F.$sa(`rbs_room_description_${id}`, 'disabled', true);
    F.$sa(`rbs_room_description_${id}`, 'readonly', true);
    F.$sa(`rbs_room_not_for_booking_${id}`, 'disabled', true);
    F.$sa(`rbs_room_not_for_booking_${id}`, 'readonly', true);
    F.$cla(`rbs_room_save_change_${id}`, 'invisible');
}

const editRoom = id => {
    if (_rbs_editableId) {
        if (_rbs_editableId == id) {
            _rbs_editableId = 0;
            makeRowUneditable(id);
        }
        else {
            makeRowUneditable(_rbs_editableId);
            makeRowEditable(id);
        }
    }
    else makeRowEditable(id);
}

const deleteRoom = id => {
    if (confirm(`Kas oled kindel, et soovid ruumi "${F.$(`rbs_room_name_${id}`).value}" (ID: ${id}) kustutada?`)) {
        F.$post(RBS_PATH, {task: 'delete_room', rbs_room_id: id})
            .then(() => window.location.reload(true))
            .catch(e => console.error(`ROOM_DELETE_ERROR:  ${e}`));
    }
}

const changeNotForRoom = id => {
    if (F.$(`rbs_room_not_for_booking_${id}`).checked)
        F.$cla(`rbs_room_${id}`, ['bg-warning', 'bg-opacity-10']);
    else
        F.$clr(`rbs_room_${id}`, ['bg-warning', 'bg-opacity-10']);
}

const saveRoomChange = (e) => {
    if (e.key === 'Enter' && e.ctrlKey) {
        let room_data = {
            task: 'save_room_change',
            rbs_room_id: _rbs_editableId,
            rbs_room_name: F.$(`rbs_room_name_${_rbs_editableId}`).value,
            rbs_room_description: F.$(`rbs_room_description_${_rbs_editableId}`).value,
            rbs_room_not_for_booking: F.$(`rbs_room_not_for_booking_${_rbs_editableId}`).checked
        };

        F.$post(RBS_PATH, room_data)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    makeRowUneditable(r.id);
                    successAlert('Ruumi muudatus salvestatud.');
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`ROOM_CHANGE_ERROR:  ${e}`));
    }
}

const saveNewRoom = () => {
    let fd = getFormData('room_add_form', 'save_new_room');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) addNewRoomToTable(r.id);
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`ROOM_SAVE_ERROR:  ${e}`));
    }
}

const addNewRoomModal = () => {
    openModal('open_new_room_modal',
        () => {
            F.$ael(e => {
                if (e.key === 'Enter') F.$('rbs_room_description').focus();
            }, 'keyup', 'rbs_room_name');

            F.$ael(e => {
                if (e.key === 'Enter' && e.ctrlKey) saveNewRoom();
            }, 'keyup', 'rbs_room_description');

            F.$('rbs_room_name').focus();
        }, () => {
            if (_rbs_roomsIsDirty) window.location.reload(true);
        });
}


F.$ael(e => {
    F.$ael(addNewRoomModal, 'click', 'add_room_modal');

    F.$('rbs_room_ids').value.split(',').forEach(id => {
        F.$ael(_ => editRoom(id), 'click', `rbs_room_edit_${id}`);
        F.$ael(_ => deleteRoom(id), 'click', `rbs_room_delete_${id}`);
        F.$ael(_ => changeNotForRoom(id), 'change', `rbs_room_not_for_booking_${id}`);

        F.$ael(e => saveRoomChange(e), 'keyup', `rbs_room_name_${id}`);
        F.$ael(e => saveRoomChange(e), 'keyup', `rbs_room_description_${id}`);
        F.$ael(_ => saveRoomChange({key:'Enter', ctrlKey:true}), 'click', `rbs_room_save_change_${id}`);
    });
}, 'DOMContentLoaded');
