/* 
    Copyright (C) 2021-2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let cal;

const POST_EXTRA_KEY = "rbs=%%RBS_KEY%%";
const MIN_LESSON_TIME = "%%RBS_MIN_LESSON_TIME%%";
const CALENDAR_EDIT_ICON = `<div class="float-end">&nbsp; %%RBS_ICON_EDIT%%</div>`;
const CALENDAR_REPEAT_ICON = `<div class="float-end">%%RBS_ICON_REPEAT%%</div>`;

// includes() polyfill (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes#polyfill)
if (!String.prototype.includes) {
    String.prototype.includes = function(search, start) {
        'use strict';
        if (search instanceof RegExp) throw TypeError('first argument must not be a RegExp');
        if (start === undefined) start = 0;
        return this.indexOf(search, start) !== -1;
    };
}

// addId() function for adding new item to penultimate location
Array.prototype.addId = function(item) {
    this.splice(this.length-1, 0, item);
    return this;
};