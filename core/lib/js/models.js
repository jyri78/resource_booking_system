/* 
    Copyright (C) 2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _rbs_modelsIsDirty = false;


const addNewModelToTable = new_id => {
    let row = F.$ce('tr');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );

    row.append( createColumn( F.$('rbs_model_name').value ) );
    row.append( createColumn('0') );
    row.append( createColumn('') );
    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    _rbs_modelsIsDirty = true;
    F.$('models_table_body').prepend(row);
    F.$('rbs_model_name').value = '';
    F.$('rbs_model_name').focus();
}

const saveModel = () => {
    let fd = getFormData('model_add_form', 'save_model');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    if (!F.$('rbs_model_id')) addNewModelToTable(r.id);
                    else window.location.reload(true);
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`MODEL_SAVE_ERROR:  ${e}`));
    }
}


const openAddChangeModelModal = (mid = 0) => {
    let p = {task: 'open_add_change_model_modal'};

    if (isNumeric(mid) && mid) p.model_id = mid;

    openModalExt(p, () => {
        F.$ael(e => {
            if (e.key === 'Enter') saveModel();
        }, 'keyup', 'rbs_model_name');
        
        F.$('rbs_model_name').focus();
    }, () => {
        if (_rbs_modelsIsDirty) window.location.reload(true);
    });
}


const deleteModel = mid => {
    if (confirm(`Kas oled kindel, et soovid mudelit "${F.$(`rbs_model_name_${mid}`).innerHTML}" (ID: ${mid}) kustutada?`)) {
        F.$post(RBS_PATH, {task: 'delete_model', rbs_model_id: mid})
            .then(() => window.location.reload(true))
            .catch(e => console.error(`MODEL_DELETE_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$ael(openAddChangeModelModal, 'click', 'add_model_modal');

    F.$('rbs_model_ids').value.split(',').forEach(id => {
        if (!id) return;
        let del_btn = F.$(`rbs_model_delete_${id}`);

        F.$ael(() => openAddChangeModelModal(id), 'click', `rbs_model_edit_${id}`);
        if (del_btn) F.$ael(() => deleteModel(id), 'click', del_btn);
    });
}, 'DOMContentLoaded');
