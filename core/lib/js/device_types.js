/* 
    Copyright (C) 2022 Jüri Kormik

    This software is released under the GNU v3 License.
    https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE
*/


let _rbs_deviceTypesIsDirty = false;


const addNewDeviceTypeToTable = new_id => {
    let row = F.$ce('tr');

    if (F.$('rbs_show_table_ids').value == '1') row.append( createColumn(new_id) );

    row.append( createColumn( F.$('rbs_device_type_name').value ) );
    row.append( createColumn('0') );
    row.append( createColumn('') );
    F.$cla(row, ['bg-success', 'bg-opacity-25']);
    _rbs_deviceTypesIsDirty = true;
    F.$('device_types_table_body').prepend(row);
    F.$('rbs_device_type_name').value = '';
    F.$('rbs_device_type_name').focus();
}

const saveDeviceType = () => {
    let fd = getFormData('device_type_add_form', 'save_device_type');

    if (fd.error) setErrorMessage(fd.error);
    else {
        F.$post(RBS_PATH, fd)
            .then(r => {
                if (r.debug) console.log(r.debug);
                if (r.result) {
                    if (!F.$('rbs_device_type_id')) addNewDeviceTypeToTable(r.id);
                    else window.location.reload(true);
                }
                else setErrorMessage(r.error);
            })
            .catch(e => console.error(`DEVICE_TYPE_SAVE_ERROR:  ${e}`));
    }
}


const openAddChangeDeviceTypeModal = (tid = 0) => {
    let p = {task: 'open_add_change_device_type_modal'};

    if (isNumeric(tid) && tid) p.device_type_id = tid;

    openModalExt(p, () => {
        F.$ael(e => {
            if (e.key === 'Enter') {
                saveDeviceType();
            }
        }, 'keyup', ['rbs_device_type_name']);

        F.$('rbs_device_type_name').focus();
    }, () => {
        if (_rbs_deviceTypesIsDirty) window.location.reload(true);
    });
}


const deleteDeviceType = tid => {
    if (confirm(`Kas oled kindel, et soovid seadme tüüpi "${F.$(`rbs_device_type_name_${tid}`).innerHTML}" (ID: ${tid}) kustutada?`)) {
        F.$post(RBS_PATH, {task: 'delete_device_type', rbs_device_type_id: tid})
            .then(() => window.location.reload(true))
            .catch(e => console.error(`DEVICE_TYPE_DELETE_ERROR:  ${e}`));
    }
}


F.$ael(e => {
    F.$ael(openAddChangeDeviceTypeModal, 'click', 'add_device_type_modal');

    F.$('rbs_device_type_ids').value.split(',').forEach(id => {
        if (!id) return;
        let del_btn = F.$(`rbs_device_type_delete_${id}`);

        F.$ael(() => openAddChangeDeviceTypeModal(id), 'click', `rbs_device_type_edit_${id}`);
        if (del_btn) F.$ael(() => deleteDeviceType(id), 'click', del_btn);
    });
}, 'DOMContentLoaded');
