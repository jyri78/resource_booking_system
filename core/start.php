<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GPL v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


$_etc = __DIR__ . DIRECTORY_SEPARATOR .'etc'. DIRECTORY_SEPARATOR;

error_reporting(E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR);
ini_set('log_errors', 1);
ini_set('error_log', "{$_etc}error.log");


if (!file_exists("{$_etc}config.ini")) {
    include "{$_etc}install.php";
    exit;
}


/**
 * Helper function to sanitize HTML output (remove extra spaces and linebreaks)
 * 
 * Credit: Rakesh Sankar (https://stackoverflow.com/users/765854/rakesh-sankar)
 * Source: https://stackoverflow.com/questions/6225351/how-to-minify-php-page-html-output
 *
 * @param   string  $buffer
 *
 * @return  string
 */
function sanitize_output($buffer) {
    $search = [
        '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
        '/[^\S ]+\</s',     // strip whitespaces before tags, except space
        '/(\s)+/s',         // shorten multiple whitespace sequences
        '/<!--(.|\s)*?-->/' // Remove HTML comments
    ];
    $replace = ['>', '<', '\\1', '' ];
    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}


include "{$_etc}config.ini";
if (!isset($_CONFIG['allow_debug'])) $_CONFIG['allow_debug'] = false;  // reset to false

if ($_CONFIG['allow_debug']) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);  // in debug mode display all messages
}
else {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
}


// Now include required files (classes and functions), and start app
try {
    foreach (glob($_CONFIG['root_dir'] . 'core/classes/*.php') as $filename) {
        include $filename;
    }
    $render_view = new rbs\core\RenderView();  // for connection between front-end and back-end

    rbs\core\SessionsManager::sessionStart('rbs', 0, $_CONFIG['root_url'], $_SERVER['HTTP_HOST'], $_CONFIG['secure']);

    ob_start('sanitize_output');
    require_once $_CONFIG['root_dir'] . 'public/views/functions.php';
    require_once $_CONFIG['root_dir'] . 'public/views/index.php';
    ob_end_flush();
}
catch (Error $e) {
        if ($_CONFIG['allow_debug']) error_log($e);
        else error_log(' *  RBS_PHP_ERROR: '. $e->getMessage() .' | '. $e->getFile() .':'. $e->getLine());
}
catch (Throwable $e) { error_log($e); }

exit;
