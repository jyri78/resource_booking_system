<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


final class Resource extends DB
{
    private const _TABLE_OBJECT_TYPE = 'object_type';
    private const _TABLE_MANUFACTURER = 'manufacturer';
    private const _TABLE_MODEL = 'model';


    /**
     * Returns list of resource types (room or device) containing at least one resource
     *
     * @return  array
     */
    public static function getObjectTypesWithObject(): array
    {
        return (new Resource)->_getObjectTypesWithObject();
    }

    /**
     * Returns all resource types
     *
     * @return  array
     */
    public static function getAllObjectsTypes(): array
    {
        return (new Resource)->_getDeviceTableDataAll(self::_TABLE_OBJECT_TYPE);
    }

    /**
     * Returns all device types
     *
     * @return  array
     */
    public static function getAllDeviceTypes(): array
    {
        return (new Resource)->_getDeviceTableDataAll(self::_TABLE_OBJECT_TYPE, true);
    }

    /**
     * Returns object type by ID
     *
     * @param   int    $id
     *
     * @return  array
     */
    public static function getObjectTypeById(int $id): array
    {
        return (new Resource)->_getDeviceTableDataById($id, self::_TABLE_OBJECT_TYPE);
    }

    /**
     * Returns list of resource types and number of objects in type
     *
     * @return  array
     */
    public static function getObjectTypesAndCntObjects(): array
    {
        return (new Resource)->_getDeviceTableDataAndCntObjects(self::_TABLE_OBJECT_TYPE);
    }

    /**
     * Returns list of device types and number of objects in type
     *
     * @return  array
     */
    public static function getDeviceTypesAndCntObjects(): array
    {
        return (new Resource)->_getDeviceTableDataAndCntObjects(self::_TABLE_OBJECT_TYPE, true);
    }

    /**
     * Adds new device type to the database
     *
     * @param   string  $name
     *
     * @return  array
     */
    public static function insertDeviceType(string $name): array
    {
        return (new Resource)->_insertDeviceTableData($name, self::_TABLE_OBJECT_TYPE);
    }

    /**
     * Updates device type name
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public static function updateDeviceType(array $params): array
    {
        return (new Resource)->_updateDeviceTableData($params, self::_TABLE_OBJECT_TYPE);
    }

    /**
     * Deletes device type from database
     *
     * @param   int  $id
     *
     * @return  array
     */
    public static function deleteDeviceType(int $id): array
    {
        if ($id < 2) return ['success' => false, 'error' => 0];
        return (new Resource)->_deleteDeviceTableData($id, self::_TABLE_OBJECT_TYPE);
    }


    /**
     * Returns list of device manufacturers
     *
     * @return  array
     */
    public static function getDeviceManufacturers(): array
    {
        return (new Resource)->_getDeviceTableDataAll(self::_TABLE_MANUFACTURER);
    }

    /**
     * Returns device manufacturer by ID
     *
     * @param   int    $id
     *
     * @return  array
     */
    public static function getDeviceManufacturerById(int $id): array
    {
        return (new Resource)->_getDeviceTableDataById($id, self::_TABLE_MANUFACTURER);
    }

    /**
     * Returns list of device manufacturers and number of devices
     *
     * @return  array
     */
    public static function getDeviceManufacturersAndCntObjects(): array
    {
        return (new Resource)->_getDeviceTableDataAndCntObjects(self::_TABLE_MANUFACTURER);
    }

    /**
     * Adds new device manufacturer to the database
     *
     * @param   string  $name
     *
     * @return  array
     */
    public static function insertDeviceManufacturer(string $name): array
    {
        return (new Resource)->_insertDeviceTableData($name, self::_TABLE_MANUFACTURER);
    }

    /**
     * Updates device manufacturers name
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public static function updateDeviceManufacturer(array $params): array
    {
        return (new Resource)->_updateDeviceTableData($params, self::_TABLE_MANUFACTURER);
    }

    /**
     * Deletes device manufacturer from database
     *
     * @param   int  $id
     *
     * @return  array
     */
    public static function deleteDeviceManufacturer(int $id): array
    {
        return (new Resource)->_deleteDeviceTableData($id, self::_TABLE_MANUFACTURER);
    }


    /**
     * Returns list of device models
     *
     * @return  array
     */
    public static function getDeviceModels(): array
    {
        return (new Resource)->_getDeviceTableDataAll(self::_TABLE_MODEL);
    }

    /**
     * Returns device model by ID
     *
     * @param   int    $id
     *
     * @return  array
     */
    public static function getDeviceModelById(int $id): array
    {
        return (new Resource)->_getDeviceTableDataById($id, self::_TABLE_MODEL);
    }

    /**
     * Returns list of device models and number of devices
     *
     * @return  array
     */
    public static function getDeviceModelsAndCntObjects(): array
    {
        return (new Resource)->_getDeviceTableDataAndCntObjects(self::_TABLE_MODEL);
    }

    /**
     * Adds new device model to the database
     *
     * @param   string  $name
     *
     * @return  array
     */
    public static function insertDeviceModel(string $name): array
    {
        return (new Resource)->_insertDeviceTableData($name, self::_TABLE_MODEL);
    }

    /**
     * Updates device model
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public static function updateDeviceModel(array $params): array
    {
        return (new Resource)->_updateDeviceTableData($params, self::_TABLE_MODEL);
    }

    /**
     * Deletes device model from database
     *
     * @param   int  $id
     *
     * @return  array
     */
    public static function deleteDeviceModel(int $id): array
    {
        return (new Resource)->_deleteDeviceTableData($id, self::_TABLE_MODEL);
    }


    /**
     * Returns first resource (room or device) by type ID
     *
     * @param   int    $type_id
     *
     * @return  array
     */
    public static function getFirstObjectByTypeID(int $type_id): array
    {
        return (new Resource)->_getFirstObjctByTypeId($type_id);
    }

    /**
     * Returns number of all resources or number of resources by type
     *
     * @param   int    $type_id
     *
     * @return  array
     */
    public static function getObjectsCount(int $type_id = 0): array
    {
        return (new Resource)->_getObjectsCount($type_id);
    }

    /**
     * Returns all objects by type name
     *
     * @param   array  $params  Array of [<type_id>, <not_for_booking>]
     *
     * @return  array
     */
    public static function getObjectsByType(array $params): array
    {
        return (new Resource)->_getObjectsByType($params);
    }

    /**
     * Returns all device objects; filtered by either `type_id`, `manufacturer_id` or `model_id`, or by all
     *
     * @param   array  [$params]  Array of [<type_id>, <manufacturer_id>, <model_id>, <room_id>]
     *
     * @return  array
     */
    public static function getDeviceObjects(array $params = []): array
    {
        return (new Resource)->_getDeviceObjects($params);
    }

    /**
     * Returns device object by ID
     *
     * @param   int    $device_object_id
     *
     * @return  array
     */
    public static function getDeviceObjectById(int $device_object_id): array
    {
        return (new Resource)->_getDeviceObjectById($device_object_id);
    }

    /**
     * Returns devices file data
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public static function getDeviceFile(int $device_id): array
    {
        return (new Resource)->_getDeviceFile($device_id);
    }


    /**
     * Adds new room to the database
     *
     * @param   array  $params  Array of [<name>, <info>, <not_for_booking>, <color>]
     *
     * @return  array
     */
    public static function insertRoom(array $params): array
    {
        return (new Resource)->_insertRoom($params);
    }

    /**
     * Adds new device object to the database
     *
     * @param   array  $params  Array of [<name>, <info>, <count>, <type_id>, <color>, <manufacturer_id>, <model_id>,
     *                                    <room_id>, <image_file>, <image_filename>, <image_type>]
     *
     * @return  array
     */
    public static function insertDeviceObject(array $params): array
    {
        return (new Resource)->_insertDeviceObject($params);
    }

    /**
     * Updates room data
     *
     * @param   array  $params  Array of [<room_id>, <name>, <info>, <not_for_booking>, <color>]
     *
     * @return  array
     */
    public static function updateRoom(array $params): array
    {
        return (new Resource)->_updateRoom($params);
    }

    /**
     * Updates device object data
     *
     * @param   array  $params  Array of [<device_id>, <name>, <info>, <count>, <color>,
     *                                    <room_id>, <image_file>, <image_filename>, <image_type>]
     *
     * @return  array
     */
    public static function updateDeviceObject(array $params): array
    {
        return (new Resource)->_updateDeviceObject($params);
    }

    /**
     * Deletes resource (room or device)
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public static function deleteObject(int $object_id): array
    {
        return (new Resource)->_deleteObject($object_id);
    }

    /**
     * Gets all device IDs by device object id
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public static function getDeviceIds(int $object_id): array
    {
        return (new Resource)->_deviceIds($object_id);
    }

    /**
     * Gets devices data by device object ID
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public static function getDevicesByObjectId(int $object_id): array
    {
        return (new Resource)->_devicesGetByObjectId($object_id);
    }

    /**
     * Gets devices count in set by device object ID
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public static function getDevicesCountInSet(int $object_id): array
    {
        return (new Resource)->_devicesCntInSet($object_id);
    }

    /**
     * Gets device data by ID
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public static function getDeviceById(int $device_id): array
    {
        return (new Resource)->_deviceGetById($device_id);
    }

    /**
     * Adds new device to the table
     *
     * @param   array  $params  Array of [<device_object_id>, <name>, <asset_code>, <serial_number>, <info>]
     *
     * @return  array
     */
    public static function insertDevice(array $params): array
    {
        return (new Resource)->_insertDevice($params);
    }

    /**
     * Updates device data
     *
     * @param   array  $params  Array of [<device_id>, <name>, <asset_code>, <serial_number>, <info>, <is_broken>]
     *
     * @return  array
     */
    public static function updateDevice(array $params): array
    {
        return (new Resource)->_updateDevice($params);
    }

    /**
     * Delete device
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public static function deleteDevice(int $device_id): array
    {
        return (new Resource)->_deleteDevice($device_id);
    }


    /* ========================================================================
     *   Private methods
     * ========================================================================
     */

    private static function _selectSql($table, $sqls)
    {
        foreach ($sqls as $key => $sql) {
            if ($table == $key) return $sql;
        }
    }


    private function __construct()
    {
        parent::__construct();
    }


    private function _getFirstObjctByTypeId($type_id)
    {
        $this->query(self::SQL_OBJECT_FIRST, [$type_id], false);
        return $this->result();
    }

    private function _getObjectTypesWithObject()
    {
        $this->query(self::SQL_OBJECT_TYPE_GET_WO);
        return $this->result();
    }


    private function _getDeviceTableDataAll($table, $exclude_rooms = false)
    {
        $sqls = [
            self::_TABLE_OBJECT_TYPE    => (
                                                $exclude_rooms ?
                                                self::SQL_DEVICE_TYPES_ALL :
                                                self::SQL_OBJECT_TYPES_ALL
                                            ),
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURERS_ALL,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODELS_ALL
        ];
        $this->query(self::_selectSql($table, $sqls));
        return $this->result();
    }

    private function _getDeviceTableDataById($id, $table, $return_success = false)
    {
        $sqls = [
            self::_TABLE_OBJECT_TYPE    => self::SQL_OBJECT_TYPE_GET,
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURER_GET,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODEL_GET
        ];
        $this->query(self::_selectSql($table, $sqls), [$id], false);
        return ($return_success ? $this->success : $this->result());
    }

    private function _getDeviceTableDataAndCntObjects($table, $exclude_rooms = false)
    {
        $sqls = [
            self::_TABLE_OBJECT_TYPE    => (
                                                $exclude_rooms ?
                                                self::SQL_DEVICE_TYPES_WITH_CNT :
                                                self::SQL_OBJECT_TYPES_WITH_CNT
                                            ),
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURERS_WITH_CNT,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODELS_WITH_CNT
        ];
        $this->query(self::_selectSql($table, $sqls));
        return $this->result();
    }

    private function _insertDeviceTableData($name, $table)
    {
        $sqls = [
            self::_TABLE_OBJECT_TYPE    => self::SQL_OBJECT_TYPE_ADD,
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURER_ADD,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODEL_ADD
        ];
        $this->query(self::_selectSql($table, $sqls), [$name, date('Y-m-d H:i:s')]);
        return $this->result();
    }

    private function _updateDeviceTableData($params, $table)
    {
        $sqls = [
            self::_TABLE_OBJECT_TYPE    => self::SQL_OBJECT_TYPE_CHANGE,
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURER_CHANGE,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODEL_CHANGE
        ];
        $this->query(self::_selectSql($table, $sqls), [$params['name'], $params['id']]);
        return $this->result();
    }

    private function _deleteDeviceTableData($id, $table)
    {
        if (!$this->_getDeviceTableDataById($id, $table, true))
            return $this->result();  // data (type, manufacturer or model) not found

        $sqls = [
            self::_TABLE_OBJECT_TYPE    => self::SQL_OBJECT_TYPE_DELETE,
            self::_TABLE_MANUFACTURER   => self::SQL_DEVICE_MANUFACTURER_DELETE,
            self::_TABLE_MODEL          => self::SQL_DEVICE_MODEL_DELETE
        ];
        $this->query(self::_selectSql($table, $sqls), [$id]);
        return $this->result();
    }


    private function _getObjectsCount($type_id)
    {
        $sql = self::SQL_OBJECTS_COUNT;
        $data = [];

        if ($type_id) {
            $sql .= ' AND  ot.id = ?';
            $data[] = $type_id;
        }

        $this->query($sql, $data);
        return $this->result();
    }

    private function _getObjectsByType($params)
    {
        if (!empty($params['type_id'])) {
            $sql = (
                    $params['not_for_booking'] ?
                    self::SQL_OBJECTS_ALL_EXT : self::SQL_OBJECTS_ALL
                ) .'id = ?';
            $data = [$params['type_id']];
        }
        // elseif (!empty($params['type_name'])) {
        //     $sql_add = 'type_name = ?';
        //     $data = [$params['type_name']];
        // }
        else return $this->result(1);

        $this->query($sql, $data);
        return $this->result();
    }

    private function _getDeviceObjects($params)
    {
        $_extra = [
            'type' => 'object_type', 'manufacturer' => 'device_manufacturer',
            'model' => 'device_model', 'room' => 'room'
        ];
        $sql = self::SQL_DEVICE_OBJECTS_ALL;
        $data = [];

        foreach ($_extra as $key => $name) {
            if (isset($params["{$key}_id"])) {
                $_val = $params["{$key}_id"];
                $sql .= " AND bo.rbs__{$name}__id " . ($_val == '0' ? ' IS NULL' : '= ?');
                if ($_val != '0') $data[] = $_val;
            }
        }
        $this->query($sql, $data);
        return $this->result();
    }

    private function _getDeviceObjectById($id)
    {
        $this->query(self::SQL_DEVICE_OBJECT_GET, [$id], false);
        return $this->result();
    }

    private function _getDeviceFile($id)
    {
        $this->query(self::SQL_DEVICE_FILE, [$id], false);
        return $this->result();
    }

    private function _insertRoom($params)
    {
        if (!isset($params['not_for_booking'])) $params['not_for_booking'] = '0';

        $this->query(self::SQL_ROOM_ADD, [
            $params['name'], $params['info'], $params['not_for_booking'],
            @$params['color'], 1, date('Y-m-d H:i:s')
        ]);
        return $this->result();
    }

    private function _insertDeviceObject($params)
    {
        $this->query(self::SQL_DEVICE_OBJECT_ADD, [
            $params['name'], $params['info'], $params['count'], @$params['color'],
            $params['type_id'], $params['manufacturer_id'], $params['model_id'],
            $params['room_id'], @$params['image_file'], @$params['image_filename'],
            @$params['image_type'], date('Y-m-d H:i:s')
        ]);
        return $this->result();
    }

    private function _updateRoom($params)
    {
        $this->query(self::SQL_OBJECT_GET, [$params['room_id']], false);
        if (!$this->success) return $this->result();  // object not found

        if (!isset($params['not_for_booking'])) $params['not_for_booking'] = '0';

        $this->query(
            self::SQL_ROOM_CHANGE,
            [
                $params['name'], $params['info'], $params['not_for_booking'],
                @$params['color'], $params['room_id']
            ]
        );
        return $this->result();
    }

    private function _updateDeviceObject($params)
    {
        $this->query(self::SQL_OBJECT_GET, [$params['device_id']], false);
        if (!$this->success) return $this->result();  // object not found

        if (isset($params['image_file'])) {
            $sql = self::SQL_DEVICE_OBJECT_CHANGE_WI;
            $data = [
                $params['name'], $params['info'], $params['count'], @$params['color'],
                $params['room_id'], $params['image_file'], $params['image_filename'],
                $params['image_type'], $params['device_id']
            ];
        }
        else {
            $sql = self::SQL_DEVICE_OBJECT_CHANGE;
            $data = [
                $params['name'], $params['info'], $params['count'], @$params['color'],
                $params['room_id'], $params['device_id']
            ];
        }

        $this->query($sql, $data);
        return $this->result();
    }

    private function _deleteObject($object_id)
    {
        $this->query(self::SQL_OBJECT_GET, [$object_id], false);
        if (!$this->success) return $this->result();  // object not found

        $this->query(self::SQL_OBJECT_HBO_DELETE, [$this->fetch_result->rbs__booking_object__id]);
        // if (!$this->success) return $this->result();  // error occured
        $this->query(self::SQL_OBJECT_DELETE, [$object_id]);
        return $this->result();
    }


    private function _deviceIds($object_id)
    {
        $this->query(self::SQL_DEVICE_IDS, [$object_id]);
        return $this->result();
    }

    private function _devicesGetByObjectId($id)
    {
        $this->query(self::SQL_DEVICES_BY_OBJECT_ID, [$id]);
        return $this->result();
    }

    private function _devicesCntInSet($id)
    {
        $this->query(self::SQL_DEVICES_CNT_IN_SET, [$id], false);
        return $this->result();
    }

    private function _deviceGetById($id)
    {
        $this->query(self::SQL_DEVICE_GET, [$id], false);
        return $this->result();
    }

    private function _insertDevice($params)
    {
        $this->query(self::SQL_DEVICE_ADD, [
            $params['device_object_id'], $params['name'], $params['asset_code'],
            $params['serial_number'], $params['info'], date('Y-m-d H:i:s')
        ]);
        return $this->result();
    }

    private function _updateDevice($params)
    {
        $this->query(self::SQL_DEVICE_GET, [$params['device_id']], false);
        if (!$this->success) return $this->result();  // device not found

        $this->query(
            self::SQL_DEVICE_CHANGE,
            [
                $params['name'], $params['asset_code'], $params['serial_number'],
                $params['info'], $params['is_broken'], $params['device_id']
            ]
        );
        return $this->result();
    }

    private function _deleteDevice($id)
    {
        $this->query(self::SQL_DEVICE_GET, [$id], false);
        if (!$this->success) return $this->result();  // device not found

        $this->query(self::SQL_DEVICE_DELETE, [$id]);
        return $this->result();
    }
}
