<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


class SessionDB extends DB
{

    public static function checkSession($params)
    {
        $session_db = new SessionDB;
        $render_w = new RenderView;

        $get_session = $session_db->selectSessionFromDB(json_encode($params));

        if ($get_session) {
            $session_db->updateUserSessionToDB($params);
            $_SESSION['EDIT_VIEWS'] = true;
            return true;
        } else {
            if ($render_w->userLogin(array('username' => 'kasutaja', 'userpasswd' => 'kasutaja_parool'))) {
                $session_db->insertUserSessionToDB($params);
                $_SESSION['EDIT_VIEWS'] = true;
                return true;
            } else {
                unset($_SESSION['EDIT_VIEWS']);
                $_SESSION['EDIT_VIEWS'] = false;
                return false;
            }

        }

    }


    private function __construct()
    {
        parent::__construct();
    }


    private function insertUserSessionToDB($params)
    {
        $sql = 'INSERT INTO rbs__session (session_data,last_login) VALUES (?,?)';
        $this->query($sql, array(json_encode($params), date("Y-m-d H:i:s")));
    }
    private function updateUserSessionToDB($params)
    {
        $sql = 'UPDATE  rbs__session SET session_data, last_login VALUES (?,?)';
        $this->query($sql, array($params, date("Y-m-d H:i:s")));
    }

    private function selectSessionFromDB($params)
    {
        $sql = 'SELECT * FROM rbs__session WHERE session_data = ?';
        $data = $this->query($sql, array($params));

        if (@$data) {

            return true;
        }
    }

}
