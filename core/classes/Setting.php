<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


final class Setting extends DB
{
    /**
     * Returns specified or all settings
     *
     * @param   string  $setting
     *
     * @return  array
     */
    public static function getSettings(string $setting = ''): array
    {
        return (new Setting)->_getSettings($setting);
    }

    /**
     * Adds settings to the database or changes existing one
     *
     * @param   array  $params  Array of [<name>, <desc>]
     *
     * @return  array 
     */
    public static function addSettings(array $params): array
    {
        return (new Setting)->_insertSettings($params);
    }


    /**
     * Class `Settings` constructor
     */
    public function __construct()
    {
        parent::__construct();
    }


    /* ========================================================================
     *   Private methods
     * ========================================================================
     */

    private function _getSettings($setting)
    {
        if (!$setting) {
            $sql = self::SQL_SETTING_ALL;
            $fetch_all = true;
            $data = [];
        }
        else {
            $sql = self::SQL_SETTING_VALUE;
            $fetch_all = false;
            $data = [$setting];
        }
        $this->query($sql, $data, $fetch_all);
        return $this->result();
    }

    private function _insertSettings($params)
    {
        $this->_getSettings($params['name']);
        $data = [$params['name'], $params['desc']];

        if (!$this->success) $sql = self::SQL_SETTING_ADD;
        else {
            $sql = self::SQL_SETTING_CHANGE;
            $data[] = $params['name'];
        }
        $this->query($sql, $data);
        return $this->result();
    }
}
