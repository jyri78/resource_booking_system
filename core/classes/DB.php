<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GPL v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;

use \PDO;
use PDOException;


class DB extends SqlQuery
{
    private $connection;
    private $_debug;
    private $pdo_error = false;
    private $success;
    private $error_code;
    private $fetch_result;

    private const DB_GETTERS = ['_debug', 'pdo_error', 'success', 'error_code', 'fetch_result'];


    /**
     * DB class Magic getter.
     *
     * @param  string  $name
     * 
     * @return bool|int|object
     */
    public function __get(string $name)
    {
        if (!in_array($name, self::DB_GETTERS)) return 0;
        else return $this->{$name};
    }


    protected function __construct()
    {
        global $_CONFIG;

        $host = &$_CONFIG['db']['host'];
        $port = &$_CONFIG['db']['port'];
        $db = &$_CONFIG['db']['db_name'];
        $name = &$_CONFIG['db']['user'];
        $passwd = &$_CONFIG['db']['passwd'];
        $this->_debug = ($_CONFIG['allow_debug'] ?? false);

        // At the moment for Data Source Name (DSN) only MySQL is supported
        $dsn = "mysql:host={$host};";
        if ($port) $dsn .= "port={$port};";
        $dsn .= "dbname={$db};charset=utf8mb4";

        try {
            $this->connection = new PDO(
                $dsn, $name, $passwd,
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4',
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET utf8mb4',
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION   // kuvab errorid
                ]
            );
        }
        catch (\Exception $e) {
            $this->pdo_error = true;
            $this->error_code = -1;

            error_log(' *  RBS_PDO_ERROR: '. $e->getMessage());
        }
    }

    /**
     * Prepares SQL query and executes it
     *
     * @param   string  $sql        SQL query
     * @param   array   $data       Data passed to the query execution
     * @param   bool    $fetch_all  Do fetch all or next result from a result set on select
     */
    protected function query(string $sql, array $data = [], bool $fetch_all = true)
    {
        if (!$this->pdo_error) {
            $this->success = true;
            $this->error_code = 0;  // reset for special cases

            try {
                $stmt = $this->connection->prepare($sql);
                $stmt->execute($data);

                $_s = explode(' ', $sql)[0];

                if ($_s == 'SELECT') {
                    if ($fetch_all) {
                        $this->fetch_result = $stmt->fetchAll(PDO::FETCH_OBJ);
                        if (empty($this->fetch_result)) $this->_unsuccess(0);  // just no result
                    }
                    else {
                        $this->fetch_result = $stmt->fetch(PDO::FETCH_OBJ);
                        if (!$this->fetch_result) $this->_unsuccess(0);  // just no result
                    }
                }
                elseif ($_s == 'INSERT') {
                    $this->query('SELECT LAST_INSERT_ID() AS id', [], false);
                    $this->fetch_result = ((int) $this->fetch_result->id ?? 0);  // we need only ID
                    if (!$this->fetch_result) $this->_unsuccess();
                }
                else $this->fetch_result = 1;
            }
            catch(PDOException $e) {
                error_log(' *  RBS_SQL_ERROR: ' . $e->getMessage() . $this->_caller_func());
                error_log('    [SQL_DATA]: '. json_encode($data));
                $this->_unsuccess();
            }
        }
    }

    /**
     * Return result of last SQL query
     *
     * @param   int    $error_code
     *
     * @return  array
     */
    protected function result(int $error_code = 0): array
    {
        // if PDO connection error, no queries can be done
        if ($this->pdo_error) return ['success' => false, 'error' => -11];

        if ($error_code != 0) return ['success' => false, 'error' => $error_code];
        else {
            if ($this->success) return ['success' => true, 'result' => $this->fetch_result];
            else return ['success' => false, 'error' => ($this->error_code ?? 0)];
        }
    }

    /**
     * Return result of specified value
     *
     * @param   mixed  $value
     *
     * @return  array
     */
    protected function value($value): array
    {
        if ($this->success) return ['success' => true, 'result' => $value];
        return ['success' => false, 'error' => $this->error_code];
    }


    /* ========================================================================
     *   Private methods
     * ========================================================================
     */

    private function _unsuccess($err_code = -1)
    {
        $this->success = false;
        $this->error_code = $err_code;
    }

    private function _caller_func()
    {
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,5);
        $bd = $backtrace[2] ?? [];
        $cfn = (!$bd['class'] ? '' : "{$bd['class']}{$bd['type']}") . ($bd['function'] ?? '');
        if ($bd['class'] == 'DB') {
            $bd = $backtrace[3] ?? [];
            $cfn .= '; '. (!$bd['class'] ? '' : "{$bd['class']}{$bd['type']}") . ($bd['function'] ?? '');
            if ($bd['class'] == 'DB') {
                $bd = $backtrace[4] ?? [];
                $cfn .= '; '. (!$bd['class'] ? '' : "{$bd['class']}{$bd['type']}") . ($bd['function'] ?? '');
            }
        }
        return (!$cfn ? '' : '  |  '. trim($cfn, ' ()') .'()');
    }
}


abstract class SqlQuery
{
    private const _OBJECT__FROM_JOIN_WHERE =
        " FROM rbs__booking_object AS bo
            JOIN rbs__object_type AS ot ON bo.rbs__object_type__id = ot.id
            WHERE bo.deleted = '0'";

    private const _BOOKING__FROM_JOIN =
        ' FROM rbs__booking AS b
            JOIN rbs__booking_has_booking_object AS hbo ON b.id = hbo.rbs__booking__id
            JOIN rbs__booking_object AS bo ON hbo.rbs__booking_object__id = bo.id ';

    private const _USER__SEL = 'SELECT * FROM rbs__user WHERE ';
    private const _USER__GET = self::_USER__SEL ."deleted = '0' AND ";
    private const _USER__LAST_LOGIN = 'UPDATE rbs__user SET last_login = ? WHERE ';


    /* ========================================================================
     *   Class `Resource` queries
     * ========================================================================
     */

    protected const SQL_OBJECT_HBO_GET =
        'SELECT * FROM rbs__booking_has_booking_object WHERE rbs__booking_object__id = ?';

    protected const SQL_OBJECT_HBO_DELETE =
        'DELETE FROM rbs__booking_has_booking_object WHERE rbs__booking_object__id = ?';


    // protected const SQL_OBJECT_INFO_ADD = 'INSERT INTO rbs__object_info (info) VALUES (?)';


    protected const SQL_OBJECT_TYPE_GET_WO =    // ... with objects (containing at least one object)
        'SELECT ot.* FROM rbs__object_type AS ot
            JOIN rbs__booking_object AS bo ON ot.id = bo.rbs__object_type__id';


    protected const SQL_OBJECT_TYPES_ALL =
        "SELECT * FROM rbs__object_type WHERE deleted = '0'";

    protected const SQL_DEVICE_TYPES_ALL =
        self::SQL_OBJECT_TYPES_ALL .' AND NOT id=1';

    protected const SQL_DEVICE_MANUFACTURERS_ALL =
        "SELECT * FROM rbs__device_manufacturer WHERE deleted = '0'";

    protected const SQL_DEVICE_MODELS_ALL =
        "SELECT * FROM rbs__device_model WHERE deleted = '0'";


    protected const SQL_OBJECT_TYPE_GET = self::SQL_OBJECT_TYPES_ALL .' AND id = ?';

    protected const SQL_DEVICE_MANUFACTURER_GET =
        self::SQL_DEVICE_MANUFACTURERS_ALL . ' AND id = ?';

    protected const SQL_DEVICE_MODEL_GET = self::SQL_DEVICE_MODELS_ALL .' AND id = ?';


    protected const SQL_OBJECT_TYPES_WITH_CNT =   // ... count objects
        "SELECT ot.*, (
            SELECT count(bo.id) FROM rbs__booking_object AS bo
                JOIN rbs__object_type AS cot ON cot.id = bo.rbs__object_type__id
                WHERE cot.id = ot.id AND bo.deleted = '0'
        ) AS no_objects FROM rbs__object_type AS ot";

    protected const SQL_DEVICE_TYPES_WITH_CNT =
            self::SQL_OBJECT_TYPES_WITH_CNT .' WHERE NOT ot.id=1';

    protected const SQL_DEVICE_MANUFACTURERS_WITH_CNT =  // ... count objects
        'SELECT dm.*, (
            SELECT count(bo.id) FROM rbs__booking_object AS bo
                JOIN rbs__device_manufacturer AS cdm ON cdm.id = bo.rbs__device_manufacturer__id
                WHERE cdm.id = dm.id
        ) AS no_objects FROM rbs__device_manufacturer AS dm';

    protected const SQL_DEVICE_MODELS_WITH_CNT =  // ... count objects
        'SELECT dm.*, (
            SELECT count(bo.id) FROM rbs__booking_object AS bo
                JOIN rbs__device_model AS cdm ON cdm.id = bo.rbs__device_model__id
                WHERE cdm.id = dm.id
        ) AS no_objects FROM rbs__device_model AS dm';


    protected const SQL_OBJECT_TYPE_ADD =
        'INSERT INTO rbs__object_type (name, created) VALUES (?,?)';

    protected const SQL_DEVICE_MANUFACTURER_ADD =
        'INSERT INTO rbs__device_manufacturer (name, created) VALUES (?,?)';

    protected const SQL_DEVICE_MODEL_ADD =
        'INSERT INTO rbs__device_model (name, created) VALUES (?,?)';


    protected const SQL_OBJECT_TYPE_CHANGE =
        'UPDATE rbs__object_type SET name = ? WHERE id = ?';

    protected const SQL_DEVICE_MANUFACTURER_CHANGE =
        'UPDATE rbs__device_manufacturer SET name = ? WHERE id = ?';

    protected const SQL_DEVICE_MODEL_CHANGE =
        'UPDATE rbs__device_model SET name = ? WHERE id = ?';


    protected const SQL_OBJECT_TYPE_DELETE =
        'DELETE FROM rbs__object_type WHERE id = ?';

    protected const SQL_DEVICE_MANUFACTURER_DELETE =
        'DELETE FROM rbs__device_manufacturer WHERE id = ?';

    protected const SQL_DEVICE_MODEL_DELETE =
        'DELETE FROM rbs__device_model WHERE id = ?';


    protected const SQL_OBJECT_FIRST = 
        'SELECT bo.* FROM rbs__object_type AS ot
            JOIN rbs__booking_object AS bo ON ot.id = bo.rbs__object_type__id
            WHERE ot.id = ? LIMIT 1';  // doesn't really need since fetch is called only once

    protected const SQL_OBJECTS_COUNT =
        'SELECT count(bo.id) AS count'. self::_OBJECT__FROM_JOIN_WHERE;

    protected const SQL_OBJECTS_ALL =
        'SELECT bo.*'. self::_OBJECT__FROM_JOIN_WHERE .' AND ot.';

    protected const SQL_OBJECTS_ALL_EXT =
        'SELECT bo.*'. self::_OBJECT__FROM_JOIN_WHERE ." AND bo.not_for_booking = '0' AND ot.";

    protected const SQL_DEVICE_OBJECTS_ALL =
        "SELECT bo.*, ot.name AS device_type, dma.name AS device_manufacturer, dmo.name AS device_model, (
            SELECT count(d.id) FROM rbs__device AS d
                JOIN rbs__booking_object AS dbo ON dbo.id = d.rbs__booking_object__id
                WHERE dbo.id = bo.id AND d.deleted = '0'
        ) AS devices_count, (SELECT object_name FROM rbs__booking_object WHERE id = bo.rbs__room__id) AS room
            FROM rbs__booking_object AS bo
            JOIN rbs__object_type AS ot ON bo.rbs__object_type__id = ot.id
            JOIN rbs__device_manufacturer AS dma ON bo.rbs__device_manufacturer__id = dma.id
            JOIN rbs__device_model AS dmo ON bo.rbs__device_model__id = dmo.id
            WHERE bo.deleted = '0' AND NOT bo.rbs__object_type__id = 1";

    protected const SQL_DEVICE_OBJECT_GET = self::SQL_DEVICE_OBJECTS_ALL .' AND bo.id = ?';

    protected const SQL_DEVICE_FILE =
        'SELECT object_name, image_file, image_filename, image_type FROM rbs__booking_object WHERE id = ?';

    protected const SQL_ROOM_ADD =
        'INSERT INTO rbs__booking_object
                (object_name, info, not_for_booking, booking_color, rbs__object_type__id, created)
            VALUES (?,?,?,?,?,?)';

    protected const SQL_DEVICE_OBJECT_ADD =
        'INSERT INTO rbs__booking_object
                (object_name, info, objects_count, booking_color, rbs__object_type__id,
                rbs__device_manufacturer__id, rbs__device_model__id, rbs__room__id,
                image_file, image_filename, image_type, created)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';

    protected const SQL_OBJECT_GET = 'SELECT * FROM rbs__booking_object WHERE id = ?';

    protected const SQL_ROOM_CHANGE =
        'UPDATE rbs__booking_object
            SET object_name = ?, info = ?, not_for_booking = ?, booking_color = ?
            WHERE id = ?';

    protected const SQL_DEVICE_OBJECT_CHANGE =
        'UPDATE rbs__booking_object
            SET object_name = ?, info = ?, objects_count = ?, booking_color = ?, rbs__room__id = ?
            WHERE id = ?';

    protected const SQL_DEVICE_OBJECT_CHANGE_WI =
        'UPDATE rbs__booking_object
            SET object_name = ?, info = ?, objects_count = ?, booking_color = ?,
                rbs__room__id = ?, image_file = ?, image_filename = ?, image_type = ?
            WHERE id = ?';

    protected const SQL_OBJECT_DELETE = "UPDATE rbs__booking_object SET deleted = '1' WHERE id = ?";


    protected const SQL_DEVICE_IDS =
        "SELECT id FROM rbs__device WHERE deleted = '0' AND rbs__booking_object__id = ?";

    protected const SQL_DEVICES_BY_OBJECT_ID =
        "SELECT * FROM rbs__device WHERE deleted = '0' AND broken = '0' AND rbs__booking_object__id = ?";

    protected const SQL_DEVICES_CNT_IN_SET =
        "SELECT count(id) AS devices_count FROM rbs__device
            WHERE deleted = '0' AND broken = '0' AND rbs__booking_object__id = ?";

    protected const SQL_DEVICE_GET =
        "SELECT * FROM rbs__device WHERE deleted = '0' AND id = ?";

    protected const SQL_DEVICE_ADD =
        'INSERT INTO rbs__device
                (rbs__booking_object__id, name, asset_code, serial_number, info, created)
            VALUES (?,?,?,?,?,?)';

    protected const SQL_DEVICE_CHANGE =
        'UPDATE rbs__device SET name = ?, asset_code = ?, serial_number = ?, info = ?, broken = ?
            WHERE id = ?';

    protected const SQL_DEVICE_DELETE = "UPDATE rbs__device SET deleted = '1' WHERE id = ?";


    /* ========================================================================
     *   Class `ResourceBooking` queries
     * ========================================================================
     */

    protected const SQL_BOOKING_GET =
        'SELECT b.*'. self::_BOOKING__FROM_JOIN . "WHERE b.id = ?";

    protected const SQL_BOOKING_GET_IN_WEEK =
        'SELECT b.*, hbo.rbs__user__id'. self::_BOOKING__FROM_JOIN .
            "WHERE b.booking_from < ? AND b.booking_to > ? AND bo.id = ?";

    protected const SQL_BOOKING_GET_IN_DAY =
        'SELECT b.*'. self::_BOOKING__FROM_JOIN .
            'WHERE (b.booking_to BETWEEN ? AND ? OR b.booking_from BETWEEN ? AND ?) AND bo.id = ?';

    protected const SQL_BOOKING_FIND =
        'SELECT * FROM rbs__booking AS b 
            JOIN rbs__booking_has_booking_object hbo ON b.id = hbo.rbs__booking__id
            WHERE b.id = ?';

    protected const SQL_BOOKING_ADD =
        'INSERT INTO rbs__booking
                (title, booking_from, booking_to, booking_repeat, week_days, devices_count, devices_all)
            VALUES (?,?,?,?,?,?,?)';

    protected const SQL_BOOKING_HBO_ADD =
        'INSERT INTO rbs__booking_has_booking_object
                (rbs__booking__id, rbs__booking_object__id, rbs__user__id)
            VALUES (?,?,?)';

    protected const SQL_BOOKING_CHANGE =
        'UPDATE rbs__booking AS b
            JOIN rbs__booking_has_booking_object AS hbo ON b.id = hbo.rbs__booking__id
            SET b.title = ?, b.booking_from = ?, b.booking_to = ?, b.booking_repeat = ?,
                    b.week_days = ?, b.devices_count = ?, b.devices_all = ?
                    hbo.rbs__booking_object__id = ?, hbo.rbs__user__id = ?
            WHERE b.id = ?';

    protected const SQL_BOOKING_HBO_DELETE =
        'DELETE FROM rbs__booking_has_booking_object WHERE rbs__booking__id = ?';

    protected const SQL_BOOKING_DELETE = 'DELETE FROM rbs__booking WHERE id = ?';


    /* ========================================================================
     *   Class `Setting` queries
     * ========================================================================
     */

    protected const SQL_SETTING_VALUE =
        'SELECT setting_value FROM rbs__setting WHERE setting_name = ?';

    protected const SQL_SETTING_ALL = 'SELECT * FROM rbs__setting';

    protected const SQL_SETTING_ADD =
        'INSERT INTO rbs__setting (setting_name, setting_value) VALUES (?, ?)';

    protected const SQL_SETTING_CHANGE =
        'UPDATE rbs__setting SET setting_name = ?, setting_value = ? WHERE setting_name = ?';


    /* ========================================================================
     *   Class `User` queries
     * ========================================================================
     */

    protected const SQL_USERS_ALL =
        "SELECT u.id, u.first_name, u.last_name, u.username, u.email, u.created,
                u.last_login, r.id AS role_id, r.role_name
            FROM rbs__user AS u
            JOIN rbs__user_role AS r ON u.rbs__user_role__id = r.id
            WHERE u.deleted = '0'";

    protected const SQL_USER_GET_BY_EMAIL = self::_USER__GET .'email = ?';
    protected const SQL_USER_GET_BY_USERNAME = self::_USER__GET .'username = ?';

    protected const SQL_USER_SET_LL_BY_EMAIL = self::_USER__LAST_LOGIN .'email = ?';
    protected const SQL_USER_SET_LL_BY_USENAME = self::_USER__LAST_LOGIN .'username = ?';

    protected const SQL_USER_GET = self::SQL_USERS_ALL .' AND u.id = ?';
    protected const SQL_USER_SEL_BY_EMAIL = self::_USER__SEL .'email = ?';
    protected const SQL_USER_SEL_BY_USERNAME = self::_USER__SEL .'username = ?';

    protected const SQL_USER_ADD =
        'INSERT INTO rbs__user
                (first_name, last_name, username, email, password, created, rbs__user_role__id)
            VALUES (?,?,?,?,?,?,?)';

    protected const SQL_USER_PWD_CHANGE = 'UPDATE rbs__user SET password = ? WHERE id = ?';

    protected const SQL_USER_CHANGE =
        'UPDATE rbs__user
            SET first_name = ?, last_name = ?, username = ?, email = ?, rbs__user_role__id = ?
            WHERE id = ?';

    protected const SQL_USER_DELETE = "UPDATE rbs__user SET deleted = '1' WHERE id = ?";
}
