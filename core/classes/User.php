<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


final class User extends DB
{
    /**
     * Tries to log user in and returns true if it was successful
     *
     * @param   array  $params  Array of [<username>, <userpasswd>]
     *
     * @return  bool
     */
    public static function login(array $params): bool
    {
        return (new User)->_logIn($params);
    }

    /**
     * Returns user data by ID
     *
     * @param   int    $user_id
     *
     * @return  array
     */
    public static function getUser(int $user_id): array
    {
        return (new User)->_selectUser($user_id);
    }

    /**
     * Returns data of all users
     *
     * @return  array
     */
    public static function getAllUsers(): array
    {
        return (new User)->_selectUsers();
    }

    /**
     * Adds new user to the database
     *
     * @param   array  $params  Array of [<first_name>, <last_name>, <username>, <email>, <userpasswd>, <user_role>]
     *
     * @return  array
     */
    public static function addUser(array $params): array
    {
        return (new User)->_insertUser($params);
    }

    /**
     * Changes user password or data
     *
     * @param   array  $params  Array of [<user_id>, <userpasswd>] or [<user_id>, <first_name>, <last_name>, <username>, <email>, <user_role>]
     *
     * @return  array
     */
    public static function updateUserData(array $params): array
    {
        return (new User)->_updateUserData($params);
    }

    /**
     * Deletes user from database
     *
     * @param   int    $user_id
     *
     * @return  array
     */
    public static function deleteUser(int $user_id): array
    {
        return (new User)->_deleteUser($user_id);
    }


    /* ========================================================================
     *   Private methods
     * ========================================================================
     */

    private function __construct()
    {
        parent::__construct();
    }


    private function _logIn($params)
    {
        if (filter_var($params['username'], FILTER_VALIDATE_EMAIL)) {
            $sql_sel = self::SQL_USER_GET_BY_EMAIL;
            $sql_ll = self::SQL_USER_SET_LL_BY_EMAIL;
        }
        else {
            $sql_sel = self::SQL_USER_GET_BY_USERNAME;
            $sql_ll = self::SQL_USER_SET_LL_BY_USENAME;
        }

        $verifed_passwd = false;
        $this->query($sql_sel, [$params['username']], false);

        if ($this->success)
            $verifed_passwd = password_verify($params['userpasswd'], $this->fetch_result->password);

        if ($verifed_passwd) {
            $_SESSION['USER_DATA'] = $this->fetch_result;  // needs to be saved here before next query
            $this->query($sql_ll, [date("Y-m-d H:i:s"), $params['username']]);  // save last login date
            $_SESSION['EDIT_VIEWS'] = true;
            return true;
        } else {
            $_SESSION['EDIT_VIEWS'] = false;
            return false;
        }

    }

    private function _selectUser($user_id)
    {
        $this->query(self::SQL_USER_GET, [$user_id], false);
        return $this->result();
    }

    private function _selectUsers()
    {
        $this->query(self::SQL_USERS_ALL);
        return $this->result();
    }

    private function _insertUser($params)
    {
        $hashed_pw = password_hash($params['userpasswd'], PASSWORD_BCRYPT);
        $has_email = false;
        $has_username = false;
        $is_email_deleted = false;
        $is_username_deleted = false;

        if (!empty($params['username'])) {
            $this->query(self::SQL_USER_SEL_BY_USERNAME, [$params['username']], false);
            
            if ($this->success) {
                $has_username = true;
                $is_username_deleted = $this->fetch_result->deleted == '1';
            }
        }
        if (!empty($params['email'])) {
            $this->query(self::SQL_USER_SEL_BY_EMAIL, [$params['email']], false);

            if ($this->success) {
                $has_email = true;
                $is_email_deleted = $this->fetch_result->deleted == '1';
            }
        }

        if ($has_email || $has_username) {
            if (!$has_email) $err_code = ($is_username_deleted ? 6 : 1);  // only "username" exists
            elseif (!$has_username) $err_code = ($is_email_deleted ? 7 : 2);  // only "email" exists
            else $err_code = ($is_email_deleted ?
                ($is_username_deleted ? 3 : 5) : ($is_username_deleted ? 4 : 0)
            );  // both exists

            error_log($err_code);
            return $this->result($err_code);
        }

        $this->query(self::SQL_USER_ADD, [
            $params['first_name'], $params['last_name'], $params['username'],
            $params['email'], $hashed_pw, date("Y-m-d H:i:s"), $params['user_role']
        ]);
        
        return $this->result();
    }

    private function _updateUserData($params)
    {
        if (isset($params['userpasswd'])) {
            $sql = self::SQL_USER_PWD_CHANGE;
            $data = [password_hash($params['userpasswd'], PASSWORD_BCRYPT), $params['user_id']];
        }
        else {
            $sql = self::SQL_USER_CHANGE;
            $data = [
                $params['first_name'], $params['last_name'], $params['username'],
                $params['email'], $params['user_role'], $params['user_id']
            ];
            // TODO: on email/username change check if not in use
        }

        $this->query($sql, $data);
        return $this->result();
    }

    private function _deleteUser($user_id)
    {
        $this->query(self::SQL_USER_GET, [$user_id]);
        if (!$this->success) return $this->result();  // user not found

        $this->query(self::SQL_USER_DELETE, [$user_id]);
        return $this->result();
    }
}
