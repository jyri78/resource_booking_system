<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


/**
 * Class for connection between front-end and back-end.
 * 
 * All functions return bool or array of `["success" => true, "result" => …]`
 * or `["success" => false, "error" => {code}]`
 */
class RenderView
{
    /* ========================================================================
     *   GENERAL functions
     * ========================================================================
     */

    /**
     * Tries to log user in and returns true if it was successful
     *
     * @param   array  $params  Array of [<username>, <userpasswd>]
     *
     * @return  bool
     */
    public function userLogin(array $params): bool
    {
        return User::login($params);
    }

    /**
     * Logs user out
     *
     * @return  bool    true
     */
    public function logOut(): bool
    {
        // return LogOut::userLogout();
        unset($_SESSION['USER_DATA']);
        $_SESSION['EDIT_VIEWS'] = false;
        return true;
    }

    /**
     * Returns specified or all settings
     *
     * @param   string  $setting
     *
     * @return  array
     */
    public function getSettings(string $setting = ''): array
    {
        return Setting::getSettings($setting);
    }

    /**
     * Returns user data by ID
     *
     * @param   int    $user_id
     *
     * @return  array
     */
    public function getUser(int $user_id): array
    {
        return User::getUser($user_id);
    }

    /**
     * Returns data of all users
     *
     * @return  array
     */
    public function getAllUsers(): array
    {
        return User::getAllUsers();
    }

    /**
     * Checks user session
     *
     * @return  bool
     */
    public function sessionControl(): bool
    {
        return SessionDB::checkSession($_SESSION);
    }

    /**
     * Returns all resource types
     *
     * @return  array
     */
    public function getAllObjectsTypes(): array
    {
        return Resource::getAllObjectsTypes();
    }

    /**
     * Returns all device types
     *
     * @return  array
     */
    public function getAllDeviceTypes(): array
    {
        return Resource::getAllDeviceTypes();
    }

    /**
     * Returns number of all resources or number of resources by type
     *
     * @param   int    $type_id
     *
     * @return  array
     */
    public function getObjectsCount(int $type_id = 0): array
    {
        return Resource::getObjectsCount($type_id);
    }

    /**
     * Returns first resource (room or device) by type ID
     *
     * @param   int    $type_id
     *
     * @return  array
     */
    public function getFirstObjectByTypeID(int $type_id): array
    {
        return Resource::getFirstObjectByTypeID($type_id);
    }

    /**
     * Returns list of resource types (room or device) containing at least one resource
     *
     * @return  array
     */
    public function getObjectTypesWithObject(): array
    {
        return Resource::getObjectTypesWithObject();
    }


    /* ========================================================================
     *   CALENDAR functions
     * ========================================================================
     */

    /**
     * Returns all objects by type name
     *
     * @param   array  $params  Array of [<type_id>, <not_for_booking>]
     *
     * @return  array
     */
    public function objectsByType(array $params): array
    {
        return Resource::getObjectsByType($params);
    }

    /**
     * Returns all bookings in selected week
     *
     * @param   array  $params  Array of [<week_start>, <week_end>, <object_id>]
     *
     * @return  array
     */
    public function getBookings(array $params): array
    {
        return ResourceBooking::getBookings($params);
    }

    /**
     * Returns booking data by ID
     *
     * @param   int    $booking_id
     *
     * @return  array
     */
    public function getBooking(int $booking_id): array
    {
        return ResourceBooking::getBooking($booking_id);
    }

    /**
     * Returns all bookings by date in specified calendar
     *
     * @param   array  $params  Array of [<date>, <object_id>]
     *
     * @return  array
     */
    public function getBookingsByDate(array $params): array
    {
        return ResourceBooking::getBookingsByDate($params);
    }

    /**
     * Adds new booking to the database
     *
     * @param   array  $params  Array of [<user_id>, <object_id>, <title>, <start>, <end>, <repeat>, <days_of_week>, <devices_count>]
     *
     * @return  array
     */
    public function insertBooking(array $params): array
    {
        return ResourceBooking::insertBooking($params);
    }

    /**
     * Cahnges booking data
     *
     * @param   array  $params  Array of [<booking_id>, <user_id>, <title>, <start>, <end>, <repeat>, <object_id>, <days_of_week>, <count>]
     *
     * @return  array
     */
    public function updateBooking(array $params): array
    {
        return ResourceBooking::updateBooking($params);
    }

    /**
     * Deletes specific booking from database
     *
     * @param   int    $booking_id
     *
     * @return  array
     */
    public function deleteBooking(int $booking_id): array
    {
        return ResourceBooking::deleteBooking($booking_id);
    }


    /* ========================================================================
     *   ADMIN functions
     * ========================================================================
     */

    /**
     * Adds settings to the database or changes existing one
     *
     * @param   array  $params  Array of [<name>, <desc>]
     *
     * @return  array 
     */
    public function addSettings(array $params): array
    {
        return Setting::addSettings($params);
    }

    /**
     * Returns all deviceobjects; filtered by either `type_id`, `manufacturer_id`, `model_id` or `room_id`, or by all
     *
     * @param   array  [$params]  Array of [<type_id>, <manufacturer_id>, <model_id>, <room_id>]
     *
     * @return  array
     */
    public function getDeviceObjects(array $params = []): array
    {
        return Resource::getDeviceObjects($params);
    }

    /**
     * Returns devices file data
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public function getDeviceFile(int $device_id): array
    {
        return Resource::getDeviceFile($device_id);
    }

    /**
     * Adds new room to the database
     *
     * @param   array  $params  Array of [<name>, <info>, <not_for_booking>, <color>]
     *
     * @return  array
     */
    public function insertRoom(array $params): array
    {
        return Resource::insertRoom($params);
    }

    /**
     * Adds new divece object to the database
     *
     * @param   array  $params  Array of [<name>, <info>, <count>, <type_id>, <manufacturer_id>, <model_id>,
     *                                    <room_id>, <image_file>, <image_filename>, <image_type>]
     *
     * @return  array
     */
    public function insertDeviceObject(array $params): array
    {
        return Resource::insertDeviceObject($params);
    }

    /**
     * Updates room data
     *
     * @param   array  $params  Array of [<room_id>, <name>, <info>, <not_for_booking>, <color>]
     *
     * @return  array
     */
    public function changeRoom(array $params): array
    {
        return Resource::updateRoom($params);
    }

    /**
     * Updates device object data
     *
     * @param   array  $params  Array of [<device_id>, <name>, <info>, <count>, <color>,
     *                                    <room_id>, <image_file>, <image_filename>, <image_type>]
     *
     * @return  array
     */
    public function changeDeviceObject(array $params): array
    {
        return Resource::updateDeviceObject($params);
    }

    /**
     * Deletes resource (room or device)
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public function deleteObject(int $object_id): array 
    {
        return Resource::deleteObject($object_id);
    }

    /**
     * Returns device object by ID
     *
     * @param   int    $device_object_id
     *
     * @return  array
     */
    public function getDeviceObjectById(int $device_object_id): array 
    {
        return Resource::getDeviceObjectById($device_object_id);
    }

    /**
     * Returns device type by ID
     *
     * @param   int    $type_id
     *
     * @return  array
     */
    public function getDeviceTypeById(int $type_id): array 
    {
        if ($type_id < 2) return ['success' => false, 'error' => 0];
        return Resource::getObjectTypeById($type_id);
    }

    /**
     * Returns list of resource types and number of objects in type
     *
     * @return  array
     */
    public function getObjectTypesAndCntObjects(): array 
    {
        return Resource::getObjectTypesAndCntObjects();
    }

    /**
     * Returns list of device types and number of objects in type
     *
     * @return  array
     */
    public function getDeviceTypesAndCntObjects(): array 
    {
        return Resource::getDeviceTypesAndCntObjects();
    }

    /**
     * Adds new device type to the database
     *
     * @param   string  $type_name
     *
     * @return  array
     */
    public function insertDeviceType(string $type_name): array 
    {
        return Resource::insertDeviceType($type_name);
    }

    /**
     * Updates device type name
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public function updateDeviceType(array $params): array 
    {
        return Resource::updateDeviceType($params);
    }

    /**
     * Deletes device type from database (must have no connected devices)
     *
     * @param   int  $type_id
     *
     * @return  array
     */
    public function deleteDeviceType(int $type_id): array 
    {
        return Resource::deleteDeviceType($type_id);
    }

    /**
     * Returns all device manufacturers
     *
     * @return  array
     */
    public function getDeviceManufacturers(): array 
    {
        return Resource::getDeviceManufacturers();
    }

    /**
     * Returns device manufacturer by ID
     *
     * @param   int    $manufacturer_id
     *
     * @return  array
     */
    public function getDeviceManufacturerById(int $manufacturer_id): array
    {
        return Resource::getDeviceManufacturerById($manufacturer_id);
    }

    /**
     * Returns all device manufacturers and number of devices
     *
     * @return  array
     */
    public function getDeviceManufacturersAndCntObjects(): array 
    {
        return Resource::getDeviceManufacturersAndCntObjects();
    }

    /**
     * Adds new device manufacturer to the database
     *
     * @param   string  $manufacturer
     *
     * @return  array
     */
    public function insertDeviceManufacturer(string $manufacturer): array 
    {
        return Resource::insertDeviceManufacturer($manufacturer);
    }

    /**
     * Updates device manufacturers name
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public function updateDeviceManufacturer(array $params): array 
    {
        return Resource::updateDeviceManufacturer($params);
    }

    /**
     * Deletes device manufacturer from database (must have no connected devices)
     *
     * @param   int  $manufacturer_id
     *
     * @return  array
     */
    public function deleteDeviceManufacturer(int $manufacturer_id): array 
    {
        return Resource::deleteDeviceManufacturer($manufacturer_id);
    }

    /**
     * Returns all device models
     *
     * @return  array
     */
    public function getDeviceModels(): array 
    {
        return Resource::getDeviceModels();
    }

    /**
     * Returns device model by ID
     *
     * @param   int    $id
     *
     * @return  array
     */
    public function getDeviceModelById(int $id): array 
    {
        return Resource::getDeviceModelById($id);
    }

    /**
     * Returns all device models and number of devices
     *
     * @return  array
     */
    public function getDeviceModelsAndCntObjects(): array 
    {
        return Resource::getDeviceModelsAndCntObjects();
    }

    /**
     * Adds new device model to the database
     *
     * @param   string  $model
     *
     * @return  array
     */
    public function insertDeviceModel(string $model): array 
    {
        return Resource::insertDeviceModel($model);
    }

    /**
     * Updates device model
     *
     * @param   array  $params  Array of [<id>, <name>]
     *
     * @return  array
     */
    public function updateDeviceModel(array $params): array 
    {
        return Resource::updateDeviceModel($params);
    }

    /**
     * Deletes device model from database (must have no connected devices)
     *
     * @param   int  $id
     *
     * @return  array
     */
    public function deleteDeviceModel(int $id): array 
    {
        return Resource::deleteDeviceModel($id);
    }


    /**
     * Gets all device IDs by device object id
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public function getDeviceIds(int $object_id): array
    {
        return Resource::getDeviceIds($object_id);
    }

    /**
     * Gets devices data by device object ID
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public function getDevicesByObjectId(int $object_id): array
    {
        return Resource::getDevicesByObjectId($object_id);
    }

    /**
     * Gets devices count in set by device object ID
     *
     * @param   int    $object_id
     *
     * @return  array
     */
    public function getDevicesCountInSet(int $object_id): array
    {
        return Resource::getDevicesCountInSet($object_id);
    }

    /**
     * Gets device data by ID
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public function getDeviceById(int $device_id): array
    {
        return Resource::getDeviceById($device_id);
    }

    /**
     * Adds new device to the table
     *
     * @param   array  $params  Array of [<device_object_id>, <name>, <asset_code>, <serial_number>, <info>]
     *
     * @return  array
     */
    public function insertDevice(array $params): array
    {
        return Resource::insertDevice($params);
    }

    /**
     * Updates device data
     *
     * @param   array  $params  Array of [<device_id>, <name>, <asset_code>, <serial_number>, <info>, <is_broken>]
     *
     * @return  array
     */
    public function updateDevice(array $params): array
    {
        return Resource::updateDevice($params);
    }

    /**
     * Delete device
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public function deleteDevice(int $device_id): array
    {
        return Resource::deleteDevice($device_id);
    }


    /**
     * Adds new user to the database
     *
     * @param   array  $params  Array of [<first_name>, <last_name>, <username>, <email>, <userpasswd>, <user_role>]
     *
     * @return  array
     */
    public function insertUser(array $params): array
    {
        return User::addUser($params);
    }

    /**
     * Changes user password or data
     *
     * @param   array  $params  Array of [<user_id>, <userpasswd>] or [<user_id>, <first_name>, <last_name>, <username>, <email>, <user_role>]
     *
     * @return  array
     */
    public function updateUserData(array $params): array
    {
        return User::updateUserData($params);
    }

    /**
     * Deletes user from database
     *
     * @param   int    $user_id
     *
     * @return  array
     */
    public function deleteUser(int $user_id): array
    {
        return User::deleteUser($user_id);
    }
}
