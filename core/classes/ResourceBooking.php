<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


namespace rbs\core;


final class ResourceBooking extends DB
{
    /**
     * Returns all bookings in selected week
     *
     * @param   array  $params  Array of [<week_start>, <week_end>, <object_id>]
     *
     * @return  array
     */
    public static function getBookings(array $params): array
    {
        return (new ResourceBooking)->_selectBookings($params);
    }

    /**
     * Returns booking data by ID
     *
     * @param   int    $booking_id
     *
     * @return  array
     */
    public static function getBooking(int $booking_id): array
    {
        return (new ResourceBooking)->_selectBooking($booking_id);
    }

    /**
     * Returns all bookings by date in specified calendar
     *
     * @param   array  $params  Array of [<date>, <object_id>]
     *
     * @return  array
     */
    public static function getBookingsByDate(array $params): array
    {
        return (new ResourceBooking)->_getBookingsByDate($params);
    }

    /**
     * Adds new booking to the database
     *
     * @param   array  $params  Array of [<user_id>, <object_id>, <title>, <start>, <end>, <repeat>, <days_of_week>, <devices_count>]
     *
     * @return  array
     */
    public static function insertBooking(array $params): array
    {
        return (new ResourceBooking)->_addBooking($params);
    }

    /**
     * Cahnges booking data
     *
     * @param   array  $params  Array of [<booking_id>, <user_id>, <title>, <start>, <end>, <repeat>, <object_id>, <days_of_week>, <count>]
     *
     * @return  array
     */
    public static function updateBooking(array $params): array
    {
        return (new ResourceBooking)->_updateBooking($params);
    }

    /**
     * Deletes specific booking from database
     *
     * @param   int    $booking_id
     *
     * @return  array
     */
    public static function deleteBooking(int $booking_id): array
    {
        return (new ResourceBooking)->_deleteBooking($booking_id);
    }


    /**
     * Class `ObjectsBookings` constructor
     */


    /* ========================================================================
     *   Private methods
     * ========================================================================
     */

    private function __construct()
    {
        parent::__construct();
    }


    private function _selectBookings($params)
    {
        $this->query(
            self::SQL_BOOKING_GET_IN_WEEK,
            // [$params['week_start'], $params['week_start'], $params['object_id']]
            ["{$params['week_end']} 23:59:59", "{$params['week_start']} 00:00:00", $params['object_id']]
        );
        return $this->result();
    }

    private function _selectBooking($booking_id)
    {
        $this->query(self::SQL_BOOKING_GET, [$booking_id], false);
        return $this->result();
    }

    private function _getBookingsByDate($params)
    {
        $this->query(
            // self::SQL_BOOKING_GET_IN_DAY,
            // [
            //     "{$params['date']} 00:00:00", "{$params['date']} 23:59:59",
            //     "{$params['date']} 00:00:00", "{$params['date']} 23:59:59", $params['object_id']
            // ]
            self::SQL_BOOKING_GET_IN_WEEK,
            ["{$params['date']} 23:59:59", "{$params['date']} 00:00:00", $params['object_id']]
        );
        return $this->result();
    }

    private function _addBooking($params)
    {
        $this->query(
            self::SQL_BOOKING_ADD,
            [
                $params['title'], $params['start'], $params['end'],
                $params['repeat'], $params['days_of_week'],
                @$params['devices_count'], @$params['devices_all']
            ]
        );
        if (!$this->success) return $this->result();  // adding new booking has failed

        $_id = $this->fetch_result;  // "remember" newly added ID
        $this->query(
            self::SQL_BOOKING_HBO_ADD,
            [$_id, $params['object_id'], $params['user_id']]
        );

        // Update object data  TODO: review algorithm
        // $this->query(self::SQL_OBJECT_GET, [$params['object_id']], false);

        // $object = (int) $this->fetch_result->objects_count - $params['count'];
        // $locked = (int) $this->fetch_result->locked_count + $params['count'];

        // $this->query(self::SQL_BOOKING_OBJECT_CHANGE, [$object, $locked, $params['object_id']]);
        return $this->value($_id);  // special case
    }

    private function _updateBooking($params)
    {
        $this->query(self::SQL_BOOKING_FIND, [$params['booking_id']]);
        if (!$this->success) return $this->result();  // booking not found

        $this->query(self::SQL_BOOKING_CHANGE, [
            $params['title'], $params['start'], $params['end'],
            $params['repeat'], $params['days_of_week'],
            @$params['devices_count'], @$params['devices_all'],
            $params['object_id'], $params['user_id'], $params['booking_id']
        ]);
        return $this->result();
    }

    private function _deleteBooking($booking_id)
    {
        $this->query(self::SQL_BOOKING_FIND, [$booking_id]);
        if (!$this->success) return $this->result();

        $this->query(self::SQL_BOOKING_HBO_DELETE, [$booking_id]);
        if (!$this->success) return $this->result();  // error occured
        $this->query(self::SQL_BOOKING_DELETE, [$booking_id]);
        return $this->result();
    }
}
