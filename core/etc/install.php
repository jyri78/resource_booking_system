<?php
// Copyright (C) 2021 Andrus Kull
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GPL v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);


if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}
if (session_status() == PHP_SESSION_NONE) session_start();


$install = new Install;


/**
 * Class for app installation.
 */
class Install
{
    //! Some functions used in this App require at least PHP version 7.3
    public const MIN_PHP_VER_STR = '7.3';
    private const MIN_PHP_VER_ID = 70300;

    private const SQL_FILENAME = 'resource_booking_system.sql';
    private const CONFIG_FILENAME = 'config.ini';
    private const ERROR_FILENAME = 'error.log';

    private const SQL_QUERIES = [
        'user_role' => 'INSERT INTO rbs__user_role (id, role_name, created) VALUES (?,?,?)',
        'user'      => 'INSERT INTO rbs__user
                                (id, first_name, last_name, username, email, password, rbs__user_role__id, created, deleted)
                            VALUES (?,?,?,?,?,?,?,?,?)',
        'obj_type'  => 'INSERT INTO rbs__object_type (id, name, created) VALUES (?,?,?)',
        'object'    => 'INSERT INTO rbs__booking_object
                                (id, object_name, info, not_for_booking, rbs__object_type__id, created, deleted)
                            VALUES (?,?,?,?,?,?,?)',
        'setting'   => 'INSERT INTO rbs__setting (setting_name, setting_value) VALUES (?,?)'
    ];

    private $_etc;
    private $_sql_file;
    private $_config_file;
    private $_sitelink;

    private $_conn;
    private $_sth;
    private $_err;


    /**
     * Install class Magic getter
     *
     * @param  string  $name
     */
    public function __get(string $name)
    {
        if ($name == 'connection') return $this->_conn;
        elseif ($name == 'error_message') return $this->_err;
        elseif ($name == 'sitelink') return $this->_sitelink;
    }

    /**
     * Class Install constructor
     */
    public function __construct()
    {
        global $_etc;
        $this->_etc = $_etc;
        $this->_sql_file = $_etc . self::SQL_FILENAME;
        $this->_config_file = $_etc . self::CONFIG_FILENAME;
        $this->_sitelink = $_SERVER['SCRIPT_URL'] ?? $_SERVER['REQUEST_URI'];
    }


    /**
     * Checks PHP version compatibility
     *
     * @return  bool
     */
    public function checkPHP(): bool
    {
        return PHP_VERSION_ID > self::MIN_PHP_VER_ID;
    }

    /**
     * Returns OS architecture
     *
     * @return  string
     */
    public function getOSArch(): string
    {
        return (PHP_INT_SIZE == 4 ? '32bit' : '64bit');
    }

    /**
     * Connects to the database and returns PDO object
     *
     * @param   array  $params
     * 
     * @return  bool
     */
    public function dbConn(array $params = []): bool
    {
        $_pd = $_SESSION['DB_DATA'] ??
            ['db_host' => '', 'db_port' => 3306, 'db_name' => '', 'db_user' => '', 'db_pwd' => ''];

        $host = $params['db_host'] ?? $_pd['db_host'];
        $port = $params['db_host'] ?? $_pd['db_port'];
        $name = $params['db_name'] ?? $_pd['db_name'];
        $user = $params['db_user'] ?? $_pd['db_user'];
        $pwd = $params['db_pwd'] ?? $_pd['db_pwd'];

        $dsn = "mysql:host={$host};";
        if ($port != 3306) $dsn .= "port={$port};";
        $dsn .= "dbname={$name};charset=utf8mb4";

        try {
            $this->_conn = new PDO(
                $dsn, $user, $pwd,
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4',
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET utf8mb4',
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION   // kuvab errorid
                ]
            );
            return true;
        }
        catch (Exception $e) {
            $this->_err = $e->getMessage();
            return false;
        }
    }

    /**
     * Method to create tables to the database (from SQL file)
     * 
     * @return bool
     */
    public function createTables(): bool
    {
        try {
            $sql = $this->_readSQLFile();
            if (!$sql) return false;
            $this->_conn->prepare($sql)->execute();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * Method to insert required data to the database
     *
     * @param   array  $params
     *
     * @return  bool
     */
    public function insertData(array $params): bool
    {
        try {
            extract($params);
            $_date = date('Y-m-d H:i:s');

            $this->_prepare('obj_type');
            $result = $this->_insert([1, 'ruum', $_date]) && $result;

            $this->_prepare('user_role');
            $result = $this->_insert([1, 'Admin', $_date]);
            $result = $this->_insert([2, 'Kasutaja', $_date]) && $result;

            $this->_prepare('object');
            $result = $this->_insert([1, 'Arvutiklass', 'Arvutiklass', '0', 1, $_date, '0']) && $result;

            $this->_prepare('user');
            $result = $this->_insert([
                1, $f_name, $l_name, $username, $email,
                password_hash($pwd, PASSWORD_BCRYPT), '1', $_date, '0'
            ]) && $result;

            $this->_prepare('setting');
            $result = $this->_insert(['view_post_key', bin2hex(random_bytes(8)) .'-R85']) && $result;
            $result = $this->_insert(['view_navbar', 'light']) && $result;
            $result = $this->_insert(['table_show_ids', '0']) && $result;
            $result = $this->_insert([
                'calendar_options',
                '{"weekends":false,"slotMinTime":"07:00","slotMaxTime":"18:00","bhStartTime":"08:00","bhEndTime":"16:00"}'
            ]) && $result;

            return $result;
        }
        catch (Exception $e) {
            return false;
        }
    }

    /**
     * Method for getting database data from POST or SESSION
     *
     * @param   string  $name
     * @param   string  $default
     *
     * @return  string
     */
    public function getDbData(string $name, string $default = ''): string
    {
        $result = $_POST[$name] ?? '';
        if (!$result && isset($_SESSION['DB_DATA'])) $result = $_SESSION['DB_DATA'][$name] ?? '';
        if (!$result) $result = $default;
        return $result;
    }

    /**
     * Method for getting user data from POST or SESSION
     *
     * @param   string  $name
     *
     * @return  string
     */
    public function getUserData(string $name): string
    {
        $result = $_POST[$name] ?? '';
        if (!$result && isset($_SESSION['USER_DATA'])) $result = $_SESSION['USER_DATA'][$name] ?? '';
        return $result;
    }

    /**
     * Method for checking if folder "etc" is writable or not
     *
     * @return  bool
     */
    public function checkIsWritable(): bool
    {
        if (is_writable($this->_etc)) {
            $img_folder = "{$this->_etc}img";
            if (!file_exists($img_folder)) mkdir($img_folder, 0777);
            return true;
        }
        return false;
    }

    /**
     * Method for checking config file existence
     * 
     * @return  bool
     */
    public function checkSqlFile(): bool
    {
        return file_exists($this->_sql_file);
    }

    /**
     * Method for writing settings to the config file
     *
     * @return  bool
     */
    public function writeConfig(): bool
    {
        $data = "<?php
define('CORE_PATH', realpath(__DIR__ .'/../').DIRECTORY_SEPARATOR);
define('ROOT_PATH', dirname(CORE_PATH) . DIRECTORY_SEPARATOR);
define('TEMPLATE_PATH', dirname(CORE_PATH) . DIRECTORY_SEPARATOR . 'public');
define('SITELINK', '{$this->_sitelink}');

\$_CONFIG = [
    'root_url'  => SITELINK,
    'root_dir'  => ROOT_PATH,
    'secure'    => '" . bin2hex(random_bytes(20)) . "',
    'db'        => [
        'host'      => '{$_SESSION['DB_DATA']['db_host']}',";

        if ($_SESSION['DB_DATA']['db_port'] != 3306) $data .= "
        'port'      => '{$_SESSION['DB_DATA']['db_port']}',";

        $data .= "
        'db_name'   => '{$_SESSION['DB_DATA']['db_name']}',
        'user'      => '{$_SESSION['DB_DATA']['db_user']}',
        'passwd'    => '{$_SESSION['DB_DATA']['db_pwd']}'
    ]
];
";
        $result = file_put_contents($this->_config_file, $data) !== false;
        if ($result) {
            @chmod($this->_config_file, 0666);

            // Create also `error.log` file and output success message
            $_err_file = $this->_etc . self::CONFIG_FILENAME;
            $_date = date('Y-m-d H:i:s');

            file_put_contents($_err_file,
                "[{$_date}] *  =================================\n".
                "[{$_date}] *    RBS installeerimine õnnestus.\n".
                "[{$_date}] *  =================================\n"
            );
            @chmod($_err_file, 0666);
        }
        return $result;
    }


    /**
     * Method for killing session data
     */
    public function killSession()
    {
        $_SESSION = [];
        session_unset();
        session_regenerate_id();
        session_destroy();
    }


    /* ========================================================================
        Private helper function
       ========================================================================
     */
    private function _readSQLFile()
    {
        if (!file_exists($this->_sql_file)) return '';
        return file_get_contents($this->_sql_file);
    }

    public function _prepare(string $sql_name)
    {
        $this->_sth = $this->_conn->prepare( self::SQL_QUERIES[$sql_name] );
    }

    public function _insert(array $values): bool
    {
        return $this->_sth->execute($values);
    }
}

?>
<!DOCTYPE html>
<html lang="et">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
   <title>Resursside broneerimise süsteemi installer</title>
</head>
<body>


<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="activity" fill="currentColor" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M6 2a.5.5 0 0 1 .47.33L10 12.036l1.53-4.208A.5.5 0 0 1 12 7.5h3.5a.5.5 0 0 1 0 1h-3.15l-1.88 5.17a.5.5 0 0 1-.94 0L6 3.964 4.47 8.171A.5.5 0 0 1 4 8.5H.5a.5.5 0 0 1 0-1h3.15l1.88-5.17A.5.5 0 0 1 6 2Z"/>
    </symbol>
    <symbol id="gear" fill="currentColor" viewBox="0 0 16 16">
        <path d="M8 4.754a3.246 3.246 0 1 0 0 6.492 3.246 3.246 0 0 0 0-6.492zM5.754 8a2.246 2.246 0 1 1 4.492 0 2.246 2.246 0 0 1-4.492 0z"/>
        <path d="M9.796 1.343c-.527-1.79-3.065-1.79-3.592 0l-.094.319a.873.873 0 0 1-1.255.52l-.292-.16c-1.64-.892-3.433.902-2.54 2.541l.159.292a.873.873 0 0 1-.52 1.255l-.319.094c-1.79.527-1.79 3.065 0 3.592l.319.094a.873.873 0 0 1 .52 1.255l-.16.292c-.892 1.64.901 3.434 2.541 2.54l.292-.159a.873.873 0 0 1 1.255.52l.094.319c.527 1.79 3.065 1.79 3.592 0l.094-.319a.873.873 0 0 1 1.255-.52l.292.16c1.64.893 3.434-.902 2.54-2.541l-.159-.292a.873.873 0 0 1 .52-1.255l.319-.094c1.79-.527 1.79-3.065 0-3.592l-.319-.094a.873.873 0 0 1-.52-1.255l.16-.292c.893-1.64-.902-3.433-2.541-2.54l-.292.159a.873.873 0 0 1-1.255-.52l-.094-.319zm-2.633.283c.246-.835 1.428-.835 1.674 0l.094.319a1.873 1.873 0 0 0 2.693 1.115l.291-.16c.764-.415 1.6.42 1.184 1.185l-.159.292a1.873 1.873 0 0 0 1.116 2.692l.318.094c.835.246.835 1.428 0 1.674l-.319.094a1.873 1.873 0 0 0-1.115 2.693l.16.291c.415.764-.42 1.6-1.185 1.184l-.291-.159a1.873 1.873 0 0 0-2.693 1.116l-.094.318c-.246.835-1.428.835-1.674 0l-.094-.319a1.873 1.873 0 0 0-2.692-1.115l-.292.16c-.764.415-1.6-.42-1.184-1.185l.159-.291A1.873 1.873 0 0 0 1.945 8.93l-.319-.094c-.835-.246-.835-1.428 0-1.674l.319-.094A1.873 1.873 0 0 0 3.06 4.377l-.16-.292c-.415-.764.42-1.6 1.185-1.184l.292.159a1.873 1.873 0 0 0 2.692-1.115l.094-.319z"/>
    </symbol>
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
    <symbol id="exclamation-octagon-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM8 4c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995A.905.905 0 0 1 8 4zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
</svg>

<div class="container my-5">
    <h1>Ressursside broneerimise süsteemi esmapaigaldus</h1>

    <div class="row row-cols-1 mt-5">
        <div class="col mb-4">
            <div class="card">
                <h5 class="card-header user-select-none">
                    <svg class="bi flex-shrink-0 me-2" width="22" height="22" role="img" aria-label="Activity:">
                        <use xlink:href="#activity"/>
                    </svg>

                    &nbsp;Esmased andmed
                    &nbsp; &nbsp; <span class="small opacity-50">(<small><?=
                            $install->sitelink ?></small>)</span>
                </h5>

                <form method="post">
                    <div class="card-body">
                        <div class="row border-bottom pt-3 mb-3">
                            <div class="col-6 mb-3 fw-bold user-select-none">
                                PHP versiooni kontroll
                            </div>

                            <div class="col-6 mb-3"><?php

/// Check PHP version

if (!isset($_SESSION['php_ver'])) $_SESSION['php_ver'] = $install->checkPHP();

if ($_SESSION['php_ver']):
?>

                                <div class="text-success fw-bold">PHP versioon: <?=
                                        phpversion() ?></div><?php
else:
?>

                                <div class="text-danger fw-bold">RBS rakendus vajab oma tööks PHP ver <b><?=
                                        $install::MIN_PHP_VER_STR ?></b> või uuemat</div>
                            </div><!-- class="col" -->
                        </div><!-- class="row" -->
                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

    die;  //! STOP here, PHP too old

endif;
//* END of PHP version check

?>

                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                Kausta <b>/core/etc</b> kirjutusõiguse kontroll
                            </div>

                            <div class="col-6 mb-3"><?php

/// Check if folder "etc" is writable

if ($install->checkIsWritable()):
?>

                                <div class="text-success fw-bold">korras</div><?php
else:
?>

                                <div class="text-danger fw-bold">
                                    RBS rakendus vajab oma tööks kaustale kirjutusõigust
                                </div>
                            </div><!-- class="col" -->
                        </div><!-- class="row" -->
                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

    die;  //! STOP here, folder "etc" not writable

endif;
//* END of folder writability chech

?>

                            </div>

                            <div class="col-12 mt-1 border-top">&nbsp;</div><?php


/// Ask for DB data and check connection

$db_host = $install->getDbData('db_host');
$db_port = $install->getDbData('db_port', '3306');
$db_name = $install->getDbData('db_name');
$db_user = $install->getDbData('db_user');
$db_pwd = $install->getDbData('db_pwd');

?>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="db_host">Andmebaasi aadress (<i>host</i>)</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="db_host" name="db_host" value="<?=
                                        $db_host ?>" placeholder="Andmebaasi aadress">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="db_port">Andmebaasi port</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="db_port" name="db_port" value="<?=
                                        $db_port ?>" placeholder="Andmebaasi port">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="db_name">Andmebaasi nimi</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="db_name" name="db_name" value="<?=
                                        $db_name ?>" placeholder="Andmebaasi nimi">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="db_user">Andmebaasi kasutajanimi</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="db_user" name="db_user" value="<?=
                                        $db_user ?>" placeholder="Andmebaasi kasutajanimi">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="db_pwd">AB kasutaja parool</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="password" id="db_pwd" name="db_pwd" value="<?=
                                        $db_pwd ?>" placeholder="Andmebaasi kasutaja parool">
                            </div>
                        </div><!-- class="row" --><?php

// Button "Next"
if (!@$_SESSION['db_conn'] && empty($_POST['db_check'])):
?>

                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="db_check" value="Edasi" class="btn btn-primary">
                    </div><?php
endif;

if (isset($_POST['db_check'])):
    if ($install->dbConn($_POST)):
        $_SESSION['db_conn'] = true;
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Ühendus andmebaasiga on korras.
                            </div>
                        </div><?php

        if ($install->checkSqlFile()):
            $_SESSION['db_file'] = true;
            $_SESSION['DB_DATA'] = $_POST;
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Andmebaasi installeerimise SQL-fail leitud.
                            </div>
                        </div><?php

            if ($install->createTables()):
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Rakenduse tabelid lisatud andmebaasi.
                            </div>
                        </div><?php

            else: //// if ($install->createTables()):
?>


                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Tabelite lisamine andmebaasi ebaõnnestus!
                            </div>
                        </div>
                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

                die;  //! STOP here, installation (creating tables) failed

            endif; //// if ($install->createTables()):

        else: //// if ($install->checkSqlFile()):
?>


                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Andmebaasi installeerimise SQL-faili ei leitud!
                            </div>
                        </div>
                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

            die;  //! STOP here, SQL file not found

        endif; //// if ($install->checkSqlFile()):

        // if ($_SESSION['db_file']): 
        // endif;

    else: //// if ($install->dbConn($_POST)):

        $_SESSION['db_conn'] = false;
?>


                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Andmebaasiga ei saanud ühendust: <?= $install->error_message ?>
                            </div>
                        </div>
                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="db_check" value="Edasi" class="btn btn-primary">
                    </div>
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

        die;  //! STOP here, no connection with database

    endif; //// if ($install->dbConn($_POST)):

endif; //// if (isset($_POST['db_check'])):

//* END of DB connection check

?>

                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" --><?php


/// Ask for Admin data and check inputs

if (@$_SESSION['db_conn']):
    $f_name = $install->getUserData('f_name');
    $l_name = $install->getUserData('l_name');
    $username = $install->getUserData('username');
    $email = $install->getUserData('email');
    $pwd = $install->getUserData('pwd');
    $pwd2 = $install->getUserData('pwd2');
?>


        <div class="col mb-4">
            <div class="card">
                <h5 class="card-header user-select-none">
                    <svg class="bi flex-shrink-0 me-2" width="22" height="22" role="img" aria-label="Gear:">
                        <use xlink:href="#gear"/>
                    </svg>

                    &nbsp;Admin andmed
                </h5>

                <form method="post">
                    <div class="card-body">
                        <div class="row border-top border-bottom pt-3 mb-3">
                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="f_name">Eesnimi</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="f_name" name="f_name" value="<?=
                                        $f_name ?>" placeholder="Eesnimi">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="l_name">Perekonnanimi</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="l_name" name="l_name" value="<?=
                                        $l_name ?>" placeholder="Perekonnanimi">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="username">Kasutajanimi</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="text" id="username" name="username" value="<?=
                                        $username ?>" placeholder="Kasutajanimi">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="email">Email</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="email" id="email" name="email" value="<?=
                                        $email ?>" placeholder="Email">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="pwd">Parool</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="password" id="pwd" name="pwd" value="<?=
                                        $pwd ?>" placeholder="Parool">
                            </div>

                            <div class="col-6 mb-2 fw-bold user-select-none">
                                <label for="pwd2">Korda parooli</label>
                            </div>
                            <div class="col-6 mb-3">
                                <input type="password" id="pwd2" name="pwd2" value="<?=
                                        $pwd2 ?>" placeholder="Korda parooli">
                            </div>
                        </div><!-- class="row" --><?php

    // Button "Next"
    if (!@$_SESSION['user_check'] && empty($_POST['user_check'])):
?>

                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="user_check" value="Edasi" class="btn btn-primary">
                    </div><?php
    endif;

    if (isset($_POST['user_check'])):

        if (
            !empty($_POST['f_name']) && !empty($_POST['l_name']) &&
            !empty($_POST['username']) && !empty($_POST['email']) &&
            !empty($_POST['pwd'])
        ):

            if ($_POST['pwd'] == $_POST['pwd2']):
                $_SESSION['user_check'] = true;
                $_SESSION['USER_DATA'] = $_POST;
                $install->dbConn();  // make connection to the database

                if ($install->insertData($_POST)):
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Esmased andmed lisatud andmebaasi.
                            </div>
                        </div><?php
                
                else: //// if ($install->insertData($_POST)):
?>


                        <div class="alert alert-warning d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Warning:">
                                <use xlink:href="#exclamation-triangle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Osa või kõiki andmeid ei õnnestunud andmebaasi lisada.
                            </div>
                        </div><?php

                endif; //// if ($install->insertData($_POST)):

            else: //// if ($_POST['pwd'] == $_POST['pw2d']):
?>


                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Sisestatud paroolid on erinevad
                            </div>
                        </div>
                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="user_check" value="Edasi" class="btn btn-primary">
                    </div>
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php
                die;  //! STOP here, inserted passwords are different

            endif; //// if ($_POST['pwd'] == $_POST['pw2d']):

        else:
?>

                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Üks või mitu välja olid tühjad. Kõik on kohustuslikud!
                            </div>
                        </div>
                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="user_check" value="Edasi" class="btn btn-primary">
                    </div>
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php
            die;  //! STOP here, one or more inputs were empty

        endif;

    endif; //// if (isset($_POST['user_check'])):
?>

                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" --><?php

endif; //// if (@$_SESSION['db_conn']):

//* END of Admin data check (and data insertion)


/// Create config.ini file

if (@$_SESSION['user_check']):
?>


        <div class="col mb-4">
            <div class="card">
                <h5 class="card-header user-select-none">
                    <svg class="bi flex-shrink-0 me-2" width="22" height="22" role="img" aria-label="Gear:">
                        <use xlink:href="#gear"/>
                    </svg>

                    &nbsp;Faili <b>config.ini</b> loomine
                </h5>

                <form method="post">
                    <div class="card-body"><?php

    if ($install->writeConfig()):
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                Seadete fail <b>config.ini</b> loodud.
                            </div>
                        </div><?php

    else: //// if ($install->writeConfig()):
?>


                        <div class="alert alert-danger d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">
                                <use xlink:href="#exclamation-octagon-fill"/>
                            </svg>

                            <div class="pl-3">
                                Nüüd läks küll midagi halvasti, seadete faili <b>config.ini</b> loomine ebaõnnestus. Järgmisel korral alustatakse paigaldamisprotsess uuesti.
                            </div>
                        </div>
                    </div><!-- class="card-body" -->
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" -->
    </div><!-- class="row" -->
</div><!-- class="container" -->


</body>
</html><?php

        $install->killSession();

        die;  //! STOP here, creating config.ini failed

    endif; //// if ($install->writeConfig()):
?>


                        <div class="alert alert-success d-flex align-items-center my-1" role="alert">
                            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                                <use xlink:href="#check-circle-fill"/>
                            </svg>

                            <div class="pl-3">
                                RBS rakenduse paigaldamine läks korda. Palun kliki nupul "Värskenda lehte"!
                            </div>
                        </div>
                    </div><!-- class="card-body" -->

                    <div class="card-footer d-flex justify-content-end">
                        <input id="next" type="submit" name="refresh_page" value="Värskenda lehte" class="btn btn-primary">
                    </div>
                </form>
            </div><!-- class="card" -->
        </div><!-- class="col" --><?php
endif;
?>

    </div><!-- class="row" -->
</div><!-- class="container" -->


<?php

if (isset($_POST['refresh_page'])) {
    $install->killSession();
    header("Location: {$_SERVER['REQUEST_URI']}");
}
?>

</body>
</html>
