-- Copyright (C) 2021 Andrus Kull
-- Copyright (C) 2022 Jüri Kormik
-- 
-- This software is released under the GPL v3 License.
-- https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


--
-- Table structure for table `rbs__booking`
--

CREATE TABLE `rbs__booking` (
  `id` INT NOT NULL,
  `title` TEXT NOT NULL,
  `booking_from` DATETIME NOT NULL,
  `booking_to` DATETIME NOT NULL,
  `booking_repeat` enum('true','false') NOT NULL,
  `week_days` TEXT DEFAULT NULL,
  `devices_count` INT(3) DEFAULT NULL,
  `devices_all` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__booking_has_booking_object`
--

CREATE TABLE `rbs__booking_has_booking_object` (
  `rbs__booking__id` INT NOT NULL,
  `rbs__booking_object__id` INT NOT NULL,
  `rbs__user__id` INT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__booking_object`
--

CREATE TABLE `rbs__booking_object` (
  `id` INT NOT NULL,
  `object_name` VARCHAR(48) NOT NULL,
  `info` LONGTEXT NOT NULL,
  `objects_count` INT DEFAULT 1,
  `not_for_booking` enum('0','1') NOT NULL DEFAULT '0',
  `booking_color` VARCHAR(32) DEFAULT NULL,
  `rbs__object_type__id` INT NOT NULL,
  `rbs__device_manufacturer__id` INT DEFAULT NULL,
  `rbs__device_model__id` INT DEFAULT NULL,
  `rbs__room__id` INT DEFAULT NULL,  -- self-referencing constraint
  `image_file` VARCHAR(96) DEFAULT NULL,
  `image_filename` VARCHAR(128) DEFAULT NULL,
  `image_type` INT(3) DEFAULT NULL,
  `created` DATETIME NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__device`
--

CREATE TABLE `rbs__device` (
  `id` INT NOT NULL,
  `rbs__booking_object__id` INT NOT NULL,
  `name` VARCHAR(48) NOT NULL,
  `asset_code` VARCHAR(24) DEFAULT NULL,
  `serial_number` VARCHAR(24) DEFAULT NULL,
  `info` LONGTEXT DEFAULT NULL,
  `broken` enum('0','1') NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__device_manufacturer`
--

CREATE TABLE `rbs__device_manufacturer` (
  `id` INT NOT NULL,
  `name` VARCHAR(48) NOT NULL,
  `created` DATETIME NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__device_model`
--

CREATE TABLE `rbs__device_model` (
  `id` INT NOT NULL,
  `name` VARCHAR(48) NOT NULL,
  `created` DATETIME NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__object_type`
--

CREATE TABLE `rbs__object_type` (
  `id` INT NOT NULL,
  `name` VARCHAR(48) NOT NULL,
  `created` DATETIME NOT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__session`
--

-- CREATE TABLE `rbs__session` (
--   `id` INT NOT NULL,
--   `session_data` LONGTEXT NOT NULL,
--   `last_login` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__setting`
--

CREATE TABLE `rbs__setting` (
  `id` INT NOT NULL,
  `setting_name` VARCHAR(24) NOT NULL,
  `setting_value` TEXT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__user`
--

CREATE TABLE `rbs__user` (
  `id` INT NOT NULL,
  `first_name` VARCHAR(64) NOT NULL,
  `last_name` VARCHAR(64) NOT NULL,
  `username` VARCHAR(48) DEFAULT NULL,
  `email` VARCHAR(64) DEFAULT NULL,
  `password` VARCHAR(64) DEFAULT NULL,
  `rbs__user_role__id` INT NOT NULL,
  `last_login` TIMESTAMP NULL DEFAULT NULL,
  `created` DATETIME DEFAULT NULL,
  `updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rbs__user_role`
--

CREATE TABLE `rbs__user_role` (
  `id` INT NOT NULL,
  `role_name` VARCHAR(32) NOT NULL,
  `created` DATETIME NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_estonian_ci;


-- ============================================================================


--
-- Indexes for table `rbs__booking`
--
ALTER TABLE `rbs__booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__booking_has_booking_object`
--
ALTER TABLE `rbs__booking_has_booking_object`
  ADD PRIMARY KEY (`rbs__booking__id`,`rbs__booking_object__id`,`rbs__user__id`),
  ADD KEY `fk__booking_has_booking_object__booking_object__idx` (`rbs__booking_object__id`),
  ADD KEY `fk__booking_has_booking_object__booking__idx` (`rbs__booking__id`),
  ADD KEY `fk__booking_has_booking_object__user__idx` (`rbs__user__id`);

--
-- Indexes for table `rbs__booking_object`
--
ALTER TABLE `rbs__booking_object`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk__booking_object__object_type__idx` (`rbs__object_type__id`),
  ADD KEY `fk__booking_object__device_manufacturer__idx` (`rbs__device_manufacturer__id`),
  ADD KEY `fk__booking_object__device_model__idx` (`rbs__device_model__id`),
  ADD KEY `fk__booking_object__booking_object__idx` (`rbs__room__id`);

--
-- Indexes for table `rbs__object_type`
--
ALTER TABLE `rbs__object_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__device`
--
ALTER TABLE `rbs__device`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk__device__booking_object__idx` (`rbs__booking_object__id`);

--
-- Indexes for table `rbs__device_manufacturer`
--
ALTER TABLE `rbs__device_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__device_model`
--
ALTER TABLE `rbs__device_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__session`
--
-- ALTER TABLE `rbs__session`
--   ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__setting`
--
ALTER TABLE `rbs__setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rbs__user`
--
ALTER TABLE `rbs__user`
  ADD PRIMARY KEY (`id`,`rbs__user_role__id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `fk__user__user_role__idx` (`rbs__user_role__id`);

--
-- Indexes for table `rbs__user_role`
--
ALTER TABLE `rbs__user_role`
  ADD PRIMARY KEY (`id`);


-- ============================================================================


--
-- AUTO_INCREMENT for table `rbs__booking`
--
ALTER TABLE `rbs__booking`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__booking_object`
--
ALTER TABLE `rbs__booking_object`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__object_type`
--
ALTER TABLE `rbs__object_type`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__device`
--
ALTER TABLE `rbs__device`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__device_manufacturer`
--
ALTER TABLE `rbs__device_manufacturer`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__device_model`
--
ALTER TABLE `rbs__device_model`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__session`
--
-- ALTER TABLE `rbs__session`
--   MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__setting`
--
ALTER TABLE `rbs__setting`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__user`
--
ALTER TABLE `rbs__user`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `rbs__user_role`
--
ALTER TABLE `rbs__user_role`
  MODIFY `id` INT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;


-- ============================================================================


--
-- Constraints for table `rbs__booking_has_booking_object`
--
ALTER TABLE `rbs__booking_has_booking_object`
  ADD CONSTRAINT `fk__booking_has_booking_object__booking_object` FOREIGN KEY (`rbs__booking_object__id`) REFERENCES `rbs__booking_object` (`id`),
  ADD CONSTRAINT `fk__booking_has_booking_object__booking` FOREIGN KEY (`rbs__booking__id`) REFERENCES `rbs__booking` (`id`),
  ADD CONSTRAINT `fk__booking_has_booking_object__user` FOREIGN KEY (`rbs__user__id`) REFERENCES `rbs__user` (`id`);

--
-- Constraints for table `rbs__booking_object`
--
ALTER TABLE `rbs__booking_object`
  ADD CONSTRAINT `fk__booking_object__object_type` FOREIGN KEY (`rbs__object_type__id`) REFERENCES `rbs__object_type` (`id`),
  ADD CONSTRAINT `fk__booking_object__device_manufacturer` FOREIGN KEY (`rbs__device_manufacturer__id`) REFERENCES `rbs__device_manufacturer` (`id`),
  ADD CONSTRAINT `fk__booking_object__device_model` FOREIGN KEY (`rbs__device_model__id`) REFERENCES `rbs__device_model` (`id`),
  ADD CONSTRAINT `fk__booking_object__booking_object` FOREIGN KEY (`rbs__room__id`) REFERENCES `rbs__booking_object` (`id`);

--
-- Constraints for table `rbs__device`
--
ALTER TABLE `rbs__device`
  ADD CONSTRAINT `fk__device__booking_object` FOREIGN KEY (`rbs__booking_object__id`) REFERENCES `rbs__booking_object` (`id`);

--
-- Constraints for table `rbs__user`
--
ALTER TABLE `rbs__user`
  ADD CONSTRAINT `fk__user__user_role` FOREIGN KEY (`rbs__user_role__id`) REFERENCES `rbs__user_role` (`id`);
