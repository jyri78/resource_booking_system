# _Resource Booking System_ for Schools

[![Bitbucket](https://img.shields.io/badge/Bitbucket-%230747A6.svg?logo=bitbucket&logoColor=white)](https://bitbucket.org/jyri78/resource_booking_system/)  [![GNU v3 Licence](https://img.shields.io/badge/License-GPL-blue.svg)](https://opensource.org/licenses/GPL-3.0)  [![Visual Studio Code](https://badgen.net/badge/icon/Visual%20Studio%20Code?icon=visualstudio&label)](https://code.visualstudio.com/)  [![MySQL](https://img.shields.io/badge/MySQL-%2300758F.svg?logo=mysql&logoColor=white)](https://www.mysql.com/)  [![PHP](https://img.shields.io/badge/PHP-%23777BB4.svg?logo=php&logoColor=white)](https://www.php.net/)  [![JavaScript](https://img.shields.io/badge/JavaScript-%23323330.svg?logo=javascript&logoColor=%23F7DF1E)](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/)  [![HTML5](https://img.shields.io/badge/HTML5-%23E34F26.svg?logo=html5&logoColor=white)](https://html.spec.whatwg.org/multipage/)  [![CSS3](https://img.shields.io/badge/CSS3-%231572B6.svg?logo=css3&logoColor=white)](https://www.w3.org/Style/CSS/)

**_[README eesti keeles](https://bitbucket.org/jyri78/resource_booking_system/src/master/README.et.md)_**

This project started as [selection internship](https://bitbucket.org/andruskull/school_booking_system/) and is continued as part of my diploma thesis.

## Installation

Copy the files to the web server (preferably in a separate folder) and open it with a web browser. The installation process starts automatically, fill in the required fields, and if all goes well, you can start using it.

## Licence

[GPL v3](https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE)  ([OSI](https://opensource.org/licenses/GPL-3.0))
