<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GPL v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


declare(strict_types=1);
include_once 'core/start.php';
