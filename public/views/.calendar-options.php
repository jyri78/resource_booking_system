<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$_fc_options = rbs_get_calendar_options();
$_fc_lessons = rbs_get_lesson_times();
$_show_filter = rbs_show_filter();

$options = [
    'locale'                => 'et',
    'initialView'           => 'timeGridWeek',
    'height'                => '80vh', //auto
    'themeSystem'           => 'bootstrap5',
    'slotLabelClassNames'   => 'text-black-50 user-select-none',
    'nowIndicator'          => true,
    'dayMaxEvents'          => true,
    'expandRows'            => true,
    'weekNumbers'           => true,
    'displayEventTime'      => false,
    'weekends'              => $_fc_options['weekends'] ?? false,
    'slotMinTime'           => $_fc_options['slotMinTime'] ?? '07:00',
    'slotMaxTime'           => $_fc_options['slotMaxTime'] ?? '18:00',

    'businessHours'         => [
        'daysOfWeek'    => [ 1, 2, 3, 4, 5 ],
        'startTime'     => $_fc_options['bhStartTime'] ?? '08:00',
        'endTime'       => $_fc_options['bhEndTime'] ?? '16:00',
    ],

    'lessons'               => $_fc_lessons,
    'selectable'            => rbs_is_user_ok() && isset($_SESSION['CALENDAR_FILTER_OBJECT_ID']),
                                                                    // require at least one object to book
    'eventSources'          => [[
        'url'               => RBS_WEB_URL,
        'method'            => 'POST',
        'extraParams'       => [
            'rbs'       => RBS_AJAX_KEY,
            'task'      => 'calendar_bookings_source'
        ],
        'failure'           => null,
        // 'color'             => 'yellow',  // a non-ajax option
        // 'textColor'         => 'black'    // a non-ajax option
        /// https://www.rapidtables.com/web/css/css-color.html
        /// https://www.quackit.com/css/color/charts/css_color_names_chart.cfm
    ]],

    'customButtons'     => [
        'filterCalendar'    => [
            'text'                  => "Vali kalender  │  {$_SESSION['CALENDAR_FILTER_OBJECT_NAME']}",
            'click'                 => null
        ]
    ],

    'headerToolbar'         => [
        'left'          => ($_show_filter ? 'filterCalendar' : ''),
        'center'        => 'title',
        'right'         => 'today prev,next'//'today,prevYear,prev,next,nextYear'
    ],

    // 'showNonCurrentDates'   => false,
    // 'editable'              => true,
    // 'themeSystem'           => 'bootstrap',
    // 'eventContent'          => ['html' => '<i>Test</i>'/*, 'domNodes' => ''*/],
    // 'slotMinWidth'          => 80,
    // 'slotLabelContent'      => 'test',
    // 'slotLaneContent'       => ['html' => '<div style="margin-left:-35px">test</div>'],
    // 'slotDuration'          => '00:15',

    // 'views' => [
    //     'timeGridWeek' => [
    //         'titleFormat' => ['year' => 'numeric', 'month' => '2-digit', 'day' => '2-digit']
    //     ],
    // ],
];