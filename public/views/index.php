<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="generator" content="Resource Booking System v<?= RBS_APP_VERSION ?>">
    <script src="<?= RBS_WEB_URL .'?js='. RBS_JS_VERSION .'_'. $rbs_settings['page']['js_ext'] ?>"></script>
    <link rel="stylesheet" href="<?= RBS_WEB_URL .'?css='. RBS_CSS_VERSION ?>">
    <title><?= $rbs_settings['page']['title'] ?> | Ressursside broneerimise süsteem</title>
</head>
<body>


<?php
include_once(RBS_VIEW_PATH .'page/_header.php');
?>

<div class="container my-5">
    <div aria-live="polite" aria-atomic="true" class="position-relative" style="z-index:9999">
        <div class="toast-container position-absolute top-50 start-50 translate-middle p-3">
            <div id="error_toast" class="toast align-items-center text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-body">
                    <div class="d-flex">
                        <strong class="me-auto">VIGA!</strong>
                        <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div id="error_text" class="mt-2 pt-2 border-top border-warning"></div>
                </div>
            </div>
            <div id="success_toast" class="toast align-items-center text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-body">
                    <div class="d-flex">
                        <strong class="me-auto">Edu!</strong>
                        <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div id="success_text" class="mt-2 pt-2 border-top border-info"></div>
                </div>
            </div>
        </div>
    </div>

<?php 
include_once("page/{$page['name']}.php");
?>
</div>

<footer class="container-fluid fixed-bottom bg-<?= $page['nav_bg'] ?> bg-opacity-75 border-top small text-center p-1">
    &copy; 2022 <!-- <a href="https://jyri78.eu/" class="text-decoration-none" target="_blank"> -->Jüri Kormik<!-- </a> --> &nbsp; &nbsp; | &nbsp; &nbsp; &copy; 2021 Mikk Herde, Jüri Kormik, Andrus Kull, Karmo Lugima &nbsp; &nbsp; | &nbsp; &nbsp;
    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAJcEhZcwAALiMAAC4jAXilP3YAAAS3aVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjUuMCI+CiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIgogICAgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIgogICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIgogICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIgogICB0aWZmOkltYWdlTGVuZ3RoPSIxNiIKICAgdGlmZjpJbWFnZVdpZHRoPSIxNiIKICAgdGlmZjpSZXNvbHV0aW9uVW5pdD0iMiIKICAgdGlmZjpYUmVzb2x1dGlvbj0iMzAwLzEiCiAgIHRpZmY6WVJlc29sdXRpb249IjMwMC8xIgogICBleGlmOlBpeGVsWERpbWVuc2lvbj0iMTYiCiAgIGV4aWY6UGl4ZWxZRGltZW5zaW9uPSIxNiIKICAgZXhpZjpDb2xvclNwYWNlPSI2NTUzNSIKICAgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIKICAgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9IkdJTVAgYnVpbHQtaW4gc1JHQiIKICAgeG1wOk1vZGlmeURhdGU9IjIwMjItMDEtMjlUMTg6Mzg6MTUrMDI6MDAiCiAgIHhtcDpNZXRhZGF0YURhdGU9IjIwMjItMDEtMjlUMTg6Mzg6MTUrMDI6MDAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJwcm9kdWNlZCIKICAgICAgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWZmaW5pdHkgUGhvdG8gMS4xMC40IgogICAgICBzdEV2dDp3aGVuPSIyMDIyLTAxLTI5VDE4OjM4OjE1KzAyOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cjw/eHBhY2tldCBlbmQ9InIiPz6fS7xQAAAAMFBMVEX///////////9HcEz//f3////13N7vyczJTFXFPkjVcHfhkZbosLPagoj77e7QZGt1QhpJAAAABXRSTlPGCAcAxhbCQiUAAABjSURBVAjXYzBkDQ1LDQ1QZgAyTvZAGNFzVx4FM04ejZ0OZvSGhv4HMdin5a+rTAAyuP+t6H1xAMjgujb9RP4GIMNr+7SKLFTGdXSGb/+0ivQCIIMvZ19FKsTS0AwYI/xpgDIAy/0xVH/CMs4AAAAASUVORK5CYII=" alt="">
    <i>TLÜ Haapsalu kolledž</i> &nbsp; &nbsp; | &nbsp; &nbsp; <a class="link-<?=
            $page['nav_bg'] == 'light' ? 'primary' : 'info' ?>" href="https://choosealicense.com/licenses/gpl-3.0/" target="_blank">GPL v3</a>
</footer>


</body>
</html>