<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_ADMIN')) {
    header('Location: ../../');
    exit;
}


class AdminAjaxSetting
{
    
    /* ============================================================================
     *  Site settings
     * ============================================================================
     */

    /**
     * Save fullcalendar options
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveCalendarOptions(array $post): array
    {
        try {
            $allow_debug = rbs_get_setting('allow_debug');

            $options = [
                'weekends'      => (isset($post['rbs_weekends']) ? true : false),
                'slotMinTime'   => $post['rbs_slot_min_time'],
                'slotMaxTime'   => $post['rbs_slot_max_time'],
                'bhStartTime'   => $post['rbs_bh_start_time'],
                'bhEndTime'     => $post['rbs_bh_end_time'],
            ];
            rbs_clear_js_cache();  // Some settings depend on it

            return [
                'result' => rbs_write_setting('calendar_options', $options, true),
                'debug'  => ($allow_debug ? compact('post', 'options') : false)
            ];
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Save booking options
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveBookingOptions($post): array
    {
        try {
            $allow_debug = rbs_get_setting('allow_debug');

            $options = [
                'allow_repeat'  => $post['rbs_allow_repeat'],  // TODO: add extra options if needed
            ];

            return [
                'result' => rbs_write_setting('booking_options', $options, true),
                'debug'  => ($allow_debug ? compact('post', 'options') : false)
            ];
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Saves site lesson times
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveLessonTimes(array $post): array
    {
        try {
            $allow_debug = rbs_get_setting('allow_debug');

            $lessons = [];

            for ($i = 0; $i < 10; $i++) {
                if (isset($post["rbs_select_lesson_$i"]))
                    $lessons["{$i}."] = [
                        'nr' => $i,
                        'begin' => $post["rbs_lesson_begin_$i"],
                        'end' => $post["rbs_lesson_end_$i"]
                    ];
            }
            return [
                'result' => rbs_write_setting('lesson_times', $lessons, true),
                'debug'  => ($allow_debug ? compact('post', 'lessons') : false)
            ];
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Save header brand settings
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveHeaderBrand($post): array
    {
        try {
            $allow_debug = rbs_get_setting('allow_debug');

            $options = [
                'institution_name'  => $post['rbs_settings_header_brand_institution_name'],
                'style'             => (int)$post['rbs_settings_header_brand_style'],
                'bg_color'          => $post['rbs_settings_header_brand_color'],
            ];

            return [
                'result' => rbs_write_setting('header', $options, true),
                'debug'  => ($allow_debug ? compact('post', 'options') : false)
            ];
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }


    /* ============================================================================
    *  Users
    * ============================================================================
    */

    /**
     * Saves new user
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveUser(array $post): array
    {
        global $render_view;

        try {
            if (!filter_var($post['rbs_email'], FILTER_VALIDATE_EMAIL)) {
                return [
                    'result' => false,
                    'error' => '|rbs_email=Palun sisesta korrektne e-maili aadress!',
                    //'debug'  => ($rbs_settings['allow_debug'] ? $_data : false)
                ];
            }
            else {
                $_data = [
                    'first_name' => filter_var($post['rbs_firstname'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'last_name'  => filter_var($post['rbs_lastname'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'username'   => filter_var($post['rbs_username'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'email'      => filter_var($post['rbs_email'], FILTER_SANITIZE_EMAIL),
                    'user_role'  => filter_var($post['rbs_role'], FILTER_SANITIZE_NUMBER_INT)
                ];

                if (isset($post['rbs_user_id'])) {
                    $_data['user_id'] = $post['rbs_user_id'];
                    $result = $render_view->updateUserData($_data);
                }
                else {
                    $_data['userpasswd'] = filter_var($post['rbs_password'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
                    $result = $render_view->insertUser($_data);
                }
                $_data['id'] = ($_data['user_id'] ?? 0);

                return rbs_render_response([$result, 'id', $_data], [
                        'See kasutajanimi ja e-mail on juba kasutuses.',
                        'Selle kasutajanimega kasutaja on juba olemas.',
                        'Selle e-mailiga kasutaja on juba olemas.',
            /* 3 */   'See kasutajanimi ja e-mail on juba kasutuses, aga mõlemal juhul on kasutaja(d) kustutatud.',
                        'See kasutajanimi ja e-mail on juba kasutuses, kuid selle kasutajanimega kasutaja on kustutatud.',
                        'See kasutajanimi ja e-mail on juba kasutuses, kuid selle e-mailiga kasutaja on kustutatud.',
            /* 6 */   'Selle kasutajanimega kasutaja on juba olemas, aga kustutatud.',
                        'Selle e-mailiga kasutaja on juba olemas, aga kustutatud.',
                    ]);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes user from database
     *
     * @param   int    $user_id
     *
     * @return  array
     */
    public static function deleteUser(int $user_id): array
    {
        global $render_view;

        try {

            $result = $render_view->deleteUser($user_id);
            return rbs_render_response([$result, 'id', $user_id], ['Kasutaja kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

}