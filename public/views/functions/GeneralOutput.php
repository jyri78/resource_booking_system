<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_GENERAL')) {
    header('Location: ../../');
    exit;
}


class GeneralOutput
{

    /**
     * Returns SVG icon string
     *
     * @param   array  [$param]   Array of [<name>, <size>, <fill>, <class>, <style>]
     *
     * @return  string
     */
    public static function getIcon(array $param = []): string
    {
        if (!isset($param['name']) || !array_key_exists($param['name'], RBS_ICONS))
            $param['name'] = 'app';

        if (!isset($param['size'])) $param['size'] = 16;
        if (!isset($param['fill']) && !isset($param['class'])) $param['fill'] = 'currentColor';
        if (!isset($param['style'])) $param['style'] = '';

        if (!isset($param['class'])) $param['class'] = '';
        else $param['class'] .= ' ';

        $icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"{$param['size']}\" height=\"{$param['size']}\"";
        if (isset($param['fill'])) $icon .= " fill=\"{$param['fill']}\"";
        $icon .= " class=\"{$param['class']}bi bi-{$param['name']}\"";
        if (isset($param['style'])) $icon .= " style=\"{$param['style']}\"";
        $icon .= " viewBox=\"0 0 16 16\">";

        return $icon . RBS_ICONS[$param['name']] .'</svg>';
    }

    /**
     * Renders response from RenderView class functions (DB queries)
     *
     * @param   array  $input      Array of 'response', 'result key' and 'post data'
     * @param   array  $error_msg
     *
     * @return  array
     */
    public static function renderResponse(array $input, array $error_msgs): array
    {
        $allow_debug = rbs_get_setting('allow_debug') && rbs_is_admin();  /// debug messages can see only admin

        if ($input[0]['success']) {
            $return = [
                'result'  => true,
                'debug'   => ($allow_debug ? $input[2] : false)
            ];
            $return[$input[1]] = $input[0]['result'];
        }
        else {
            $return = [
                'result' => false,
                'debug'  => ($allow_debug ? $input[2] : false)
            ];
            $return[$input[1]] = '-';

            if ($input[0]['error'] == -11) $return['error'] = 'Andmebaasiga ei saanud ühendust.';
            elseif ($input[0]['error'] == -1) $return['error'] = 'SQL-päringu viga.';
            else {
                $_cnt = count($error_msgs) + 1;

                for ($i = $_cnt; $i > -1; $i--) {
                    if (isset($error_msgs[$i]) && $input[0]['error'] == $i) {
                        $return['error'] = $error_msgs[$i];
                        break;
                    }
                }

                if (!isset($return['error']))  // just in case (should not happen)
                    $return['error'] = $error_msgs[0] ?? 'Tekkis tundmatu viga...';
            }
        }
        return $return;
    }


    /**
     * Prints out page device types filter
     *
     * @param   string  $id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_type]
     */
    public static function printDeviceTypes(
            string $id, string $text, int $no_tabs = 0, int $sel_type = -1
        )
    {
        global $render_view;

        self::_printSelectOptions(
            $render_view->getAllDeviceTypes(), $id, $text, $no_tabs, $sel_type
        );
    }

    /**
     * Prints out page device manufacturers filter
     *
     * @param   string  $id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_manufacturer]
     */
    public static function printDeviceManufacturers(
            string $id, string $text, int $no_tabs = 0, int $sel_manufacturer = -1
        )
    {
        global $render_view;

        self::_printSelectOptions(
            $render_view->getDeviceManufacturers(), $id, $text, $no_tabs, $sel_manufacturer
        );
    }

    /**
     * Prints out page device models filter
     *
     * @param   string  $id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_model]
     */
    public static function printDeviceModels(
            string $id, string $text, int $no_tabs = 0, int $sel_model = -1
        )
    {
        global $render_view;

        self::_printSelectOptions(
            $render_view->getDeviceModels(), $id, $text, $no_tabs, $sel_model
        );
    }

    /**
     * Prints out page device locations filter
     *
     * @param   string  $id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_location]
     */
    public static function printDeviceLocations(
            string $id, string $text, int $no_tabs = 0, int $sel_location = -1
        )
    {
        global $render_view;

        self::_printSelectOptions(
            $render_view->objectsByType(['type_id' => 1, 'not_for_booking' => false]),
            $id, $text, $no_tabs, $sel_location, 'object_name'
        );
    }


    /**
     * Prints out page devices table
     *
     * @param   int  [$no_tabs]
     * 
     * @return  array
     */
    public static function printDevicesTable(int $no_tabs = 0): array
    {
        $to_print = [];
        $ids = [];
        $_devices = rbs_get_device_objects( rbs_get_device_filters(false) );
        $_is_admin = rbs_is_admin();

        $_table_show_ids = rbs_get_setting('table_show_ids') && $_is_admin;

        foreach($_devices as $device_id => $device) {
            $ids[] = $device_id;

            $_no_objects = (int)$device->devices_count;  // TODO: maybe check booked devices!?
            $_cant_delete = $_no_objects > 0;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr class="align-middle">', 'replaces' => []];

            $_table_show_ids && $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##ID##</td>',
                'replaces' => ['##ID##' => $device_id]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td><img class="img-thumbnail rounded-3" src="##THUMB##" role="button" data-rbs-image-id="##ID##" /></td>',
                'replaces' => [
                        '##ID##'    => $device_id,
                        '##THUMB##' => RBS_WEB_URL . "?thumb_{$device_id}"
                        // '##THUMB##' => 'data:'. mime_content_type($_img) .';base64,'. base64_encode( file_get_contents($_img) )
                    ]
            ];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string'   => '<td>##NAME##</td>',
                'replaces' => ['##NAME##' => $device->object_name]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##TYPE##</td>',
                'replaces' => ['##TYPE##' => $device->device_type]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##MANUFACTURER##</td>',
                'replaces' => ['##MANUFACTURER##' => $device->device_manufacturer]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##MODEL##</td>',
                'replaces' => ['##MODEL##' => $device->device_model]
            ];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##LOCATION##</td>',
                'replaces' => [
                    '##LOCATION##' => $device->room ?: '<small class="text-muted">(asukoht puudub)</small>'
                ]
            ];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##CNT_DEVICES##</td>',
                'replaces' => [
                    '##CNT_DEVICES##' => $device->devices_count . (
                            $_is_admin ? " <span class=\"text-muted small\">/ {$device->objects_count}</span>" : ''
                        )
                ]
            ];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##INFO##</td>',
                'replaces' => ['##INFO##' => $device->info]
            ];

            if ($_is_admin) {
                $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];
                $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="btn-group btn-group-sm">', 'replaces' => []];

                $to_print[] = [
                    'no_tabs'  => $no_tabs + 3,
                    'string'   => '<button type="button" id="rbs_devices_list_##ID##" class="btn btn-secondary me-2 rounded-end">##LIST_ICON##</button>',
                    'replaces' => [
                        '##ID##'        => $device_id,
                        '##LIST_ICON##' => rbs_get_icon(['name' => 'list-check'])
                    ]
                ];

                $to_print[] = [
                    'no_tabs'  => $no_tabs + 3,
                    'string'   => '<button type="button" id="rbs_device_edit_##ID##" class="btn btn-primary rounded-start">##EDIT_ICON##</button>',
                    'replaces' => [
                        '##ID##'        => $device_id,
                        '##EDIT_ICON##' => rbs_get_icon(['name' => 'pencil-square'])
                    ]
                ];
                $to_print[] = [
                    'no_tabs'  => $no_tabs + 3,
                    'string'   => '<button type="button"##ID## class="btn btn-sm btn-##OPACITY##"##DISABLED##>##DEL_ICON##</button>',
                    'replaces' => [
                        '##ID##'       => ($_cant_delete ? '' : " id=\"rbs_device_delete_{$device_id}\""),
                        '##OPACITY##'  => ($_cant_delete ? 'outline-danger opacity-25' : 'danger'),
                        '##DISABLED##' => ($_cant_delete ? ' disabled' : ''),
                        '##DEL_ICON##' => rbs_get_icon(['name' => 'trash'])
                    ]
                ];

                $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
                $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            }
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</tr>', 'replaces' => []];
        }
        rbs_print_data($to_print);
        return $ids;
    }


    /**
     * Converts local datestring to ISO date and vice versa
     *
     * @param   array  $file
     */
    public static function outputImage(array $file)
    {
        global $render_view;

        $result = $render_view->getDeviceFile($file[0]);

        if ($result['success']) {
            $img_file = RBS_IMG_FLDR . $result['result']->image_file . $file[1];

            if (file_exists($img_file)) {
                header('Content-type: '. mime_content_type($img_file));
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: '. filesize($img_file));

                ob_end_flush();
                readfile($img_file);
            }
        }
        exit;
    }


    // ========================================================================


    /*
     * Helper function for printing options for HTML select
     */
    private static function _printSelectOptions(
            array $data, string $id, string $text, int $no_tabs,
            int $sel_id, string $col_name = 'name'
        )
    {
        $to_print = [
            [
                'no_tabs'   => $no_tabs,
                'string'    => "<select class=\"form-select form-select-sm fw-bold\" id=\"{$id}\">",
                'replaces'  => []
            ],
            [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option class="fw-bold" value="-1"##SELECTED##>##NAME##</option>',
                'replaces'  => [
                    '##SELECTED##'  => ($sel_id == -1 ? ' selected' : ''),
                    '##NAME##'      => $text
                ]
            ]
        ];

        if ($col_name == 'object_name')
            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option class="text-muted" value="0"##SELECTED##>(asukoht puudub)</option>',
                'replaces'  => ['##SELECTED##'  => (!$sel_id ? ' selected' : '')]
            ];

        if ($data['success']) {
            foreach ($data['result'] as $_data) {
                $to_print[] = [
                    'no_tabs'   => $no_tabs + 1,
                    'string'    => '<option value="##ID##"##SELECTED##>##NAME##</option>',
                    'replaces'  => [
                        '##ID##'        => $_data->id,
                        '##SELECTED##'  => ($_data->id == $sel_id ? ' selected' : ''),
                        '##NAME##'      => $_data->{$col_name}
                    ]
                ];
            }
        }

        $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</select>', 'replaces' => []];
        rbs_print_data($to_print);
    }

}