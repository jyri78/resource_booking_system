<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}
define("RBS_APP_CLS_GENERAL", true);  // for General classes

foreach(['Data', 'Output', 'Ajax'] as $cls)
    require_once "General{$cls}.php";


/**
 * Clears Javascript cache
 */
function rbs_clear_js_cache()
{
    $settings = @file_get_contents(RBS_JS_CACHE_DATA) ?? '';

    if ($settings) {
        $settings = json_decode($settings, true);
        unlink($settings['file']);
        unlink($settings['file_d']);
        unlink(RBS_JS_CACHE_DATA);
    }
}

/**
 * Sets default filters for calendar
 */
function rbs_default_filter()
{
    global $render_view;

    $obj_types = rbs_get_object_types();
    $result = $render_view->getFirstObjectByTypeID(1);

    if ($result['success']) {
        $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_ID'] = 1;
        $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_NAME'] = $obj_types[1]->name;
        $_SESSION['CALENDAR_FILTER_OBJECT_ID'] = $result['result']->id;
        $_SESSION['CALENDAR_FILTER_OBJECT_NAME'] = $result['result']->object_name;
    }
}

/**
 * Returns device page filter valueas
 * 
 * @param   bool    [$all]
 * 
 * @return  array
 */
function rbs_get_device_filters(bool $all = true): array
{
    if ($all) return [
            'type_id'           => $_SESSION['DEVICES_FILTER_TYPE_ID'] ?? -1,
            'manufacturer_id'   => $_SESSION['DEVICES_FILTER_MANUFACTURER_ID'] ?? -1,
            'model_id'          => $_SESSION['DEVICES_FILTER_MODEL_ID'] ?? -1,
            'room_id'           => $_SESSION['DEVICES_FILTER_ROOM_ID'] ?? -1
        ];
    else {
        $params = [];
        foreach (['TYPE', 'MANUFACTURER', 'MODEL', 'ROOM'] as $_id) {
            $key = "DEVICES_FILTER_{$_id}_ID";

            if (isset($_SESSION[$key]) && $_SESSION[$key] > 0)
                $params[strtolower($_id) .'_id'] = $_SESSION[$key];
        }
        return $params;
    }
}

/**
 * Sets device page filter
 *
 * @param   array  $post
 */
function rbs_set_device_filter(array $post)
{
    $type = strtoupper($post['type']);
    if ($type == 'RESET') {
        $_SESSION['DEVICES_FILTER_TYPE_ID'] = -1;
        $_SESSION['DEVICES_FILTER_MANUFACTURER_ID'] = -1;
        $_SESSION['DEVICES_FILTER_MODEL_ID'] = -1;
        $_SESSION['DEVICES_FILTER_ROOM_ID'] = -1;
    }
    else $_SESSION["DEVICES_FILTER_{$type}_ID"] = (int)$post['value'];
}

/**
 * Validates password
 * 
 * @param   string  $password
 * @param   bool    [$requireSpecialSymbol]
 *
 * @return  bool
 */
function rbs_validate_password(string $password, bool $requireSpecialSymbol = false): bool
{
    if (!$requireSpecialSymbol) return preg_match("/.*^(?=.{8,20})(?=.*[a-zšžõäöü])(?=.*[A-ZŠŽÕÄÖÜ])(?=.*[0-9]).*$/", $password);
    else return preg_match("/.*^(?=.{8,20})(?=.*[a-zšžõäöü])(?=.*[A-ZŠŽÕÄÖÜ])(?=.*[0-9])(?=.*\W).*$/", $password);
}


/**
 * Gets specific setting data from settings array
 * 
 * @param   string  $key
 * @param   mixed   [$default]
 *
 * @return  mixed
 */
function rbs_get_setting(string $key, $default = [])
{
    global $rbs_settings;

    if (strpos($key, '.') > 0) {
        $_keys = explode('.', $key);
        return $rbs_settings[ $_keys[0] ][ $_keys[1] ] ?? $default;
    }
    return $rbs_settings[$key] ?? $default;
}


/**
 * Returns datastring from array
 *
 * @param   array   $data
 *
 * @return  string
 */
function rbs_get_datastring(array $data): string
{
    ob_start();

    foreach ($data as $_data) {
        $str = "\n" . str_repeat('    ', $_data['no_tabs']) . $_data['string'];
        foreach ($_data['replaces'] as $key => $replace) $str = str_replace($key, $replace, $str);
        echo $str;
    }
    return ob_get_clean();
}

/**
 * Prints out data from array
 *
 * @param   array  $data
 *
 */
function rbs_print_data(array $data)
{
    ob_start();
    echo rbs_get_datastring($data);
    ob_end_flush();
}


/**
 * Return PHP error string (if debug enabled)
 *
 * @param   string  $e
 * @param   string  [$key]
 *
 * @return  array
 */
function rbs_return_php_error(string $e, string $key = 'id'): array
{
    // header("{$_SERVER['SERVER_PROTOCOL']} 500 Internal Server Error");

    return rbs_render_response(
        [['success' => false, 'error' => 0], $key, "PHP_ERROR: {$e}"],
        ['Tekkis serveri viga (kood 500).']
    );
}

/**
 * Writes single setting to database
 *
 * @param   string  $name
 * @param   mixed   $desc
 * @param   bool    [$json_encode]
 *
 * @return  mixed
 */
function rbs_write_setting(...$params)
{
    return GeneralData::writeSetting(...$params);
}

/**
 * Check user login and role if needed
 *
 * @param   int   $role_id
 * @param   bool  $output_message
 * @param   bool  $add_header
 *
 * @return  bool
 */
function rbs_check_user_role(int $role_id = 0, bool $output_message = false, bool $add_header = false): bool
{
    $user = rbs_get_setting('user');

    if (!count($user)) return false;
    elseif (!$user['logged_in'] || ($role_id && $user['role'] != RBS_USER_ROLES[$role_id]))
    {
        if ($add_header || $output_message) header("{$_SERVER['SERVER_PROTOCOL']} 403 Forbidden");

        if ($output_message) echo json_encode([
            'result' => false,
            'error' => 'Sul ei ole selleks tegevuseks õigusi.'
        ]);

        return false;
    }
    return true;
}

/**
 * Check if user is logged in
 *
 * @param   bool  [$output_message]
 * @param   bool  [$add_header]
 *
 * @return  bool
 */
function rbs_is_logged_in(bool $output_message = false, bool $add_header = false): bool
{
    return rbs_check_user_role(0, $output_message, $add_header);
}

/**
 * Check if user is logged in and data is retrieved (just in case if something goes wrong)
 * 
 * @return  bool
 */
function rbs_is_user_ok(): bool
{
    return (rbs_is_logged_in() && rbs_get_setting('user.role_id'));
}

/**
 * Check if logged in user is admin
 *
 * @param   bool  [$output_message]
 * @param   bool  [$add_header]
 *
 * @return  bool
 */
function rbs_is_admin(bool $output_message = false, bool $add_header = false): bool
{
    return rbs_check_user_role(1, $output_message, $add_header);
}

function rbs_is_demo_disabled_user(int $user_id = 0): bool
{
    if (!$user_id) {
        if (!rbs_is_user_ok()) return true;  // should not happen
        else $user_id = rbs_get_setting('user.id');
    }
    return rbs_get_setting('demo_mode', false) && in_array($user_id, RBS_DEMO_DISABLE_UID);
}

/**
 * Finds when first lesson begins and last one ends
 *
 * @param   array  $lesson_times
 *
 * @return  array
 */
function rbs_find_lessons_start_end(array $lesson_times): array
{
    $result = ['start' => '23:59', 'end' => '00:00'];
    
    foreach ($lesson_times as $time) {
        if ($result['start'] > $time['begin']) $result ['start'] = $time['begin'];
        if ($result['end'] < $time['end']) $result ['end'] = $time['end'];
    }
    if ($result['start'] > $result['end']) $result = ['start' => '', 'end' => ''];  /// reset to empty string

    return $result;
}

/**
 * Converts local datestring to ISO date and vice versa
 *
 * @param   string  $date
 * @param   bool    [$local2iso]
 *
 * @return  string
 */
function rbs_convert_date(string $date, bool $local2iso = true): string
{
    $sep = [['-', '.'], ['.', '-']];
    $_i = (int)$local2iso;

    if (strpos($date, $sep[$_i][0])) {
        $_date = explode('.', $date);
        $date = "{$_date[2]}{$sep[$_i][1]}{$_date[1]}{$sep[$_i][1]}{$_date[0]}";
    }
    return $date;
}




/**
 * Reads single setting from database
 *
 * @param   string  $name
 * @param   mixed   $default
 * @param   bool    [$json_decode]
 *
 * @return  mixed
 */
function rbs_read_setting(...$params)
{
    return GeneralData::readSetting(...$params);
}

/**
 * Loads settings from session and database
 *
 * @return  array
 */
function rbs_get_settings(): array
{
    return GeneralData::settings();
}

/**
 * Returns list of device objects with number of devices in type;
 * filtered by either `type_id`, `manufacturer_id` or `model_id`, or by all
 * 
 * @param   array  [$params]  Array of [<type_id>, <manufacturer_id>, <model_id>, <room_id>]
 *
 * @return  array
 */
function rbs_get_device_objects(array $params): array
{
    return GeneralData::deviceObjects($params);
}

/**
 * Returns device image data
 * 
 * @return  array
 */
function rbs_m_get_image(): array
{
    return GeneralData::image();
}

/**
 * Returns user data for modal window
 * 
 * @param   bool  [$use_empty_data]
 * 
 * @return  array
 */
function rbs_m_get_user(bool $use_empty_data = false): array
{
    return GeneralData::user($use_empty_data);
}


/**
 * Returns SVG icon string
 *
 * @param   array  [$param]   Array of [<name>, <size>, <fill>, <class>, <style>]
 *
 * @return  string
 */
function rbs_get_icon(array $param = []): string
{
    return GeneralOutput::getIcon($param);
}

/**
 * Renders response from RenderView class functions (DB queries)
 *
 * @param   array  $input      Array of 'response', 'result key' and 'post data'
 * @param   array  $error_msg
 *
 * @return  array
 */
function rbs_render_response(...$params): array
{
    return GeneralOutput::renderResponse(...$params);
}

/**
 * Converts local datestring to ISO date and vice versa
 *
 * @param   array  $file
 */
function rbs_output_image(array $file)
{
    GeneralOutput::outputImage($file);
}

/**
 * Prints out page device types filter
 *
 * @param   string  $id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_type]
 */
function rbs_print_p_device_types(...$params)
{
    GeneralOutput::printDeviceTypes(...$params);
}

/**
 * Prints out page device manufacturers filter
 *
 * @param   string  $id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_manufacturer]
 */
function rbs_print_p_device_manufacturers(...$params)
{
    GeneralOutput::printDeviceManufacturers(...$params);
}

/**
 * Prints out page device models filter
 *
 * @param   string  $id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_model]
 */
function rbs_print_p_device_models(...$params)
{
    GeneralOutput::printDeviceModels(...$params);
}

/**
 * Prints out page device locations filter
 *
 * @param   string  $id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_location]
 */
function rbs_print_p_device_locations(...$params)
{
    GeneralOutput::printDeviceLocations(...$params);
}

/**
 * Prints out page devices table
 *
 * @param   int  [$no_tabs]
 * 
 * @return  array
 */
function rbs_print_p_devices_table(int $no_tabs = 0): array
{
    return GeneralOutput::printDevicesTable($no_tabs);
}


/* ============================================================================
 *  Ajax functions
 * ============================================================================
 */

/**
 * Returns header branding style
 *
 * @param   array   [$post]
 *
 * @return  string
 */
function rbs__get_header_brand_style(array $post = []): string
{
    return GeneralAjax::getHeaderBrandStyle($post);
}

/**
 * Check user login data and return result
 *
 * @param   string  $rbs_user
 * @param   string  $rbs_password
 *
 * @return  array
 */
function rbs__user_login(...$params): array
{
    return GeneralAjax::userLogin(...$params);
}

/**
 * Check user login data and return result
 *
 * @param   int     $user_id
 * @param   string  $rbs_password
 * @param   string  $rbs_password_repeat
 *
 * @return  array
 */
function rbs__user_change_password(...$params): array
{
    return GeneralAjax::userChangePassword(...$params);
}


/**
 * Gets rooms from database
 * 
 * @return  array
 */
function rbs__get_rooms(): array
{
    return rbs__get_resources(1, false);
}
