<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_CALENDAR')) {
    header('Location: ../../');
    exit;
}


class CalendarAjax
{

    /**
     * Save calendar filter selection to the SESSION
     *
     * @param   array   $post
     * @param   string  [$goto_date]
     */
    public static function calendarFilter(array $post, string $goto_date = '')
    {
        $obj_types = $_SESSION['CALENDAR_FILTER_OBJECT_TYPES_ARRAY'] ?? ['1' => 'ruum'];
        $objects = $_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY'];

        $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_ID'] = $post['rbs_resource_type'];
        $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_NAME'] = $obj_types[$post['rbs_resource_type']];
        $_SESSION['CALENDAR_FILTER_OBJECT_ID'] = $post['rbs_resource'];
        $_SESSION['CALENDAR_FILTER_OBJECT_NAME'] = $objects[$post['rbs_resource']];
        if ($goto_date) $_SESSION['RBS_CALENDAR_GOTO_DATE'] = $goto_date;

        unset($_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY']);
        unset($_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY']);
    }

    /**
     * Returns list of objects by type
     *
     * @param   int   $type_id
     * 
     * @return  string
     */
    public static function filterGetObjects(int $type_id): string
    {
        $objects = [];
        $obj_arr = [];

        foreach (rbs__get_resources($type_id) as $resource) {
            $_obj_id = $resource->id;
            $_sel = ($_obj_id == rbs_get_sel_obj_id() ? ' selected' : '');
            $obj_arr[$_obj_id] = $resource->object_name;
            $objects[] = "<option value=\"{$_obj_id}\"{$_sel}>{$resource->object_name}</option>";
        }
        $_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY'] = $obj_arr;  // update objects array data
        return implode("\n", $objects);
    }

    /**
     * Return selected week bookings (requested by Fullcalendar)
     *
     * @param   string  $start
     * @param   string  $end
     *
     * @return  array
     */
    public static function bookingsSource(string $start, string $end): array
    {
        global $render_view;

        try {
            $start = date('Y-m-d', strtotime($start) + 3600);
            $end = date('Y-m-d', strtotime($end) - 3600);

            $bookings = $render_view->getBookings(['week_start' => $start, 'week_end' => $end, 'object_id' => rbs_get_sel_obj_id()]);
            $return = [];

            if ($bookings['success']) {
                $users = $render_view->getAllUsers();
                $user_names = [];

                if ($users['success'])
                    foreach ($users['result'] as $user)
                        $user_names[$user->id] = $user->first_name .' '. $user->last_name;

                foreach ($bookings['result'] as $booking) {
                    $canChange = rbs_can_change_event($booking->rbs__user__id, $booking->booking_to, $end);
                    $_title = explode('%%%', $booking->title);

                    if ($booking->booking_repeat == 'true') {
                        $_s = explode(' ', $booking->booking_from);
                        $_e = explode(' ', $booking->booking_to);

                        $_b = [
                            'title' => $_title[0],
                            'startRecur'    => $_s[0],
                            'startTime'     => $_s[1],
                            'endRecur'      => $_e[0],
                            'endTime'       => $_e[1],
                            'daysOfWeek'    => $booking->week_days
                        ];
                    }
                    else $_b = [
                            'title' => $_title[0],
                            'start' => str_replace(' ', 'T', $booking->booking_from),
                            'end'   => str_replace(' ', 'T', $booking->booking_to),

                            // 'color' =>  'green',
                            // 'borderColor'  => 'black',
                        ];

                    $_b['extendedProps'] = [
                        'bookingID' => $booking->id,
                        'canChange' => $canChange,
                        'repeating' => $booking->booking_repeat == 'true',
                        'userName'  => ($_title[1] ?? $user_names[$booking->rbs__user__id])
                    ];

                    if ($booking->devices_count) $_b['extendedProps']['devicesCount'] = $booking->devices_count;

                    $return[] = $_b;
                }
            }

            return $return;
        }
        catch (Exception $e) {
            return [];  /// Fullcalendar does not handle array keys 'result' and 'error'
        }
    }


    /**
     * Returns calendar booked times
     *
     * @param   string  $date
     * @param   int     $object_id
     *
     * @return  array
     */
    public static function getBookedTimes(string $date, int $object_id): array
    {
        $lessons = array_keys(rbs_get_setting('lesson_times'));
        $booked_lessons = rbs_get_booked_times($date, $object_id)['booked_lessons'];
        return compact('lessons', 'booked_lessons');
    }

    /**
     * Saves new calendar booking
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveBooking(array $post): array
    {
        global $render_view;

        try {
            $start_time = '23:59';
            $end_time = '00:00';
            $se_times = [];
            $is_recur = isset($post['rbs_booking_recur']);
            $booking_date = rbs_convert_date($post['rbs_booking_date']);

            $end_date = ($is_recur ?
                date('Y-m-d', strtotime(rbs_convert_date($post['rbs_booking_recur_date'])) + 86400) :
                $booking_date);

            $title_extra = '';
            $days_of_week = [];

            if (isset($post['rbs_booking_teacher']) && $post['rbs_booking_teacher'] != '0')
                $title_extra = "%%%{$post['rbs_booking_teacher']}";

            if (isset($post['rbs_booking_all_day'])) {
                $_lse = rbs_get_setting('lessons_start_end');
                $start_time = $_lse['start'];
                $end_time = $_lse['end'];
            }
            else {
                $_se_time = false;

                for ($i = 0; $i < 10; $i++) {
                    if (isset($post["rbs_select_booking_lesson_$i"])) {
                        $_se_time = true;
                        $_t = explode(';', $post["rbs_select_booking_lesson_$i"]);
                        if ($_t[0] < $start_time) $start_time = $_t[0];
                        if ($_t[1] > $end_time) $end_time = $_t[1];
                    }
                    elseif ($_se_time) {
                        $_se_time = false;
                        $se_times[] = [$start_time, $end_time];
                        $start_time = '23:59';  // reset start_time
                    }
                }
                if ($_se_time) $se_times[] = [$start_time, $end_time];  // add also last ones
            }

            if ($end_time == '00:00') return rbs_render_response(
                    [['success' => false, 'error' => 0], 'id', $post],
                    ['Ühtegi tundi pole valitud, kuhu broneering teha.']
                );

            if ($is_recur) {
                for ($i = 0; $i < 7; $i++) {
                    if (isset($post["rbs_repeat_weekdays_$i"])) $days_of_week[] = $i;
                }

                if (!count($days_of_week)) return rbs_render_response(
                        [['success' => false, 'error' => 0], 'id', $post],
                        ['Ühtegi nädalapäeva pole valitud, millal broneering korrata.']
                    );
            }

            $devices_count = 0;
            $devices_all = 0;

            if (isset($post['rbs_devices_count'])) {
                $devices_count = $post['rbs_devices_count'];
                $devices_all = '0';
            }
            elseif (isset($post['rbs_all_devices'])) {
                $devices_count = $post['rbs_all_devices'];
                $devices_all = '1';
            }

            for ($i = 0; $i < count($se_times); $i++) {
                $start_time = $se_times[$i][0];
                $end_time = $se_times[$i][1];

                $new_booking = [
                    'user_id'   => $_SESSION['USER_DATA']->id,
                    'title'     => "{$post['rbs_booking_title']}{$title_extra}",
                    'start'     => "{$booking_date} {$start_time}",
                    'end'       => "{$end_date} {$end_time}",
                    'repeat'    => (isset($post['rbs_booking_recur']) ? 'true' : 'false'),
                    'object_id' => $post['rbs_resource'],

                    'days_of_week'      => json_encode($days_of_week),
                    // 'manufacturer_id'   => 3,  // või NULL
                    // 'model_id'          => 4,
                    'count'             => 1,                //TODO: needs update (during device booking)
                ];

                if ($devices_count) {
                    $new_booking['devices_count'] = $devices_count;
                    $new_booking['devices_all'] = $devices_all;
                }

                if (isset($post['rbs_booking_id'])) {
                    $error_msg = 'Broneeringu muutmine ebaõnnestus.';
                    $new_booking['booking_id'] = $post['rbs_booking_id'];
                    $result = $render_view->updateBooking($new_booking);
                }
                else {
                    $error_msg = 'Ruumi/Seadme broneerimine ebaõnnestus.';
                    $result = $render_view->insertBooking($new_booking);
                }
            }

            if ($result['success']) {
                $post['rbs_resource_type'] = rbs_get_sel_obj_type_id();
                rbs__calendar_filter($post, $booking_date);
            }
            return rbs_render_response([$result, 'id', $post], [$error_msg]);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes calendar booking
     *
     * @param   int   $booking_id
     *
     * @return  array
     */
    public static function deleteBooking(int $booking_id): array
    {
        global $render_view;

        try {
            // $result = ['success' => false, 'error' => 1];
            $result = $render_view->deleteBooking($booking_id);
            return rbs_render_response([$result, 'id', "\$booking_id = {$booking_id}"], ['Broneeringu kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Gets resources from database by resource type ID
     *
     * @param   int     $type_id
     * @param   bool    [$hide_from_booking]
     *
     * @return  array
     */
    public static function getResources(int $type_id, bool $hide_from_booking = true): array
    {
        global $render_view;

        $param = ['type_id' => $type_id, 'not_for_booking' => $hide_from_booking];
        $result = $render_view->objectsByType($param);

        if ($result['success']) return $result['result'];
        else return [];
    }

    /**
     * Gets devices by device object ID
     *
     * @param   int     $object_id
     *
     * @return  array
     */
    public static function getDevices(int $object_id): array
    {
        global $render_view;

        $result = $render_view->getDevicesByObjectId($object_id);

        if ($result['success']) return $result['result'];
        else return [];
    }

}