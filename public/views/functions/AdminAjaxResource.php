<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_ADMIN')) {
    header('Location: ../../');
    exit;
}


class AdminAjaxResource
{

    /* ============================================================================
     *  Rooms
     * ============================================================================
     */

    /**
     * Saves new room to the database
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveNewRoom(array $post): array
    {
        global $render_view;

        try {
            $params = [
                'name'   => filter_var($post['rbs_room_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                'info'   => filter_var(($post['rbs_room_description'] ?? ''), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                                //TODO: some symbols like "+" will be removed; find solution for this
                'color'  => null
            ];

            if (!$post['rbs_room_description']) $params['not_for_booking'] = '1';
            $result = $render_view->insertRoom($params);

            if (isset($post['rbs_return_options']) && $result['success']) {
                return [
                    'result'   => true,
                    'options'  => rbs_print_m_device_types(
                        '', '', RBS_SELECT_OTPION_TITLE['device_location'], 0, $result['result'], true
                    )
                ];
            }
            else return rbs_render_response([$result, 'id', $post], ['Uue ruumi salvestamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Saves new room to the database
     *
     * @param   array   $post
     *
     * @return  array
     */
    public static function saveRoomChange(array $post): array
    {
        global $render_view;

        try {
            $result = $render_view->changeRoom([
                'room_id'         => $post['rbs_room_id'],
                'name'            => filter_var($post['rbs_room_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                'info'            => filter_var($post['rbs_room_description'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                'not_for_booking' => ($post['rbs_room_not_for_booking'] == 'true' ? '1' : '0'),
                'color'           => null
            ]);

            $result['result'] = $post['rbs_room_id'];  // for rendering response
            return rbs_render_response([$result, 'id', $post], ['Ruumi andmete muutmine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes room from database
     *
     * @param   int    $id
     * 
     * @return  array
     */
    public static function deleteRoom(int $id)
    {
        global $render_view;

        try {
            $result = $render_view->deleteObject($id);
            return rbs_render_response([$result, '_', []], ['Ruumi kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }


    /* ============================================================================
     *  Devices
     * ============================================================================
     */

    /**
     * Saves or changes device type; returns result or options for select
     *
     * @param   array  $post
     *
     * @return  array
     */
    public static function saveDeviceType(array $post): array
    {
        global $render_view;

        try {
            if (isset($post['rbs_device_type_id'])) {
                $result = $render_view->updateDeviceType([
                    'id'   => $post['rbs_device_type_id'],
                    'name' => filter_var($post['rbs_device_type_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                ]);
                $result['result'] = $post['rbs_device_type_id'];
                return rbs_render_response([$result, 'id', $post], ['Seadme tüübi muutmine ebaõnnestus.']);
            }
            else {
                $result = $render_view->insertDeviceType(
                    filter_var($post['rbs_device_type_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                );

                if (isset($post['rbs_return_options']) && $result['success']) {
                    return [
                        'result'   => true,
                        'options'  => rbs_print_m_device_types(
                            '', '', RBS_SELECT_OTPION_TITLE['device_type'], 0, $result['result'], true
                        )
                    ];
                }
                else return rbs_render_response(
                    [$result, 'id', $post],
                    ['Uue seadme tüübi lisamine ebaõnnestus.']
                );
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes device type from database
     *
     * @param   int    $type_id
     * 
     * @return  array
     */
    public static function deleteDeviceType(int $type_id): array
    {
        global $render_view;

        try {
            $result = $render_view->deleteDeviceType($type_id);
            return rbs_render_response([$result, '_', []], ['Seadme tüübi kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Saves or changes device manufacturer
     *
     * @param   array  $post
     *
     * @return  array
     */
    public static function saveManufacturer(array $post): array
    {
        global $render_view;

        try {
            if (isset($post['rbs_manufacturer_id'])) {
                $result = $render_view->updateDeviceManufacturer([
                    'id'   => $post['rbs_manufacturer_id'],
                    'name' => filter_var($post['rbs_manufacturer_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                ]);
                $result['result'] = $post['rbs_manufacturer_id'];
                return rbs_render_response([$result, 'id', $post], ['Tootja nime muutmine ebaõnnestus.']);
            }
            else {
                $result = $render_view->insertDeviceManufacturer(
                    filter_var($post['rbs_manufacturer_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                );

                if (isset($post['rbs_return_options']) && $result['success']) {
                    return [
                        'result'   => true,
                        'options'  => rbs_print_m_device_manufacturers(
                            '', '', RBS_SELECT_OTPION_TITLE['manufacturer'], 0, $result['result'], true
                        )
                    ];
                }
                else return rbs_render_response([$result, 'id', $post], ['Uue tootja lisamine ebaõnnestus.']);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes manufacturer from database
     *
     * @param   int    $id
     * 
     * @return  array
     */
    public static function deleteManufacturer(int $id): array
    {
        global $render_view;

        try {
            $result = $render_view->deleteDeviceManufacturer($id);
            return rbs_render_response([$result, '_', []], ['Seadme tootja kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Saves or changes device model
     *
     * @param   array  $post
     *
     * @return  array
     */
    public static function saveModel(array $post): array
    {
        global $render_view;

        try {
            if (isset($post['rbs_model_id'])) {
                $result = $render_view->updateDeviceModel([
                    'id'   => $post['rbs_model_id'],
                    'name' => filter_var($post['rbs_model_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                ]);
                $result['result'] = $post['rbs_model_id'];
                return rbs_render_response([$result, 'id', $post], ['Mudeli muutmine ebaõnnestus.']);
            }
            else {
                $result = $render_view->insertDeviceModel(
                    filter_var($post['rbs_model_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                );

                if (isset($post['rbs_return_options']) && $result['success']) {
                    return [
                        'result'   => true,
                        'options'  => rbs_print_m_device_models(
                            '', '', RBS_SELECT_OTPION_TITLE['device_model'], 0, $result['result'], true
                        )
                    ];
                }
                else return rbs_render_response([$result, 'id', $post], ['Uue mudeli lisamine ebaõnnestus.']);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes model from database
     *
     * @param   int    $id
     * 
     * @return  array
     */
    public static function deleteModel(int $id): array
    {
        global $render_view;

        try {
            $result = $render_view->deleteDeviceModel($id);
            return rbs_render_response([$result, '_', []], ['Seadme mudeli kustutamine ebaõnnestus.']);
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Saves or changes device or set of devices
     * 
     * @return  array
     */
    public static function saveImage(): array
    {
        $file = date('YmdHis') . bin2hex(random_bytes(2));
        $filename = $_FILES['rbs_image']['name'];
        $tmp_name = $_FILES['rbs_image']['tmp_name'];

        $type = rbs_move_uploaded_image($file, $tmp_name);

        if (!$type) return rbs_return_php_error("Can't move uploaded image file.");
        else return ['result' => true, 'img' => compact('file', 'filename', 'type'), 'debug' => ''];
    }

    /**
     * Saves or changes device or set of devices
     *
     * @param   array  $post
     *
     * @return  array
     */
    public static function saveDeviceObject(array $post): array
    {
        global $render_view;

        try {
            if (isset($post['rbs_device_object_id'])) {
                $params = [
                    'device_id' => $post['rbs_device_object_id'],
                    'name'      => filter_var($post['rbs_device_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'info'      => filter_var(($post['rbs_info'] ?? ''), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'count'     => $post['rbs_devices_count'],
                    'color'     => null,
                    'room_id'   => ($post['rbs_device_location'] == '0' ? null : $post['rbs_device_location']),
                ];

                if (isset($post['rbs_image_file'])) {
                    $img_data = $render_view->getDeviceFile($post['rbs_device_object_id']);

                    if ($img_data['success']) {
                        $_file = RBS_IMG_FLDR . $img_data['result']->image_file;

                        if (file_exists($_file)) {
                            unlink($_file);
                            unlink("{$_file}_thumb");
                        }
                    }
                    $params['image_file'] = $post['rbs_image_file'];
                    $params['image_filename'] = $post['rbs_image_filename'];
                    $params['image_type'] = $post['rbs_image_type'];
                }

                $result = $render_view->changeDeviceObject($params);
                return rbs_render_response([$result, 'id', $post], ['Uue seadme salvestamine ebaõnnestus.']);
            }
            else {
                $params = [
                    'name'            => filter_var($post['rbs_device_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'info'            => filter_var(($post['rbs_info'] ?? ''), FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'count'           => $post['rbs_devices_count'],
                    'color'           => null,
                    'type_id'         => $post['rbs_device_type'],
                    'manufacturer_id' => $post['rbs_device_manufacturer'],
                    'model_id'        => $post['rbs_device_model'],
                    'room_id'         => ($post['rbs_device_location'] == '0' ? null : $post['rbs_device_location']),
                    'image_file'      => ($post['rbs_image_file'] ?? null),
                    'image_filename'  => ($post['rbs_image_filename'] ?? null),
                    'image_type'      => ($post['rbs_image_type'] ?? null)
                ];

                $result = $render_view->insertDeviceObject($params);
                return rbs_render_response([$result, 'id', $post], ['Uue seadme salvestamine ebaõnnestus.']);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Outputs image preview
     *
     * @param   int  $id
     */
    public static function outputImagePreview(int $id)
    {
        global $render_view;
        $result = $render_view->getDeviceFile($id); 

        if ($result['success']) {
            $img_file = RBS_IMG_FLDR . $result['result']->image_file;

            if (file_exists($img_file)) {
                list($img_width, $img_height) = getimagesize($img_file);
                $image = imagecreatefromstring( file_get_contents($img_file) );
                $img_size = min($img_width, $img_height);
                $out_image = imagecreatetruecolor(RBS_PREVIEW_IMG_SIZE, RBS_PREVIEW_IMG_SIZE);

                $_result = imagecopyresampled(
                    $out_image, $image, 0, 0,
                    ($img_width - $img_size) / 2.0, ($img_height - $img_size) / 2.0,
                    RBS_PREVIEW_IMG_SIZE, RBS_PREVIEW_IMG_SIZE, $img_size, $img_size
                );

                if ($_result) {
                    header('Content-type: '. mime_content_type($img_file));
                    header('Content-Transfer-Encoding: binary');
    
                    ob_end_flush();
                    AdminGeneral::outputImagePreview($out_image, $result['result']->image_type);
                }
                imagedestroy($image);
                imagedestroy($out_image);
            }
        }
    }

    /**
     * Return device data by id
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public static function getDeviceData(int $device_id): array
    {
        $data = rbs_m_get_device_by_id($device_id);

        if (!$data['name']) $result = rbs_render_response(
                [
                    ['success' => false, 'error' => 0], '',
                    ['task' => 'get_device_data', 'device_id' => $device_id]
                ],
                ['Seadet ei leitud.']
            );
        else $result = rbs_render_response(
                [
                    ['success' => true, 'result' => $data], 'data',
                    ['task' => 'get_device_data', 'device_id' => $device_id]
                ], []
            );

        return $result;
    }

    /**
     * Saves device data
     *
     * @param   array  $params
     *
     * @return  array
     */
    public static function saveDevice(array $post): array
    {
        global $render_view;

        try {
            if ($post['rbs_device_id'] == '-1') {
                $params = [
                    'device_object_id' => $post['rbs_device_object_id'],
                    'name'             => filter_var($post['rbs_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'asset_code'       => filter_var($post['rbs_asset_code'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'serial_number'    => filter_var($post['rbs_serial_number'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'info'             => filter_var($post['rbs_info'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                ];
                $result = $render_view->insertDevice($params);
                $result = rbs_m_get_device_if_needed($result, $post);

                return rbs_render_response([$result, 'id', $post], ['Uue seadme lisamine ebaõnnestus.']);
            }
            else {
                $params = [
                    'device_id'     => $post['rbs_device_id'],
                    'name'          => filter_var($post['rbs_name'], FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'asset_code'    => filter_var($post['rbs_asset_code'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'serial_number' => filter_var($post['rbs_serial_number'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'info'          => filter_var($post['rbs_info'] ?? '', FILTER_SANITIZE_FULL_SPECIAL_CHARS),
                    'is_broken'     => (isset($post['rbs_is_broken']) ? '1' : '0')
                ];
                $result = $render_view->updateDevice($params);
                $result['result'] = $post['rbs_device_id'];
                $result = rbs_m_get_device_if_needed($result, $post, 'next_prev_device_id');

                return rbs_render_response([$result, 'id', $post], ['Seadme info muutmine ebaõnnestus.']);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Deletes device from database
     *
     * @param   array  $post
     * 
     * @return  array
     */
    public static function deleteDevice(array $post): array
    {
        global $render_view;

        try {
            $result = $render_view->deleteDevice($post['rbs_device_id']);
            $result['result'] = $post['rbs_device_id'];
            $result = rbs_m_get_device_if_needed($result, $post, 'rbs_next_device_id');

            return rbs_render_response([$result, 'id', $post], ['Seadme kustutamine ebaõnnestus.']);

            return rbs_render_response(
                [$result, '_', ['task' => 'delete_device', 'post' => $post]],
                ['Seadme kustutamine ebaõnnestus.']
            );
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

}