<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_GENERAL')) {
    header('Location: ../../');
    exit;
}


class GeneralData
{

    /**
     * Reads single setting from database
     *
     * @param   string  $name
     * @param   mixed   $default
     * @param   bool    [$json_decode]
     *
     * @return  mixed
     */
    public static function readSetting(string $name, $default, bool $json_decode = false)
    {
        global $_CONFIG, $render_view;

        if (isset($_CONFIG[ $name ])) {
            if (!$json_decode) return $_CONFIG[ $name ];
            return json_decode($_CONFIG[ $name ], true);
        }
        $result = $render_view->getSettings($name);

        if ($result['success']) {
            if (!$json_decode) return $result['result']->setting_value;
            return json_decode($result['result']->setting_value, true);
        }
        else return $default;
    }

    /**
     * Writes single setting to database
     *
     * @param   string  $name
     * @param   mixed   $desc
     * @param   bool    [$json_encode]
     *
     * @return  mixed
     */
    public static function writeSetting(string $name, $desc, bool $json_encode = false)
    {
        global $render_view;

        if ($json_encode) $desc = json_encode($desc);

        return $render_view->addSettings( compact('name', 'desc') )['result'] ?? false;
    }


    /**
     * Loads settings from session and database
     *
     * @return  array
     */
    public static function settings(): array
    {
        global $_CONFIG, $render_view;

        $_bg = ['light' => 'light', 'dark' => 'dark text-light'];

        $result = $render_view->getSettings();
        if ($result['success']) {
            $_settings = [];

            foreach ($result['result'] as $setting)
                $_settings[$setting->setting_name] = $setting->setting_value;
            
            extract($_settings);
        }

        $_CONFIG['view_post_key'] = $view_post_key ?? RBS_DEFAULT_POST_KEY;
        $_CONFIG['view_navbar'] = ($view_navbar ?? 'light');

        // Just in case for function rbs_read_setting() to speed up things
        if (isset($calendar_options)) $_CONFIG['calendar_options'] = $calendar_options;
        if (isset($lesson_times)) $_CONFIG['lesson_times'] = $lesson_times;

        $lesson_times = json_decode(($lesson_times ?? '[]'), true);

        $settings = [
            'header'            => (isset($header) ? json_decode($header, true) : RBS_DEFAULT_HEADER),
            'table_show_ids'    => ($table_show_ids ?? '0' ) == '1',
            'calendar_options'  => json_decode(($calendar_options ?? '[]'), true),
            'booking_options'   => json_decode(($booking_options ?? '[]'), true),
            'lesson_times'      => $lesson_times,
            'lessons_start_end' => rbs_find_lessons_start_end($lesson_times),

            /// Couple default settings
            'user'      => ['logged_in' => false, 'role' => RBS_USER_ROLES[0]],
            'page'      => [
                'name'      => 'home',
                'title'     => 'Avaleht',
                'js_ext'    => 'h',
                'navbar'    => $_CONFIG['view_navbar'],
                'nav_bg'    => $_bg[ ($_CONFIG['view_navbar'] ?? 'light') ],
            ],

            'allow_debug'   => $_CONFIG['allow_debug'] ?? false,
            'demo_mode'     => $_CONFIG['demo_mode'] ?? false
        ];

        if (isset($_SESSION['USER_DATA'])) {
            $_data = $_SESSION['USER_DATA'];
            $_role = ($_data->rbs__user_role__id ?? 0);

            $settings['user'] = [
                'logged_in' => true,
                'role'      => RBS_USER_ROLES[$_role],  //TODO: vaja täiendada!
                'id'        => ($_data->id ?? 0),
                'firstname' => ($_data->first_name ?? 'JOHN'),
                'lastname'  => ($_data->last_name ?? 'DOE'),
                'username'  => ($_data->username ?? 'johndoe'),
                'email'     => ($_data->email ?? 'john@doe.eu'),
                'role_id'   => $_role
            ];
        }

        return $settings;
    }


    /**
     * Returns list of device objects with number of devices in type;
     * filtered by either `type_id`, `manufacturer_id` or `model_id`, or by all
     * 
     * @param   array  [$params]  Array of [<type_id>, <manufacturer_id>, <model_id>, <room_id>]
     *
     * @return  array
     */
    public static function deviceObjects($params): array
    {
        global $render_view;

        $result = $render_view->getDeviceObjects($params);

        if ($result['success']) {
            $devices = [];

            foreach ($result['result'] as $_device) {
                $devices[ $_device->id ] = $_device;
            }
            return $devices;
        }
        else return [];
    }

    /**
     * Returns device image data
     * 
     * @return  array
     */
    public static function image(): array
    {
        global $render_view;

        $_result = ['image_id' => 0, 'device_name' => '', 'image_filename' => ''];

        if (isset($_POST['image_id'])) {
            $result = $render_view->getDeviceFile($_POST['image_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $_result =  [
                    'image_id'       => $_POST['image_id'],
                    'device_name'    => $_data->object_name,
                    'image_filename' => $_data->image_filename
                ];
            }
        }
        return $_result;
    }


    /**
     * Returns user data for modal window
     * 
     * @param   bool  [$use_empty_data]
     * 
     * @return  array
     */
    public static function user(bool $use_empty_data = false): array
    {
        global $render_view;

        $_user = ($use_empty_data
            ? ['id' => 0, 'username' => '', 'email' => '', 'firstname' => '', 'lastname' => '', 'role' => '', 'role_id' => 0]
            : rbs_get_setting('user')
        );

        if (isset($_POST['user_id']) && $_POST['user_id'] > 0) {
            $result = $render_view->getUser($_POST['user_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $_user =  [
                    'role'      => RBS_USER_ROLES[ $_data->role_id ],
                    'id'        => $_data->id,
                    'firstname' => $_data->first_name,
                    'lastname'  => $_data->last_name,
                    'username'  => $_data->username,
                    'email'     => $_data->email,
                    'role_id'   => $_data->role_id
                ];
            }
        }
        return $_user;
    }

}