<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_ADMIN')) {
    header('Location: ../../');
    exit;
}


class AdminGeneral
{

    /**
     * Returns list of device types with number of devices in type
     * 
     * @return  array
     */
    public static function getDeviceTypes(): array
    {
        global $render_view;

        $result = $render_view->getDeviceTypesAndCntObjects();

        if ($result['success']) {
            $obj_types = [];

            foreach ($result['result'] as $obj_type) {
                $obj_types[ $obj_type->id ] = $obj_type;
            }
            return $obj_types;
        }
        else return [];
    }

    /**
     * Return list of device manufacturers with number of devices
     *
     * @return  array
     */
    public static function getDeviceManufacturers(): array
    {
        global $render_view;

        $result = $render_view->getDeviceManufacturersAndCntObjects();

        if ($result['success']) {
            $manufacturers = [];

            foreach ($result['result'] as $manufacturer)
                $manufacturers[ $manufacturer->id ] = $manufacturer;

            return $manufacturers;
        }
        else return [];
    }

    /**
     * Return list of device models with number of devices
     *
     * @return  array
     */
    public static function getDeviceModels(): array
    {
        global $render_view;

        $result = $render_view->getDeviceModelsAndCntObjects();

        if ($result['success']) {
            $models = [];

            foreach ($result['result'] as $model)
                $models[ $model->id ] = $model;

            return $models;
        }
        else return [];
    }

    /**
     * Resizes uploaded image if necessary, creates thumbnail and saves it
     *
     * @param   string  $file
     * @param   string  $uploaded_image
     *
     * @return  int
     */
    public static function moveUploadedImage(string $file, string $uploaded_image): int
    {
        $max_width = 1280;  //TODO: add numbers to the settings
        $max_height = 960;
        $thumb_size = 70;

        $img_array = self::_getUploadedImageArray($uploaded_image);
        if (!$img_array['src']) return 0;

        list($img_width, $img_height) = getimagesize($uploaded_image);
        $img_size = min($img_width, $img_height);
        $out_width = $img_width;
        $out_height = $img_height;

        if ($img_width > $max_width) {
            $out_width = $max_width;
            $out_height = $max_width * $img_height / (float)$img_width;
        }
        if ($img_height > $max_height) {
            $out_width = $max_height * $img_width / (float)$img_height;
            $out_height = $max_height;
        }

        /// Create, resize if neccessary and output image
        $out_img = imagecreatetruecolor($out_width, $out_height);

        $_result = imagecopyresampled(
            $out_img, $img_array['src'], 0, 0, 0, 0,
            $out_width, $out_height, $img_width, $img_height
        );
        if ($_result)
            $_result = self::_outputImage(
                RBS_IMG_FLDR . $file, $img_array['img_type'], $out_img
            );

        imagedestroy($out_img);
        if (!$_result) return 0;

        /// Create, resize and output image thumnail
        $thumb_img = imagecreatetruecolor($thumb_size, $thumb_size);

        $_result = imagecopyresampled(
            $thumb_img, $img_array['src'], 0, 0,
            ($img_width - $img_size) / 2.0, ($img_height - $img_size) / 2.0,
            $thumb_size, $thumb_size, $img_size, $img_size
        );
        if ($_result)
            $_result = self::_outputImage(
                RBS_IMG_FLDR . $file .'_thumb', $img_array['img_type'], $thumb_img
            );

        imagedestroy($thumb_img);
        if (!$_result) return 0;

        return $img_array['img_type'];
    }

    /**
     * Outputs image preview
     *
     * @param   resource|GDImage  $image
     * @param   int               $img_type
     * @param   int               $quality
     *
     * @return  bool
     */
    public static function outputImagePreview($image, int $img_type, int $quality = 85): bool
    {
        return self::_outputImage(null, $img_type, $image, $quality);
    }

    /**
     * Returns device data by ID
     *
     * @param   int    $device_id
     *
     * @return  array
     */
    public static function getDeviceById(int $device_id): array
    {
        global $render_view;

        if ($device_id > 0) {  // parameter value may be -1
            $result = $render_view->getDeviceById($device_id);

            if ($result['success']) {
                return [
                    'name'          => $result['result']->name,
                    'asset_code'    => $result['result']->asset_code,
                    'serial_number' => $result['result']->serial_number,
                    'info'          => $result['result']->info,
                    'is_broken'     => $result['result']->broken == '1'
                ];
            }
        }
        return [
            'name' => '', 'asset_code' => '', 'serial_number' => '',
            'info' => '', 'is_broken' => false
        ];
    }

    /**
     * Gets device data if neede
     *
     * @param   array   $result
     * @param   array   $post
     * @param   string  $key
     *
     * @return  array
     */
    public static function getDeviceIfNeeded(array $result, array $post, string $key): array
    {
        if (isset($post[ $key ]))
            $result['result'] = [
                'new_id'      => $result['result'],
                'device_data' => self::getDeviceById($post[ $key ])
            ];

        return $result;
    }


    // ========================================================================


    /*
     * Private helper function for returning array of uploaded image content and extension
     */
    private static function _getUploadedImageArray(string $uploaded_image): array
    {
        $return = ['src' => '', 'img_type' => 0];
        if (filesize($uploaded_image) < 12) return $return;

        // Allowed imagetypes
        $img_types = [
            IMAGETYPE_BMP,    IMAGETYPE_PNG,    IMAGETYPE_WEBP,
            IMAGETYPE_GIF,    IMAGETYPE_WBMP,   IMAGETYPE_XBM,
            IMAGETYPE_JPEG,
        ];
        $exif_type = exif_imagetype($uploaded_image);  // accept only images by exif type
        if (!$exif_type || !in_array($exif_type, $img_types)) return $return;

        $return['img_type'] = $exif_type;

        // switch ($exif_type) {
        //     case IMAGETYPE_BMP:
        //         $return['src'] = imagecreatefrombmp($uploaded_image);   break;
        //     case IMAGETYPE_GIF:
        //         $return['src'] = imagecreatefromgif($uploaded_image);   break;
        //     case IMAGETYPE_JPEG:
        //         $return['src'] = imagecreatefromjpeg($uploaded_image);  break;
        //     case IMAGETYPE_PNG:
        //         $return['src'] = imagecreatefrompng($uploaded_image);   break;
        //     case IMAGETYPE_WBMP:
        //         $return['src'] = imagecreatefromwbmp($uploaded_image);  break;
        //     case IMAGETYPE_WEBP:
        //         $return['src'] = imagecreatefromwebp($uploaded_image);  break;
        //     case IMAGETYPE_XBM:
        //         $return['src'] = imagecreatefromxbm($uploaded_image);   break;
        //     default:
        //     $return['src'] = false;
        // }
        $return['src'] = imagecreatefromstring( file_get_contents($uploaded_image) );

        return $return;
    }

    /*
     * Private helper function for outputing/saving newly created image to the file
     *
     * @param   string|null       $file
     * @param   int               $img_type
     * @param   resource|GDImage  $image
     * @param   int               [$quality]
     *
     * @return  bool
     */
    private static function _outputImage($file, int $img_type, $image, int $quality = 85): bool
    {
        switch ($img_type) {
            case IMAGETYPE_BMP:
                $result = imagebmp($image, $file);             break;
            case IMAGETYPE_GIF:
                $result = imagegif($image, $file);             break;
            case IMAGETYPE_JPEG:
                $result = imagejpeg($image, $file, $quality);  break;
            case IMAGETYPE_PNG:
                $result = imagepng($image, $file);             break;
            case IMAGETYPE_WBMP:
                $result = imagewbmp($image, $file);            break;
            case IMAGETYPE_WEBP:
                $result = imagewebp($image, $file, $quality);  break;
            case IMAGETYPE_XBM:
                $result = imagexbm($image, $file);             break;
            default:
            $result = false;
        }
        return $result;
    }

}