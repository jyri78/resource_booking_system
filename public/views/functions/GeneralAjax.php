<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_GENERAL')) {
    header('Location: ../../');
    exit;
}


class GeneralAjax
{

    /**
     * Returns header branding style
     *
     * @param   array   [$post]
     *
     * @return  string
     */
    public static function getHeaderBrandStyle(array $post = []): string
    {
        $header_opt = rbs_get_setting('header');
        $result = 'fw-bold py-2 opacity-75';
        $style = (!$post ? $header_opt['style'] : (int)$post['rbs_settings_header_brand_style']);

        if (!$style) return $result;

        $_style = ($style > 2 ? 'pill px-4' : 'circle px-5');
        $color = (!$post ? $header_opt['bg_color'] : $post['rbs_settings_header_brand_color']);
        $result .= " rounded-{$_style} bg-{$color} bg-opacity-10";

        if (!($style % 2)) {
            $_border = ($style > 2 ? 'border-3' : 'border-5');
            $result .= " border-{$color} border-start border-end {$_border}";
        }

        return $result;
    }

    /**
     * Check user login data and return result
     *
     * @param   string  $rbs_user
     * @param   string  $rbs_password
     *
     * @return  array
     */
    public static function userLogin(string $rbs_user, string $rbs_password): array
    {
        global $render_view;

        try {
            $allow_debug = rbs_get_setting('allow_debug');

            if (strpos($rbs_user, '@') !== false && !filter_var($rbs_user, FILTER_VALIDATE_EMAIL))
                return [
                    'result' => false,
                    'error' => '|rbs_user=Palun sisesta korrektne kasutajanimi või e-maili aadress!',
                    //'debug' => ($allow_debug ? $_data : false)
                ];
            else {
                $_data = [
                    'username'   => (strpos($rbs_user, '@') !== false ?
                                        $rbs_user : filter_var($rbs_user, FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                                    ),
                    'userpasswd' => filter_var($rbs_password, FILTER_SANITIZE_FULL_SPECIAL_CHARS)
                ];

                if ($render_view->userLogin($_data)) return [  // one of exceptions of response (not array, just boolean)
                        'result' => true,
                        'debug' => ($allow_debug ? $_data : false)
                    ];
                else return [
                        'result' => false,
                        'error' => '|rbs_user,rbs_password=Vale kasutajanimi/e-mail või parool.',
                        'debug' => ($allow_debug ? $_data : false)
                    ];
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

    /**
     * Check user login data and return result
     *
     * @param   int     $user_id
     * @param   string  $rbs_password
     * @param   string  $rbs_password_repeat
     *
     * @return  array
     */
    public static function userChangePassword(int $user_id, string $password, string $password_repeat): array
    {
        global $render_view;
        
        try {
            $password = filter_var($password, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $password_repeat = filter_var($password_repeat, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

            if ($password != $password_repeat)
                return [
                    'result' => false,
                    'error' => '|rbs_password,rbs_password_repeat=Sisestatud paroolid ei ole ühesugused.'
                ];
            elseif (!rbs_validate_password($password))
                return [
                    'result' => false,
                    'error' => '|rbs_password,rbs_password_repeat=Parool peab olema vähemalt 8 sümbolit pikk ning sisaldama suur- ja väiketähte ja numbrit.'
                ];
            else {
                $result = $render_view->updateUserData(['user_id' => $user_id, 'userpasswd' => $password]);
                return rbs_render_response([$result, 'id', compact('user_id', 'password')], [
                    'Parooli muutmine ebaõnnestus.',
                ]);
            }
        }
        catch (Exception $e) {
            return rbs_return_php_error($e);
        }
    }

}