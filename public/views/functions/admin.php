<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}
define("RBS_APP_CLS_ADMIN", true);  // for Admin classes

foreach(['General', 'PageOutput', 'Modal', 'AjaxResource', 'AjaxSetting'] as $cls)
    require_once "Admin{$cls}.php";


/**
 * Prints out page device types table
 *
 * @param   int  [$no_tabs]
 * 
 * @return  array
 */
function rbs_print_p_device_types_table(int $no_tabs = 0): array
{
    return AdminPageOutput::deviceTypesTable($no_tabs);
}

/**
 * Prints out page device types table
 *
 * @param   int  [$no_tabs]
 * 
 * @return  array
 */
function rbs_print_p_manufacturers_table(int $no_tabs = 0): array
{
    return AdminPageOutput::manufacturersTable($no_tabs);
}

/**
 * Prints out page models table
 *
 * @param   int  [$no_tabs]
 * 
 * @return  array
 */
function rbs_print_p_models_table(int $no_tabs = 0): array
{
    return AdminPageOutput::modelsTable($no_tabs);
}

/**
 * Prints out page settings lesson times table
 *
 * @param   int  [$no_tabs]
 */
function rbs_print_p_settings_lesson_times(int $no_tabs = 0)
{
    AdminPageOutput::settingsLessonTimes($no_tabs);
}

/**
 * Prints out header background styles selection
 *
 * @param   int    [$no_tabs]
 */
function rbs_print_p_settings_styles_selection(int $no_tabs = 0)
{
    AdminPageOutput::settingsStylesSelection($no_tabs);
}

/**
 * Prints out header background colors selection
 *
 * @param   int    [$no_tabs]
 */
function rbs_print_p_settings_colors_selection(int $no_tabs = 0)
{
    AdminPageOutput::settingsColorsSelection($no_tabs);
}

/**
 * Prints out page users data table
 *
 * @param   int    [$no_tabs]
 * @param   array  [$users]
 * 
 * @return  array
 */
function rbs_print_p_users_table(...$params): array
{
    return AdminPageOutput::usersTable(...$params);
}


/**
 * Prints out modal calendar booking users
 *
 * @param   int     [$no_tabs]
 * @param   string  [$sel_user]
 */
function rbs_print_m_calendar_booking_users(...$params)
{
    AdminModal::printCalendarBookingUsers(...$params);
}

/**
 * Prints out modal add-change device types
 *
 * @param   string  $id
 * @param   string  $next_id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_type]
 * @param   bool    [$return_str]
 * 
 * @return  int|string
 */
function rbs_print_m_device_types(...$params)
{
    return AdminModal::printDeviceTypes(...$params);
}

/**
 * Prints out modal add-change device manufacturers
 *
 * @param   string  $id
 * @param   string  $next_id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_manufacturer]
 * @param   bool    [$return_str]
 * 
 * @return  int|string
 */
function rbs_print_m_device_manufacturers(...$params)
{
    return AdminModal::printDeviceManufacturers(...$params);
}

/**
 * Prints out modal add-change device models
 *
 * @param   string  $id
 * @param   string  $next_id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_model]
 * @param   bool    [$return_str]
 * 
 * @return  int|string
 */
function rbs_print_m_device_models(...$params)
{
    return AdminModal::printDeviceModels(...$params);
}

/**
 * Prints out modal add-change device locations
 *
 * @param   string  $id
 * @param   string  $next_id
 * @param   string  $text
 * @param   int     [$no_tabs]
 * @param   int     [$sel_location]
 * @param   bool    [$return_str]
 * @param   bool    [$disable_room]
 * 
 * @return  int|string
 */
function rbs_print_m_device_locations(...$params)
{
    return AdminModal::printDeviceLocations(...$params);
}


/**
 * Returns device object data for modal window
 * 
 * @return  array
 */
function rbs_m_get_device_object(): array
{
    return AdminModal::getDeviceObject();
}

/**
 * Returns device type data for modal window
 * 
 * @return  array
 */
function rbs_m_get_device_type(): array
{
    return AdminModal::getDeviceType();
}

/**
 * Returns device manufacturer data for modal window
 * 
 * @return  array
 */
function rbs_m_get_manufacturer(): array
{
    return AdminModal::getManufacturer();
}

/**
 * Returns device model data for modal window
 * 
 * @return  array
 */
function rbs_m_get_model(): array
{
    return AdminModal::getModel();
}

/**
 * Returns device ids
 * 
 * @return  array
 */
function rbs_m_get_device_ids(): array
{
    return AdminModal::getDeviceIds();
}

/**
 * Returns device data by ID
 * 
 * @return  array
 */
function rbs_m_get_device_by_id(int $device_id): array
{
    return AdminGeneral::getDeviceById($device_id);
}

/**
 * Gets device data if neede
 *
 * @param   array   $result
 * @param   array   $post
 * @param   string  $key
 *
 * @return  array
 */
function rbs_m_get_device_if_needed(...$params): array
{
    return AdminGeneral::getDeviceIfNeeded(...$params);
}


/**
 * Returns list of device types with number of devices in type
 * 
 * @return  array
 */
function rbs_get_device_types(): array
{
    return AdminGeneral::getDeviceTypes();
}

/**
 * Return list of device manufacturers with number of devices
 *
 * @return  array
 */
function rbs_get_device_manufacturers(): array
{
    return AdminGeneral::getDeviceManufacturers();
}

/**
 * Return list of device models with number of devices
 *
 * @return  array
 */
function rbs_get_device_models(): array
{
    return AdminGeneral::getDeviceModels();
}

/**
 * Resizes uploaded image if necessary, creates thumbnail and saves them
 *
 * @param   string  $file
 * @param   string  $uploaded_image
 *
 * @return  int
 */
function rbs_move_uploaded_image(...$params): int
{
    return AdminGeneral::moveUploadedImage(...$params);
}


/* ============================================================================
 *  Rooms
 * ============================================================================
 */

/**
 * Saves new room to the database
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_new_room(array $post): array
{
    return AdminAjaxResource::saveNewRoom($post);
}

/**
 * Saves new room to the database
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_room_change(array $post): array
{
    return AdminAjaxResource::saveRoomChange($post);
}

/**
 * Deletes room from database
 *
 * @param   int    $id
 * 
 * @return  array
 */
function rbs__delete_room(int $id): array
{
    return AdminAjaxResource::deleteRoom($id);
}


/* ============================================================================
 *  Devices
 * ============================================================================
 */

/**
 * Saves or changes device type; returns result or options for select
 *
 * @param   array  $post
 *
 * @return  array
 */
function rbs__save_device_type(array $post): array
{
    return AdminAjaxResource::saveDeviceType($post);
}

/**
 * Deletes device type from database
 *
 * @param   int    $type_id
 * 
 * @return  array
 */
function rbs__delete_device_type(int $type_id): array
{
    return AdminAjaxResource::deleteDeviceType($type_id);
}

/**
 * Saves or changes device manufacturer
 *
 * @param   array  $post
 *
 * @return  array
 */
function rbs__save_manufacturer(array $post): array
{
    return AdminAjaxResource::saveManufacturer($post);
}

/**
 * Deletes manufacturer from database
 *
 * @param   int    $id
 * 
 * @return  array
 */
function rbs__delete_manufacturer(int $id): array
{
    return AdminAjaxResource::deleteManufacturer($id);
}

/**
 * Saves or changes device model
 *
 * @param   array  $post
 *
 * @return  array
 */
function rbs__save_model(array $post): array
{
    return AdminAjaxResource::saveModel($post);
}

/**
 * Deletes model from database
 *
 * @param   int    $id
 * 
 * @return  array
 */
function rbs__delete_model(int $id): array
{
    return AdminAjaxResource::deleteModel($id);
}

/**
 * Saves or changes device or set of devices
 * 
 * @return  array
 */
function rbs__save_image(): array
{
    return AdminAjaxResource::saveImage();
}

/**
 * Outputs image preview
 * 
 * @param   int    $id
 */
function rbs__output_image_preview(int $id)
{
    return AdminAjaxResource::outputImagePreview($id);
}

/**
 * Saves or changes device or set of devices
 *
 * @param   array  $post
 *
 * @return  array
 */
function rbs__save_device_object(array $post): array
{
    return AdminAjaxResource::saveDeviceObject($post);
}

/**
 * Saves or changes device
 *
 * @param   array  $post
 *
 * @return  array
 */
function rbs__save_device(array $post): array
{
    return AdminAjaxResource::saveDevice($post);
}

/**
 * Returns device data by ID
 *
 * @param   int    $device_id
 *
 * @return  array
 */
function rbs__get_device_data(int $device_id): array
{
    return AdminAjaxResource::getDeviceData($device_id);
}

/**
 * Deletes device from database
 *
 * @param   array  $post
 * 
 * @return  array
 */
function rbs__delete_device(array $post): array
{
    return AdminAjaxResource::deleteDevice($post);
}


/* ============================================================================
 *  Site settings
 * ============================================================================
 */

/**
 * Save fullcalendar options
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_settings_calendar_options(array $post): array
{
    return AdminAjaxSetting::saveCalendarOptions($post);
}

/**
 * Save booking options
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_settings_booking_options($post): array
{
    return AdminAjaxSetting::saveBookingOptions($post);
}

/**
 * Saves site lesson times
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_lesson_times(array $post): array
{
    return AdminAjaxSetting::saveLessonTimes($post);
}

/**
 * Save header brand settings
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_settings_header_brand($post): array
{
    return AdminAjaxSetting::saveHeaderBrand($post);
}


/* ============================================================================
 *  Users
 * ============================================================================
 */

/**
 * Saves new user
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_user(array $post): array
{
    return AdminAjaxSetting::saveUser($post);
}

/**
 * Deletes user from database
 *
 * @param   int    $user_id
 *
 * @return  array
 */
function rbs__delete_user(int $user_id): array
{
    return AdminAjaxSetting::deleteUser($user_id);
}
