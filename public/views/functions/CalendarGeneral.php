<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_CALENDAR')) {
    header('Location: ../../');
    exit;
}


class CalendarGeneral
{

    /**
     * Returns array of booked times of specific day
     * 
     * @param   string  $date
     * @param   int     [$object_id]
     *
     * @return  array
     */
    public static function getBookedTimes(string $date, int $object_id = 0): array
    {
        global $render_view;

        $date = rbs_convert_date($date);
        $weekday = date('w', strtotime($date));
        if (!$object_id) $object_id = rbs_get_sel_obj_id();
        $bookings = $render_view->getBookingsByDate(['date' => $date, 'object_id' => $object_id]);
        $lesson_times = rbs_get_setting('lesson_times');
        $result = [];
        $booked_devices = [];

        if ($bookings['success']) {
            foreach ($bookings['result'] as $booking) {
                if (
                    $booking->booking_repeat == 'true' &&
                    !in_array($weekday, json_decode($booking->week_days, true))
                ) continue;  // repeating booking does not occur in this day

                $_from = substr(explode(' ', $booking->booking_from)[1], 0, 5);
                $_to = substr(explode(' ', $booking->booking_to)[1], 0, 5);

                foreach ($lesson_times as $lesson => $times) {
                    if ($times['begin'] >= $_from && $times['end'] <= $_to) $result[] = $lesson;
                }

                if (!isset($booked_devices[$lesson])) $booked_devices[$lesson] = 0;
                $booked_devices[$lesson] += ($booking->devices_count ?? 0);
            }
        }
        sort($result);
        $booked_lessons = array_unique($result);

        return compact('booked_lessons', 'booked_devices');
    }

    /**
     * Gets object types from database
     *
     * @param   bool    [$include_also_empty_types]
     *
     * @return  array
     */
    public static function getObjectTypes(bool $include_also_empty_types = true): array
    {
        global $render_view;

        if ($include_also_empty_types) $result = $render_view->getAllObjectsTypes();
        else $result = $render_view->getObjectTypesWithObject();

        if ($result['success']) {
            $obj_types = [];
            foreach ($result['result'] as $obj_type) $obj_types[$obj_type->id] = $obj_type;
            return $obj_types;
        }
        else return ['1' => (object)['id' => 1, 'name' => 'ruum']];
    }

    /**
     * Check if selected event can be changed by user or not
     *
     * @param   int     $uid
     * @param   int     $date
     * @param   string  $sel_date
     *
     * @return  bool
     */
    public static function canChangeEvent(int $uid, string $date, string $sel_date): bool
    {
        if (strtotime($date) < time() || strtotime($sel_date) < time()) return false;
        if (!rbs_is_logged_in()) return false;
        if (rbs_is_admin()) return true;

        return $uid == rbs_get_setting('user')['id'];
    }

    /**
     * Returns booking data by ID
     *
     * @param   int  $booking_id
     *
     * @return  array
     */
    public static function getBookingData(int $booking_id): array
    {
        global $render_view;

        $result = $render_view->getBooking($booking_id);
        $all_day = 'false';

        if ($result['success']) {
            $_booking = $result['result'];
            $_repeat = $_booking->booking_repeat == 'true';

            // $_from = strtotime($_POST['date']);
            $_lse = rbs_get_setting('lessons_start_end');

            $start = date('H:i', strtotime($_booking->booking_from));
            $end = date('H:i', strtotime($_booking->booking_to));
            if ($start == $_lse['start'] && $end == $_lse['end']) $all_day = 'true';

            return [
                'title'         => $_booking->title,
                'start'         => $start,
                'end'           => $end,
                'all_day'       => $all_day,
                'repeat'        => $_repeat,
                'week_days'     => (
                    $_repeat ?
                    json_decode($_booking->week_days, true) :
                    [date('w', strtotime(rbs_convert_date($_POST['date'])))]
                ),
                'devices_count' => ($_booking->devices_count ?? 0),
                'devices_all'   => ($_booking->devices_all ?? '0') == '1',
                'date'          => $_POST['date'],//date('d.m.Y', $_from),
                'recur_date'    => (
                    $_repeat ?
                    date('Y-m-d', strtotime($_booking->booking_to) - 86400) :
                    date('Y-m-d', strtotime($_booking->booking_from .'+7 DAYS'))
                )
            ];
        }
        else return [  /// should not happen
            'title'   => '(TUNDMATU)',
            'start'   => '08:00', 'end' => '08:45',
            'all_day' => false,
            'repeat'  => false, 'date' => date('d.m.Y')
        ];
    }

    /**
     * Returns site lesson times
     *
     * @return  array
     */
    public static function getLessonTimes(): array
    {
        $lesson_times = rbs_get_setting('lesson_times');
        $return = [];

        foreach ($lesson_times as $title => $lesson_data) {
            $b = explode(':', $lesson_data['begin']);
            $e = explode(':', $lesson_data['end']);
            $begin = $b[0] + $b[1] / 60;
            $end = $e[0] + $e[1] / 60;
            $return[] = compact('title', 'begin', 'end');
        }
        return $return;
    }

    /**
     * To show filter button or not
     * 
     * @return  bool
     */
    public static function showFilter(): bool
    {
        global $render_view;

        $result = $render_view->getObjectsCount();

        if (!$result['success']) return false;  // nothing to filter
        else return $result['result'][0]->count > 1;
    }

    /**
     * Gets devices count in set by device object ID
     *
     * @param   int   $object_id
     *
     * @return  int
     */
    public static function getDevicesCountInSet(int $object_id): int
    {
        global $render_view;

        $result = $render_view->getDevicesCountInSet($object_id);

        if ($result['success']) return $result['result']->devices_count;
        else return 0;
    }

}