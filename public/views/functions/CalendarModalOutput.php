<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_CALENDAR')) {
    header('Location: ../../');
    exit;
}


class CalendarModalOutput
{

    /**
     * Prints out modal calendar filter object types and returns array of object type names
     *
     * @param   int  [$no_tabs]
     *
     * @return  array
     */
    public static function filterObjectTypes(int $no_tabs = 0): array
    {
        $to_print = [];
        $obj_types = [];
        $sel_obj_type = rbs_get_sel_obj_type_id() ?? 1;

        foreach (rbs_get_object_types(false) as $obj_type) {
            $obj_types[ $obj_type->id ] = $obj_type->name;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<option value="##VALUE##"##SELECTED##>##NAME##</option>', 'replaces' => [
                '##VALUE##'     => $obj_type->id,
                '##SELECTED##'  => ($sel_obj_type == $obj_type->id ? ' selected' : '' ),
                '##NAME##'      => $obj_type->name
            ]];
        }
        rbs_print_data($to_print);
        return $obj_types;
    }

    /**
     * Prints out modal calendar filter objects and returns array of object names
     *
     * @param   int  [$no_tabs]
     *
     * @return  array
     */
    public static function filterObjects(int $no_tabs = 0): array
    {
        $to_print = [];
        $objects = [];
        $sel_object = rbs_get_sel_obj_id() ?? 1;

        foreach (rbs__get_resources(rbs_get_sel_obj_type_id()) as $resource) {
            $objects[$resource->id] = $resource->object_name;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<option value="##VALUE##"##SELECTED##>##NAME##</option>', 'replaces' => [
                '##VALUE##'     => $resource->id,
                '##SELECTED##'  => ($sel_object == $resource->id ? ' selected' : '' ),
                '##NAME##'      => $resource->object_name
            ]];
        }
        rbs_print_data($to_print);
        return $objects;
    }

    /**
     * Prints out modal calendar available devices count
     *
     * @param   int   [$no_tabs]
     * @param   int   [$booked_devices]
     * @param   int   [$devices_count]
     * @param   bool  [$devices_all]
     * 
     * @return  int
     */
    public static function devicesCount(int $no_tabs = 0, int $booked_devices = 0, int $devices_count = 0, bool $devices_all = true): int
    {
        $cnt_devices = rbs_get_devices_count(rbs_get_sel_obj_id());
        $free_devices = $cnt_devices - $booked_devices;

        $to_print = [
            [
                'no_tabs'   => $no_tabs,
                'string'    => '<option value="##VALUE##"##SELECTED##>##NAME##</option>',
                'replaces'  => [
                    '##VALUE##'     => $free_devices,
                    '##SELECTED##'  => ($devices_all || $devices_count == $free_devices ? ' selected' : '' ),
                    '##NAME##'      => "Kõik ({$free_devices})"
                ]
            ]
        ];

        for ($i = 1; $i < $free_devices; $i++) {
            $to_print[] = [
                'no_tabs'   => $no_tabs,
                'string'    => '<option value="##VALUE##"##SELECTED##>##NAME##</option>',
                'replaces'  => [
                    '##VALUE##' => $i,
                    '##SELECTED##'  => ($devices_count == $i ? ' selected' : '' ),
                    '##NAME##' => $i
                ]
            ];
        }
        rbs_print_data($to_print);
        return $cnt_devices;
    }

    /**
     * Prints out modal calendar booking lesson times
     *
     * @param   int     $no_tabs
     * @param   array   $lesson_times
     * @param   string  $start_time
     * @param   string  $end_time
     * @param   bool    $is_new_booking
     * @param   array   $booked_times
     */
    public static function bookingLessons(
            int $no_tabs, array $lesson_times, string $start_time,
            string $end_time, bool $is_new_booking, array $booked_times
        )
    {
        $to_print = [];

        for ($i = 0; $i < 10; $i++) {
            $_k = "{$i}.";

            if (array_key_exists($_k, $lesson_times)) {
                $to_print[] = [
                    'no_tabs' => $no_tabs,
                    'string' => '<div class="col">', 'replaces' => []
                ];
                $to_print[] = [
                    'no_tabs' => $no_tabs + 1,
                    'string' => '<div class="form-check form-check-inline">', 'replaces' => []
                ];

                $to_print[] = [
                    'no_tabs'   => $no_tabs + 2,
                    'string'    => '<input class="form-check-input" type="checkbox" id="rbs_select_booking_lesson_##IDX##" name="rbs_select_booking_lesson_##IDX##" value="##VALUE##"##CHECKED##>',
                    'replaces'  => [
                        '##IDX##'       => $i,
                        '##VALUE##'     => "{$lesson_times[$_k]['begin']};{$lesson_times[$_k]['end']}",
                        '##CHECKED##'   => (in_array($_k, $booked_times)
                                            ? ($is_new_booking
                                                ? ' disabled'
                                                : ($start_time < $lesson_times[$_k]['end'] && $end_time > $lesson_times[$_k]['begin']
                                                    ? ' checked' : ' disabled'))
                                            : ($start_time < $lesson_times[$_k]['end'] && $end_time > $lesson_times[$_k]['begin']
                                                ? ' checked' : ''))
                    ]
                ];
                $to_print[] = [
                    'no_tabs'   => $no_tabs + 2,
                    'string'    => '<label class="form-check-label" for="rbs_select_booking_lesson_##IDX##">##IDX##. t</label>',
                    'replaces'  => ['##IDX##' => $i]
                ];

                $to_print[] = [
                    'no_tabs' => $no_tabs + 1,
                    'string' => '</div>', 'replaces' => []
                ];
                $to_print[] = [
                    'no_tabs' => $no_tabs,
                    'string' => '</div>', 'replaces' => []
                ];
            }
        }
        rbs_print_data($to_print);
    }

    /**
     * Prints out modal calendar weekdays
     *
     * @param   int     $no_tabs
     * @param   array   $week_days
     * @param   bool    $repeat
     */
    public static function bookingWeekdays(int $no_tabs, array $week_days, bool $repeat)
    {
        $weekends = rbs_get_setting('calendar_options.weekends', false);

        $_weekdays = ['Püh', 'Esm', 'Tei', 'Kol', 'Nel', 'Ree', 'Lau', 'Püh'];
        $_cols = ($weekends ? 7 : 5);

        $to_print = [[
            'no_tabs' => $no_tabs,
            'string' => '<div id="rbs_select_booking_recur_rate" class="row row-cols-##COLS## mb-3 pb-2 bg-light border border-top-0##HIDE##">',
            'replaces' => [
                '##COLS##'  => $_cols,
                '##HIDE##'  => ($repeat ? '' : ' d-none')
            ]
        ]];

        for ($i = 1; $i <= $_cols; $i++) {
            $_i = $i % 7;

            $to_print[] = [
                'no_tabs' => $no_tabs + 1,
                'string' => '<div class="col">', 'replaces' => []
            ];
            $to_print[] = [
                'no_tabs' => $no_tabs + 2,
                'string' => '<div class="form-check form-check-inline">', 'replaces' => []
            ];

            $to_print[] = [
                'no_tabs'   => $no_tabs + 3,
                'string'    => '<input class="form-check-input" type="checkbox" id="rbs_repeat_weekdays_##IDX##" name="rbs_repeat_weekdays_##IDX##" value="##IDX##"##CHECKED##>',
                'replaces'  => [
                    '##IDX##'       => $_i,
                    '##CHECKED##'   => (in_array($_i, $week_days) ? ' checked' : '')
                ]
            ];
            $to_print[] = [
                'no_tabs'   => $no_tabs + 3,
                'string'    => '<label class="form-check-label" for="rbs_select_weekday_##IDX##">##WEEKDAY##</label>',
                'replaces'  => [
                    '##IDX##'       => $_i,
                    '##WEEKDAY##'   => $_weekdays[$i]
                ]
            ];

            $to_print[] = [
                'no_tabs' => $no_tabs + 2,
                'string' => '</div>', 'replaces' => []
            ];
            $to_print[] = [
                'no_tabs' => $no_tabs + 1,
                'string' => '</div>', 'replaces' => []]
                ;
        }

        $to_print[] = [
            'no_tabs' => $no_tabs,
            'string' => '</div>', 'replaces' => []
        ];

        rbs_print_data($to_print);
    }

}