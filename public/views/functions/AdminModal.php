<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_ADMIN')) {
    header('Location: ../../');
    exit;
}


class AdminModal
{

    /**
     * Prints out modal calendar booking users
     *
     * @param   int     [$no_tabs]
     * @param   string  [$sel_user]
     */
    public static function printCalendarBookingUsers(int $no_tabs = 0, string $sel_user = '')
    {
        global $render_view;

        $user = rbs_get_setting('user');
        $_admin = "{$user['firstname']} {$user['lastname']}";

        $to_print = [];
        $users = $render_view->getAllUsers();


        if ($users['success']) {
            foreach ($users['result'] as $usr) {
                $_user = "{$usr->first_name} {$usr->last_name}";
                if ($_user == $_admin) continue;

                $to_print[] = [
                    'no_tabs'   => $no_tabs,
                    'string'    => '<option value="##NAME##"##SELECTED##>##NAME##</option>',
                    'replaces'  => [
                        '##NAME##'      => $_user,
                        '##SELECTED##'  => ($_user == $sel_user ? ' selected' : '')
                    ]
                ];
            }
            rbs_print_data($to_print);
        }
    }

    /**
     * Prints out modal add-change device types
     *
     * @param   string  $id
     * @param   string  $next_id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_type]
     * @param   bool    [$return_str]
     * 
     * @return  int|string
     */
    public static function printDeviceTypes(
            string $id, string $next_id, string $text, int $no_tabs = 0,
            int $sel_type = 0, bool $return_str = false
        )
    {
        global $render_view;

        return self::_printSelectOptions(
            $render_view->getAllDeviceTypes(),
            $id, $next_id, $text, $no_tabs, $sel_type,
            true, 'id', 'name', $return_str
        );
    }

    /**
     * Prints out modal add-change device manufacturers
     *
     * @param   string  $id
     * @param   string  $next_id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_manufacturer]
     * @param   bool    [$return_str]
     * 
     * @return  int|string
     */
    public static function printDeviceManufacturers(
            string $id, string $next_id, string $text, int $no_tabs = 0,
            int $sel_manufacturer = 0, bool $return_str = false
        )
    {
        global $render_view;

        return self::_printSelectOptions(
            $render_view->getDeviceManufacturers(),
            $id, $next_id, $text, $no_tabs, $sel_manufacturer,
            true, 'id', 'name', $return_str
        );
    }

    /**
     * Prints out modal add-change device models
     *
     * @param   string  $id
     * @param   string  $next_id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_model]
     * @param   bool    [$return_str]
     * 
     * @return  int|string
     */
    public static function printDeviceModels(
            string $id, string $next_id, string $text, int $no_tabs = 0,
            int $sel_model = 0, bool $return_str = false
        )
    {
        global $render_view;

        return self::_printSelectOptions(
            $render_view->getDeviceModels(),
            $id, $next_id, $text, $no_tabs, $sel_model,
            true, 'id', 'name', $return_str
        );
    }

    /**
     * Prints out modal add-change device locations
     *
     * @param   string  $id
     * @param   string  $next_id
     * @param   string  $text
     * @param   int     [$no_tabs]
     * @param   int     [$sel_location]
     * @param   bool    [$return_str]
     * @param   bool    [$disable_room]
     * 
     * @return  int|string
     */
    public static function printDeviceLocations(
            string $id, string $next_id, string $text, int $no_tabs = 0,
            int $sel_location = 0, bool $return_str = false, bool $disable_room = false
        )
    {
        global $render_view;

        return self::_printSelectOptions(
            $render_view->objectsByType(['type_id' => 1, 'not_for_booking' => false]),
            $id, $next_id, $text, $no_tabs, $sel_location,
            false, 'rbs__room__id', 'object_name', $return_str, $disable_room
        );
    }


    /**
     * Returns device object data for modal window
     * 
     * @return  array
     */
    public static function getDeviceObject(): array
    {
        global $render_view;

        $return = [
            'id' => 0,
            'device_name' => '',
            'device_type_id' => 0,
            'device_manufacturer_id' => 0,
            'device_model_id' => 0,
            'objects_count' => 1,
            // 'count_devices' => 0,  // real number of added devices
            'room_id' => 0,
            'info' => ''
        ];

        if (isset($_POST['device_object_id'])) {
            $result = $render_view->getDeviceObjectById($_POST['device_object_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $return =  [
                    'id'                      => $_data->id,
                    'device_name'             => $_data->object_name,
                    'device_type_id'          => $_data->rbs__object_type__id,
                    'device_manufacturer_id'  => $_data->rbs__device_manufacturer__id,
                    'device_model_id'         => $_data->rbs__device_model__id,
                    'objects_count'           => $_data->objects_count,
                    // 'count_devices'           => $_data->count_devices,
                    'room_id'                 => $_data->rbs__room__id ?? 0,
                    'info'                    => $_data->info ?? ''
                ];
            }
        }
        return $return;
    }

    /**
     * Returns device type data for modal window
     * 
     * @return  array
     */
    public static function getDeviceType(): array
    {
        global $render_view;

        $return = ['id' => 0, 'name' => ''];

        if (isset($_POST['device_type_id']) && $_POST['device_type_id'] > 1) {  // ID "1" is for room
            $result = $render_view->getDeviceTypeById($_POST['device_type_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $return =  [
                    'id'   => $_data->id,
                    'name' => $_data->name
                ];
            }
        }
        return $return;
    }

    /**
     * Returns device manufacturer data for modal window
     * 
     * @return  array
     */
    public static function getManufacturer(): array
    {
        global $render_view;

        $return = ['id' => 0, 'name' => ''];

        if (isset($_POST['manufacturer_id'])) {
            $result = $render_view->getDeviceManufacturerById($_POST['manufacturer_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $return =  [
                    'id'   => $_data->id,
                    'name' => $_data->name
                ];
            }
        }
        return $return;
    }

    /**
     * Returns device model data for modal window
     * 
     * @return  array
     */
    public static function getModel(): array
    {
        global $render_view;

        $return = ['id' => 0, 'name' => ''];

        if (isset($_POST['model_id'])) {
            $result = $render_view->getDeviceModelById($_POST['model_id']);

            if ($result['success']) {
                $_data = $result['result'];
                $return =  [
                    'id'   => $_data->id,
                    'name' => $_data->name
                ];
            }
        }
        return $return;
    }

    /**
     * Returns array of device IDs in group
     *
     * @return  array
     */
    public static function getDeviceIds(): array
    {
        global $render_view;

        $return = [];

        if (isset($_POST['device_object_id'])) {
            $result = $render_view->getDeviceIds($_POST['device_object_id']);

            if ($result['success']) {
                foreach ($result['result'] as $_val) $return[] = $_val->id;
            }
        }
        $return[] = -1;
        return $return;
    }


    // ========================================================================


    /*
     * Helper function for printing options for HTML select
     */
    private static function _printSelectOptions(
        array $data, string $id, string $next_id, string $text, int $no_tabs,
        int $sel_id, bool $disable_option = true, string $col_id = 'id',
        string $col_name = 'name', bool $ret_str = false, bool $disable_room = false
        )
    {
        $to_print = [];
        $cnt = 1;
        $_disabled = (!count($data['result']) || $sel_id) && $disable_option || $disable_room;

        if ($data['success']) {
            if (!$ret_str) $to_print[] = [
                    'no_tabs'   => $no_tabs,
                    'string'    => "<select class=\"form-select##SIZE##\" id=\"{$id}\" name=\"{$id}\" data-rbs-next-input=\"{$next_id}\"##DISABLED##>",
                    'replaces'  => [
                        '##SIZE##'     => ($_disabled ? ' form-select-sm' : ''),
                        '##DISABLED##' => ($_disabled ? ' disabled' : ''),
                        // '##REQUIRED##' => ($_disabled ? '' : ' required')
                    ]
                ];

            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option##VALUE####SELECTED####DISABLED##>##NAME##</option>',
                'replaces'  => [
                    '##VALUE##'     => ($_disabled ? '' : ' value="0"'),
                    '##SELECTED##'  => (!$sel_id ? ' selected' : ''),
                    '##DISABLED##'  => ($disable_option ? ' disabled' : ''),
                    '##NAME##'      => $text
                ]
            ];

            foreach ($data['result'] as $_data) {
                $cnt = 0;

                $to_print[] = [
                    'no_tabs'   => $no_tabs + 1,
                    'string'    => '<option value="##ID##"##SELECTED##>##NAME##</option>',
                    'replaces'  => [
                        '##ID##'        => $_data->id,
                        '##SELECTED##'  => (
                            $sel_id && $_data->{$col_id} && $_data->{$col_id} == $sel_id ?
                            ' selected' : ''
                        ),
                        '##NAME##'      => $_data->{$col_name}
                    ]
                ];
            }
            if (!$ret_str) $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</select>', 'replaces' => []];
        }
        else {
            if (!$ret_str) $to_print[] = [
                    'no_tabs'   => $no_tabs,
                    'string'    => "<select class=\"form-select\" id=\"{$id}\" name=\"{$id}\" data-rbs-next-input=\"{$next_id}\" disabled>",
                    'replaces'  => []
                ];

            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option selected disabled>##NAME##</option>',
                'replaces'  => ['##NAME##' => $text]
            ];
            if (!$ret_str) $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</select>', 'replaces' => []];
        }

        if ($ret_str) return rbs_get_datastring($to_print);
        else {
            rbs_print_data($to_print);
            return $cnt;
        }
    }

}