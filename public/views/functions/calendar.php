<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}
define("RBS_APP_CLS_CALENDAR", true);  // for Calendar classes

foreach(['General', 'ModalOutput', 'Ajax'] as $cls)
    require_once "Calendar{$cls}.php";


/**
 * Prints out modal calendar filter object types and returns array of object type names
 *
 * @param   int  [$no_tabs]
 *
 * @return  array
 */
function rbs_print_m_calendar_filter_object_types(int $no_tabs = 0): array
{
    return CalendarModalOutput::filterObjectTypes($no_tabs);
}

/**
 * Prints out modal calendar filter objects and returns array of object names
 *
 * @param   int  [$no_tabs]
 *
 * @return  array
 */
function rbs_print_m_calendar_filter_objects(int $no_tabs = 0): array
{
    return CalendarModalOutput::filterObjects($no_tabs);
}

/**
 * Prints out modal calendar available devices count
 *
 * @param   int   [$no_tabs]
 * @param   int   [$booked_devices]
 * @param   int   [$devices_count]
 * @param   bool  [$devices_all]
 * 
 * @return  int
 */
function rbs_print_m_calendar_devices_count(...$params): int
{
    return CalendarModalOutput::devicesCount(...$params);
}

/**
 * Prints out modal calendar booking lesson times
 *
 * @param   int     $no_tabs
 * @param   array   $lesson_times
 * @param   string  $start_time
 * @param   string  $end_time
 * @param   bool    $is_new_booking
 * @param   array   $booked_times
 */
function rbs_print_m_calendar_booking_lessons(...$params)
{
    CalendarModalOutput::bookingLessons(...$params);
}

/**
 * Prints out modal calendar weekdays
 *
 * @param   int     $no_tabs
 * @param   array   $week_days
 * @param   bool    $repeat
 */
function rbs_print_m_calendar_booking_weekdays(...$params)
{
    CalendarModalOutput::bookingWeekdays(...$params);
}


/**
 * Returns array of booked times of specific day
 * 
 * @param   string  $date
 * @param   int     [$object_id]
 *
 * @return  array
 */
function rbs_get_booked_times(...$params): array
{
    return CalendarGeneral::getBookedTimes(...$params);
}

/**
 * Gets object types from database
 *
 * @param   bool    [$include_also_empty_types]
 *
 * @return  array
 */
function rbs_get_object_types(bool $include_also_empty_types = true): array
{
    return CalendarGeneral::getObjectTypes($include_also_empty_types);
}

/**
 * Check if selected event can be changed by user or not
 *
 * @param   int     $uid
 * @param   int     $date
 * @param   string  $sel_date
 *
 * @return  bool
 */
function rbs_can_change_event(...$params): bool
{
    return CalendarGeneral::canChangeEvent(...$params);
}

/**
 * Returns booking data by ID
 *
 * @param   int  $booking_id
 *
 * @return  array
 */
function rbs_get_booking_data(int $booking_id): array
{
    return CalendarGeneral::getBookingData($booking_id);
}

/**
 * Returns site lesson times
 *
 * @return  array
 */
function rbs_get_lesson_times(): array
{
    return CalendarGeneral::getLessonTimes();
}

/**
 * To show filter button or not
 *
 * @return  bool
 */
function rbs_show_filter(): bool
{
    return CalendarGeneral::showFilter();
}

/**
 * Gets devices count in set by device object ID
 *
 * @param   int   $object_id
 *
 * @return  int
 */
function rbs_get_devices_count($object_id): int
{
    return CalendarGeneral::getDevicesCountInSet($object_id);
}


/**
 * Gets fullcalendar optionas
 *
 * @return  array
 */
function rbs_get_calendar_options(): array
{
    return rbs_read_setting('calendar_options', [], true);
}

/**
 * Helper function to get selected object type ID
 *
 * @return   int
 */
function rbs_get_sel_obj_type_id(): int
{
    return $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_ID'];
}

/**
 * Helper function to get selected object ID
 *
 * @return   int
 */
function rbs_get_sel_obj_id(): int
{
    return $_SESSION['CALENDAR_FILTER_OBJECT_ID'];
}

/**
 * Helper function to get selected object type name
 *
 * @return   string
 */
function rbs_get_sel_obj_type_name(): string
{
    return $_SESSION['CALENDAR_FILTER_OBJECT_TYPE_NAME'];
}

/**
 * Helper function to get selected object string
 *
 * @return   string
 */
function rbs_get_sel_obj_name(): string
{
    return $_SESSION['CALENDAR_FILTER_OBJECT_NAME'];
}


/* ****************************************************************************
 * **    AJAX response functions
 * ****************************************************************************
 */

/**
 * Save calendar filter selection to the SESSION
 *
 * @param   array   $post
 * @param   string  [$goto_date]
 */
function rbs__calendar_filter(...$params)
{
    CalendarAjax::calendarFilter(...$params);
}

/**
 * Returns list of objects by type
 *
 * @param   int   $type_id
 * 
 * @return  string
 */
function rbs__calendar_filter_get_objects(int $type_id): string
{
    return CalendarAjax::filterGetObjects($type_id);
}

/**
 * Return selected week bookings (requested by Fullcalendar)
 *
 * @param   string  $start
 * @param   string  $end
 *
 * @return  array
 */
function rbs__calendar_bookings_source(...$params): array
{
    return CalendarAjax::bookingsSource(...$params);
}


/**
 * Returns calendar booked times
 *
 * @param   string  $date
 * @param   int     $object_id
 *
 * @return  array
 */
function rbs__get_calendar_booked_times(...$params): array
{
    return CalendarAjax::getBookedTimes(...$params);
}

/**
 * Saves new calendar booking
 *
 * @param   array   $post
 *
 * @return  array
 */
function rbs__save_calendar_booking(array $post): array
{
    return CalendarAjax::saveBooking($post);
}

/**
 * Deletes calendar booking
 *
 * @param   int   $booking_id
 *
 * @return  array
 */
function rbs__delete_calendar_booking(int $booking_id): array
{
    return CalendarAjax::deleteBooking($booking_id);
}

/**
 * Gets resources from database by resource type ID
 *
 * @param   int     $type_id
 * @param   bool    [$hide_from_booking]
 *
 * @return  array
 */
function rbs__get_resources(...$params): array
{
    return CalendarAjax::getResources(...$params);
}

/**
 * Gets devices by device object ID
 *
 * @param   int     $object_id
 *
 * @return  array
 */
function rbs__get_devices($object_id): array
{
    return CalendarAjax::getDevices($object_id);
}
