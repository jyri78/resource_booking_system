<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION') || !defined('RBS_APP_CLS_ADMIN')) {
    header('Location: ../../');
    exit;
}


class AdminPageOutput
{

    /**
     * Prints out page device types table
     *
     * @param   int  [$no_tabs]
     * 
     * @return  array
     */
    public static function deviceTypesTable(int $no_tabs = 0): array
    {
        $to_print = [];
        $ids = [];
        $_table_show_ids = rbs_get_setting('table_show_ids');

        foreach(rbs_get_device_types() as $type_id => $obj_type) {
            $ids[] = $type_id;
            $_type_name = $obj_type->name;
            $_no_objects = (int)$obj_type->no_objects;
            $_cant_delete = $_no_objects > 0;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr>', 'replaces' => []];

            $_table_show_ids && $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##ID##</td>',
                'replaces' => ['##ID##' => $type_id]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1,
                'string'   => '<td><span id="rbs_device_type_name_##ID##">##TYPE##</span></td>',
                'replaces' => [
                    '##ID##'    => $type_id,
                    '##TYPE##'  => $_type_name
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##COUNT##</td>',
                'replaces' => ['##COUNT##' => $_no_objects]
            ];
            // $to_print[] = [
            //     'no_tabs'  => $no_tabs + 1, 'string'   => '<td>##BOOKINGS##</td>',
            //     'replaces' => ['##BOOKINGS##' => 0]
            // ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##UPDATED##</td>',
                'replaces' => ['##UPDATED##' => $obj_type->updated]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="btn-group btn-group-sm">', 'replaces' => []];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button" id="rbs_device_type_edit_##ID##" class="btn btn-primary">##EDIT_ICON##</button>',
                'replaces' => [
                    '##ID##'        => $type_id,
                    '##EDIT_ICON##' => rbs_get_icon(['name' => 'pencil-square'])
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 2,
                'string'   => '<button type="button"##ID## class="btn btn-sm btn-##OPACITY##"##DISABLED##>##DEL_ICON##</button>',
                'replaces' => [
                    '##ID##'       => ($_cant_delete ? '' : " id=\"rbs_device_type_delete_{$type_id}\""),
                    '##OPACITY##'  => ($_cant_delete ? 'outline-danger opacity-25' : 'danger'),
                    '##DISABLED##' => ($_cant_delete ? ' disabled' : ''),
                    '##DEL_ICON##' => rbs_get_icon(['name' => 'trash'])
                ]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</td>', 'replaces' => []];
        }
        rbs_print_data($to_print);
        return $ids;
    }

    /**
     * Prints out page device types table
     *
     * @param   int  [$no_tabs]
     * 
     * @return  array
     */
    public static function manufacturersTable(int $no_tabs = 0): array
    {
        $to_print = [];
        $ids = [];
        $_table_show_ids = rbs_get_setting('table_show_ids');

        foreach(rbs_get_device_manufacturers() as $_id => $manufacturer) {
            $ids[] = $_id;
            $_name = $manufacturer->name;
            $_no_objects = $manufacturer->no_objects;
            $_cant_delete = $_no_objects > 0;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr>', 'replaces' => []];

            $_table_show_ids && $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##ID##</td>',
                'replaces' => ['##ID##' => $_id]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td><span id="rbs_manufacturer_name_##ID##">##NAME##</span></td>',
                'replaces' => [
                    '##ID##'   => $_id,
                    '##NAME##' => $_name
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##COUNT##</td>',
                'replaces' => ['##COUNT##' => $_no_objects]
            ];
            // $to_print[] = [
            //     'no_tabs'  => $no_tabs + 1, 'string'   => '<td>##BOOKINGS##</td>',
            //     'replaces' => ['##BOOKINGS##' => 0]
            // ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##UPDATED##</td>',
                'replaces' => ['##UPDATED##' => $manufacturer->updated]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="btn-group btn-group-sm">', 'replaces' => []];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button" id="rbs_manufacturer_edit_##ID##" class="btn btn-primary">##EDIT_ICON##</button>',
                'replaces' => [
                    '##ID##'        => $_id,
                    '##EDIT_ICON##' => rbs_get_icon(['name' => 'pencil-square'])
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button"##ID## class="btn btn-sm btn-##OPACITY##"##DISABLED##>##DEL_ICON##</button>',
                'replaces' => [
                    '##ID##'       => ($_cant_delete ? '' : " id=\"rbs_manufacturer_delete_{$_id}\""),
                    '##OPACITY##'  => ($_cant_delete ? 'outline-danger opacity-25' : 'danger'),
                    '##DISABLED##' => ($_cant_delete ? ' disabled' : ''),
                    '##DEL_ICON##' => rbs_get_icon(['name' => 'trash'])
                ]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</td>', 'replaces' => []];
        }
        rbs_print_data($to_print);
        return $ids;
    }

    /**
     * Prints out page models table
     *
     * @param   int  [$no_tabs]
     * 
     * @return  array
     */
    public static function modelsTable(int $no_tabs = 0): array
    {
        $to_print = [];
        $ids = [];
        $_table_show_ids = rbs_get_setting('table_show_ids');

        foreach(rbs_get_device_models() as $_id => $model) {
            $ids[] = $_id;
            $_name = $model->name;
            $_no_objects = $model->no_objects;
            $_cant_delete = $_no_objects > 0;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr>', 'replaces' => []];

            $_table_show_ids && $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##ID##</td>',
                'replaces' => ['##ID##' => $_id]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td><span id="rbs_model_name_##ID##">##NAME##</span></td>',
                'replaces' => [
                    '##ID##'   => $_id,
                    '##NAME##' => $_name
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##COUNT##</td>',
                'replaces' => ['##COUNT##' => $_no_objects]
            ];
            // $to_print[] = [
            //     'no_tabs'  => $no_tabs + 1, 'string'   => '<td>##BOOKINGS##</td>',
            //     'replaces' => ['##BOOKINGS##' => 0]
            // ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##UPDATED##</td>',
                'replaces' => ['##UPDATED##' => $model->updated]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="btn-group btn-group-sm">', 'replaces' => []];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button" id="rbs_model_edit_##ID##" class="btn btn-primary">##EDIT_ICON##</button>',
                'replaces' => [
                    '##ID##'        => $_id,
                    '##EDIT_ICON##' => rbs_get_icon(['name' => 'pencil-square'])
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button"##ID## class="btn btn-sm btn-##OPACITY##"##DISABLED##>##DEL_ICON##</button>',
                'replaces' => [
                    '##ID##'       => ($_cant_delete ? '' : " id=\"rbs_model_delete_{$_id}\""),
                    '##OPACITY##'  => ($_cant_delete ? 'outline-danger opacity-25' : 'danger'),
                    '##DISABLED##' => ($_cant_delete ? ' disabled' : ''),
                    '##DEL_ICON##' => rbs_get_icon(['name' => 'trash'])
                ]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</td>', 'replaces' => []];
        }
        rbs_print_data($to_print);
        return $ids;
    }

    /**
     * Prints out page settings lesson times table
     *
     * @param   int  [$no_tabs]
     */
    public static function settingsLessonTimes(int $no_tabs = 0)
    {
        $to_print = [];
        $_lt = rbs_get_setting('lesson_times');

        for ($i = 0; $i < 10; $i++) {
            $_k = "{$i}.";
            $_ake = array_key_exists($_k, $_lt);
            $checked = ($_ake ? ' checked' : '');
            $text = ($_ake ? '' : ' text-black-50');
            $class = ($_ake ? '' : ' class="opacity-25"');
            $begin = ($_ake ? ' value="'. $_lt[$_k]['begin'] .'"' : ' disabled');
            $end = ($_ake ? ' value="'. $_lt[$_k]['end'] .'"' : ' disabled');

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="form-check form-check-inline form-switch mt-1">', 'replaces' => []];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<input type="checkbox" class="form-check-input" id="rbs_select_lesson_##IDX##" name="rbs_select_lesson_##IDX##" value="##IDX##"##CHECKED##>',
                'replaces' => [
                    '##IDX##'       => $i,
                    '##CHECKED##'   => $checked
                ]
            ];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<label class="form-check-label##TEXT##" id="rbs_sll_##IDX##" for="rbs_select_lesson_##IDX##">##IDX##. tund</label>',
                'replaces' => [
                    '##TEXT##'  => $text,
                    '##IDX##'   => $i
                ]
            ];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];

            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td id="rbs_ls_##IDX##"##CLASS##>', 'replaces' => [
                '##IDX##'   => $i,
                '##CLASS##' => $class
            ]];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="input-group input-group-sm">', 'replaces' => []];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<label class="input-group-text" for="rbs_lesson_begin_##IDX##">Algus</label>',
                'replaces' => ['##IDX##' => $i]
            ];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<input type="time" class="form-control" id="rbs_lesson_begin_##IDX##" name="rbs_lesson_begin_##IDX##"##BEGIN##>',
                'replaces' => [
                    '##IDX##'   => $i,
                    '##BEGIN##'  => $begin
                ]
            ];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];

            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td id="rbs_le_##IDX##"##CLASS##>', 'replaces' => [
                '##IDX##'   => $i,
                '##CLASS##' => $class
            ]];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="input-group input-group-sm">', 'replaces' => []];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<label class="input-group-text" for="rbs_lesson_end_##IDX##">Lõpp</label>',
                'replaces' => ['##IDX##' => $i]
            ];
            $to_print[] = [
                'no_tabs' => $no_tabs + 3,
                'string' => '<input type="time" class="form-control" id="rbs_lesson_end_##IDX##" name="rbs_lesson_end_##IDX##"##END##>',
                'replaces' => [
                    '##IDX##'  => $i,
                    '##END##'  => $end
                ]
            ];
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</tr>', 'replaces' => []];
        }
        rbs_print_data($to_print);
    }

    /**
     * Prints out header background styles selection
     *
     * @param   int    [$no_tabs]
     */
    public static function settingsStylesSelection(int $no_tabs = 0)
    {
        $bg_styles = [
            'Taustata (ainult tekst)', 'Ovaal raamita', 'Ovaal, otstes raamjoon',
            '"Tablett" raamita', '"Tablett", otstes raamjoon'
        ];
        $cur_style = rbs_get_setting('header.style');

        $to_print[] = [
            'no_tabs'   => $no_tabs,
            'string'    => '<select class="form-select" id="rbs_settings_header_brand_style" name="rbs_settings_header_brand_style">',
            'replaces'  => []
        ];

        for ($i = 0; $i < count($bg_styles); $i++)
            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option value="##VALUE##"##SELECTED##>##OPTION##</option>',
                'replaces'  => [
                    '##VALUE##'     => $i,
                    '##SELECTED##'  => ($i == $cur_style ? ' selected' : ''),
                    '##OPTION##'    => $bg_styles[ $i ]
                ]
            ];

        $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</select>', 'replaces' => []];
        rbs_print_data($to_print);
    }

    /**
     * Prints out header background colors selection
     *
     * @param   int    [$no_tabs]
     */
    public static function settingsColorsSelection(int $no_tabs = 0)
    {
        $bg_colors = [
            'primary'   => 'Tumesinine',
            'info'      => 'Helesinine',
            'secondary' => 'Hall',
            'success'   => 'Roheline',
            'warning'   => 'Kollane',
            'danger'    => 'Punane'
        ];
        $cur_color = rbs_get_setting('header.bg_color');

        $to_print[] = [
            'no_tabs'   => $no_tabs,
            'string'    => '<select class="form-select" id="rbs_settings_header_brand_color" name="rbs_settings_header_brand_color">',
            'replaces'  => []
        ];

        foreach ($bg_colors as $_color => $_option)
            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<option value="##VALUE##"##SELECTED##>##OPTION##</option>',
                'replaces'  => [
                    '##VALUE##'     => $_color,
                    '##SELECTED##'  => ($_color == $cur_color ? ' selected' : ''),
                    '##OPTION##'    => $_option
                ]
            ];

        $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</select>', 'replaces' => []];
        rbs_print_data($to_print);
    }

    /**
     * Prints out page users data table
     *
     * @param   int    [$no_tabs]
     * @param   array  [$users]
     * 
     * @return  array
     */
    public static function usersTable(int $no_tabs = 0, array $users = []): array
    {
        $user_id = rbs_get_setting('user')['id'];
        $_table_show_ids = rbs_get_setting('table_show_ids');
        $ids = [];
        $to_print = [];

        foreach ($users as $_user) {
            $ids[] = $_user->id;

            // Disable editing of first two users in demo mode
            $_demo_disable = rbs_is_demo_disabled_user($_user->id);

            // Avoid deletion of first admin and user itself
            $_cant_delete = $_user->id == 1 || $_user->id == $user_id;

            // Avoid to change password of other admin, but allow to change own password
            $_cant_change_pwd = ($_user->role_id == 1) && $_user->id != $user_id;

            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '<tr>', 'replaces' => []];

            $_table_show_ids && $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##ID##</td>',
                'replaces' => ['##ID##' => $_user->id]
            ];
            $to_print[] = [
                'no_tabs'   => $no_tabs + 1,
                'string'    => '<td><span class="small badge ##BADGE## rounded-pill user-select-none">##ROLE##</span></td>',
                'replaces'  => [
                    '##BADGE##' => ($_user->role_id == 1 ? 'bg-danger' : 'bg-info'),
                    '##ROLE##'  => $_user->role_name
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1,'string' => '<td>##FIRST_NAME##</td>',
                'replaces' => ['##FIRST_NAME##' => $_user->first_name]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1,'string' => '<td>##LAST_NAME##</td>',
                'replaces' => ['##LAST_NAME##' => $_user->last_name]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td><i id="rbs_username_##ID##">##USERNAME##</i></td>',
                'replaces' => [
                    '##ID##'       => $_user->id,
                    '##USERNAME##' => $_user->username
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##EMAIL##</td>',
                'replaces' => ['##EMAIL##' => $_user->email]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 1, 'string' => '<td>##LAST_LOGIN##</td>',
                'replaces' => ['##LAST_LOGIN##' => $_user->last_login]
            ];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '<td>', 'replaces' => []];

            /* $to_print[] = [
                'no_tabs'  => $no_tabs + 2,
                'string'   => '<button type="button" id="rbs_user_save_change_##ID##" class="btn btn-sm btn-primary invisible">##SAVE_ICON##</button>',
                'replaces' => [
                    '##ID##'        => $_user->id,
                    '##SAVE_ICON##' => rbs_get_icon(['name' => 'save'])
                ]
            ]; */
            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '<div class="btn-group btn-group-sm">', 'replaces' => []];

            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button" id="rbs_user_edit_##ID##" class="btn btn-primary"##DISABLED##>##EDIT_ICON##</button>',
                'replaces' => [
                    '##ID##'        => $_user->id,
                    '##DISABLED##' => ($_demo_disable ? ' disabled' : ''),
                    '##EDIT_ICON##' => rbs_get_icon(['name' => 'pencil-square'])
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button"##ID## class="btn btn-##OPACITY##"##DISABLED##>##PWD_ICON##</button>',
                'replaces' => [
                    '##ID##'       => ($_cant_change_pwd ? '' : " id=\"rbs_user_password_{$_user->id}\""),
                    '##OPACITY##'  => ($_cant_change_pwd ? 'outline-secondary opacity-25' : 'secondary'),
                    '##DISABLED##' => ($_cant_change_pwd || $_demo_disable ? ' disabled' : ''),
                    '##PWD_ICON##' => rbs_get_icon(['name' => 'lock'])
                ]
            ];
            $to_print[] = [
                'no_tabs'  => $no_tabs + 3,
                'string'   => '<button type="button"##ID## class="btn btn-##OPACITY##"##DISABLED##>##DEL_ICON##</button>',
                'replaces' => [
                    '##ID##'       => ($_cant_delete ? '' : " id=\"rbs_user_delete_{$_user->id}\""),
                    '##OPACITY##'  => ($_cant_delete ? 'outline-danger opacity-25' : 'danger'),
                    '##DISABLED##' => ($_cant_delete || $_demo_disable ? ' disabled' : ''),
                    '##DEL_ICON##' => rbs_get_icon(['name' => 'trash'])
                ]
            ];

            $to_print[] = ['no_tabs' => $no_tabs + 2, 'string' => '</div>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs + 1, 'string' => '</td>', 'replaces' => []];
            $to_print[] = ['no_tabs' => $no_tabs, 'string' => '</tr>', 'replaces' => []];
        }
        rbs_print_data($to_print);
        return $ids;
    }

}