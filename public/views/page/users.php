<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

$_table_show_ids = rbs_get_setting('table_show_ids');

?>
    <div class="card mt-5">
        <div class="card-header d-flex justify-content-between">
            <h3 class="d-inline-block user-select-none text-muted">
                <?= rbs_get_icon(['name' => 'people', 'size' => 32]) ?>

                &nbsp;Saidi kasutajad
            </h3>

            <button type="button" class="btn btn-outline-primary" id="add_user_modal">
                <?= rbs_get_icon(['name' => 'person-plus', 'size' => 20]) ?>

                &nbsp; Lisa kasutaja
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped mt-3">
                    <thead class="table-light">
                        <tr><?php
if ($_table_show_ids):
?>

                            <th>ID</th><?php
endif;
?>

                            <th>Roll</th>
                            <th>Eesnimi</th>
                            <th>Perenimi</th>
                            <th>Kasutajanimi</th>
                            <th>E-mail</th>
                            <th>Viimane sisselogimine</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody id="user_table_body"><?php

$_ids = rbs_print_p_users_table(6, $rbs_users);
?>

                        <input type="hidden" id="rbs_user_ids" value="<?= implode(',', $_ids) ?>" readonly>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
