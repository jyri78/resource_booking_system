<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$_is_admin = rbs_is_admin();
$_table_show_ids = rbs_get_setting('table_show_ids');
$_device_filters = rbs_get_device_filters();


?>
    <div class="card mt-5">
        <div class="card-header d-flex justify-content-between">
            <h3 class="d-inline-block user-select-none text-muted">
                <?= rbs_get_icon(['name' => 'pc-display-horizontal', 'size' => 32]) ?>

                &nbsp;Seadmed &nbsp;<small>(komplektid)</small>
            </h3><?php
if ($_is_admin):
?>


            <button type="button" class="btn btn-outline-primary" id="add_device_modal">
                <?= rbs_get_icon(['name' => 'plus-square', 'size' => 20]) ?>

                &nbsp; Lisa seade
            </button><?php
else:
?>


            <button type="button" class="btn btn-outline-secondary" onclick="history.back()">
                <?= rbs_get_icon(['name' => 'arrow-left-square', 'size' => 20]) ?>

                &nbsp; Mine tagasi
            </button><?php
endif;
?>

        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table id="rbs_devices_table" class="table table-sm table-striped mt-3">
                    <thead class="table-light">
                        <tr><?php

if ($_is_admin && $_table_show_ids):
?>

                            <th>ID</th><?php
endif;
?>

                            <th>&nbsp;</th>
                            <th>Nimetus</th>
                            <th><?php

$_cnt = rbs_print_p_device_types(
    'rbs_filter_device_type', 'Seadme tüüp', 8, $_device_filters['type_id']
);

$cnt_disabled = $_cnt;
?>

                            </th>
                            <th><?php

$_cnt = rbs_print_p_device_manufacturers(
    'rbs_filter_device_manufacturer', 'Tootja', 8, $_device_filters['manufacturer_id']
);

$cnt_disabled = $_cnt;
?>

                            </th>
                            <th><?php

$_cnt = rbs_print_p_device_models(
    'rbs_filter_device_model', 'Mudel', 8, $_device_filters['model_id']
);

$cnt_disabled = $_cnt;
?>

                            </th>
                            <th><?php

$_cnt = rbs_print_p_device_locations(
    'rbs_filter_device_room', 'Asukoht', 8, $_device_filters['room_id']
);

$cnt_disabled = $_cnt;
?>

                            </th>
                            <th>Kogus</th>
                            <th>Lisainfo</th><?php
if ($_is_admin):
?>

                        <th class="text-end">
                            <button type="button" id="rbs_filter_device_reset" class="btn btn-sm btn-outline-secondary" value="1"
                                data-bs-toggle="tooltip" data-bs-placement="top" title="Lähtesta filtrid"><?=
                                    rbs_get_icon(['name' => 'funnel']) ?></button>
                        </th><?php
endif;
?>

                        </tr>
                    </thead>

                    <tbody id="device_table_body"><?php

$_ids = rbs_print_p_devices_table(6);
?>

                        <input type="hidden" id="rbs_device_ids" value="<?= implode(',', $_ids) ?>" readonly>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
