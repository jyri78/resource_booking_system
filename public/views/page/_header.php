<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$page = rbs_get_setting('page');
$user = rbs_get_setting('user');

//TODO: add to the settings
$_logo_style = (true ? 'circle border-5 px-5' : 'pill border-3 px-4');
$_is_logged_in = rbs_is_logged_in();
$_is_user_ok = rbs_is_user_ok();


?>
<nav class="navbar navbar-<?= $page['navbar'] ?> bg-<?= $page['nav_bg'] ?> sticky-top shadow-sm">
    <div class="container-fluid"><?php

if (!$_is_user_ok):
?>

        <span class="px-5"></span><?php
else:
?>

        <button type="button" class="navbar-toggler" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
            <span class="navbar-toggler-icon"></span>
            Menüü
        </button><?php
endif;
?>


        <a class="navbar-brand fs-5 <?=
                rbs__get_header_brand_style() ?>" href="<?=
                RBS_WEB_URL .'">'. rbs_get_setting('header.institution_name') ?></a>

        <span class="px-5"></span><span class="px-5"><?= (rbs_get_setting('demo_mode') ?
                '<span class="badge rounded-pill bg-warning border border-2 border-danger text-danger px-3 py-2 bg-opacity-10 opacity-25 fs-6 fw-bold">DEMO</span>' :
                '')
            ?></span><span class="px-5"></span><span class="px-5"></span>

        <div class="d-flex">
            <div class="navbar-text">
                <div class="nav-item">
                    <a id="rbs_<?= ($_is_user_ok ? 'help" data-rbs-page="'. rbs_get_setting('page.name') : 'about')
                    ?>" class="nav-link text-primary btn-outline-info opacity-75 rounded-pill p-0" role="button">
                        <?= rbs_get_icon([
                                'name' => ($_is_user_ok ? 'question-circle' : 'info-circle'),
                                'size' => 32
                            ]) ?>

                    </a>
                </div>
            </div>

            <div class="navbar-text px-2 py-0"><?php
if (!$_is_logged_in):
?>
</div>

            <div class="navbar-text">
                <ul class="navbar-nav">
                    <li type="button" id="person_login" class="navbar-item">
                        <?= rbs_get_icon(['name' => 'person-circle', 'size' => 32]) ?>

                        Logi sisse &nbsp;
                    </li>
                </ul>
            </div><?php
else:
?>

                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted" data-bs-toggle="dropdown" href="#">
                        <?= rbs_get_icon(['name' => 'person-circle', 'size' => 32])
                                . " {$user['firstname']} {$user['lastname']}" ?>

                    </a>

                    <ul class="dropdown-menu dropdown-menu-end">
                        <li>
                            <button type="button" id="person_change_password" class="dropdown-item"<?=
                                    (rbs_is_demo_disabled_user() ? ' disabled' : '') ?>>
                                <?= rbs_get_icon(['name' => 'lock', 'size'=> 20]) ?>

                                &nbsp; Muuda parool
                            </button>
                        </li>

                        <li>
                            <button type="button" id="rbs_about" class="dropdown-item">
                                <?= rbs_get_icon(['name' => 'info-circle', 'size'=> 18]) ?>

                                &nbsp; Teave
                            </button>
                        </li>

                        <li><hr class="dropdown-divider"></li>

                        <li>
                            <button type="button" id="person_logout" class="dropdown-item">
                                <?= rbs_get_icon(['name' => 'power', 'size' => 20]) ?>

                                &nbsp; Logi välja
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="offcanvasNavbar" class="offcanvas offcanvas-start bg-<?= $page['nav_bg'] ?>" tabindex="-1">
            <div class="offcanvas-header shadow-sm">
                <h5 class="offcanvas-title">Menüü</h5>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>

            <div class="offcanvas-body">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="<?= ($page['name'] == 'home' ? '#' : RBS_WEB_URL) ?>" class="nav-link<?=
                                ($page['name'] == 'home' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                            &nbsp;<?= rbs_get_icon(['name' => 'house-door']) ?>

                            &nbsp; Avaleht
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="<?= ($page['name'] == 'rooms' ? '#' : RBS_WEB_URL .'?ruumid') ?>" class="nav-link<?=
                                ($page['name'] == 'rooms' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                            &nbsp;<?= rbs_get_icon(['name' => 'door-open']) ?>

                            &nbsp; Ruumide info
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="<?= ($page['name'] == 'devices' ? '#' : RBS_WEB_URL .'?seadmed') ?>" class="nav-link<?=
                                ($page['name'] == 'devices' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                            &nbsp;<?= rbs_get_icon(['name' => 'pc-display-horizontal']) ?>

                            &nbsp; Seadmete info
                        </a><?php
if (rbs_is_admin()):
?>


                        <ul class="navbar-nav border-start border-2 small">
                            <li class="nav-item">
                                <a href="<?= ($page['name'] == 'device_types' ? '#' : RBS_WEB_URL .'?seadmete_tyybid') ?>" class="nav-link<?=
                                        ($page['name'] == 'device_types' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                                    &nbsp; &nbsp;

                                    <?= rbs_get_icon(['name' => 'usb-plug', 'size' => 13]) ?>

                                    &nbsp; Seadmete tüübid
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= ($page['name'] == 'manufacturers' ? '#' : RBS_WEB_URL .'?tootjad') ?>" class="nav-link<?=
                                        ($page['name'] == 'manufacturers' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                                    &nbsp; &nbsp;

                                    <?= rbs_get_icon(['name' => 'box2', 'size' => 13]) ?>

                                    &nbsp; Tootjad
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="<?= ($page['name'] == 'models' ? '#' : RBS_WEB_URL .'?mudelid') ?>" class="nav-link<?=
                                        ($page['name'] == 'models' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                                    &nbsp; &nbsp;

                                    <?= rbs_get_icon(['name' => 'tag', 'size' => 13]) ?>

                                    &nbsp; Mudelid
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item"><hr class="bg-danger bg-opacity-50"></li>

                    <li class="nav-item">
                        <a href="<?= ($page['name'] == 'admin' ? '#' : RBS_WEB_URL .'?saidi_seaded') ?>" class="nav-link<?=
                                ($page['name'] == 'admin' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                            &nbsp;<?= rbs_get_icon(['name' => 'gear']) ?>

                            &nbsp; Saidi seaded
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= ($page['name'] == 'users' ? '#' : RBS_WEB_URL .'?kasutajad') ?>" class="nav-link<?=
                                ($page['name'] == 'users' ? ' pe-none shadow-sm rounded bg-info bg-opacity-10' : '') ?>">
                            &nbsp;<?= rbs_get_icon(['name' => 'people']) ?>

                            &nbsp; Kasutajad
                        </a><?php
endif;
?>

                    </li>
                </ul>
            </div>
        </div><?php
endif;
?>

    </div>
</nav>
