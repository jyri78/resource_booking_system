<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$_is_admin = rbs_is_admin();
$_table_show_ids = rbs_get_setting('table_show_ids');

?>
    <div class="card mt-5">
        <div class="card-header d-flex justify-content-between">
            <h3 class="d-inline-block user-select-none text-muted">
                <?= rbs_get_icon(['name' => 'door-open', 'size' => 32]) ?>

                &nbsp;Ruumid
            </h3><?php
if ($_is_admin):
?>


            <button type="button" class="btn btn-outline-primary" id="add_room_modal">
                <?= rbs_get_icon(['name' => 'plus-square', 'size' => 20]) ?>

                &nbsp; Lisa ruum
            </button><?php
else:
?>


            <button type="button" class="btn btn-outline-secondary" onclick="history.back()">
                <?= rbs_get_icon(['name' => 'arrow-left-square', 'size' => 20]) ?>

                &nbsp; Mine tagasi
            </button><?php
endif;
?>

        </div>

        <div class="card-body">
            <table class="table mt-3">
                <thead class="table-light">
                    <tr><?php

if ($_is_admin && $_table_show_ids):
?>

                        <th>ID</th><?php
endif;
?>

                        <th class="w-25">Ruum</th>
                        <th>Lisainfo</th><?php
if ($_is_admin):
?>

                        <th>Mitte broneeritav</th>
                        <th>Viimati muudetud</th>
                        <th>&nbsp;</th><?php
endif;
?>

                    </tr>
                </thead>

                <tbody id="rooms_table_body"><?php
$_ids = [];

foreach ($rbs_rooms as $room):
    if ($room->not_for_booking == '1' && !$_is_admin) continue;  // hide for regular user
    $_ids[] = $room->id;
    $_row_bg = ($room->not_for_booking == '1' ? ' class="bg-warning bg-opacity-10"' : '');
?>

                    <tr id="rbs_room_<?= $room->id ?>"<?= $_row_bg ?>><?php

    if ($_is_admin && $_table_show_ids):
?>

                        <td><?= $room->id ?></td><?php
    endif;
?>

                        <td class="w-25">
                            <input type="text" id="rbs_room_name_<?= $room->id
                                ?>" class="form-control form-control-sm" value="<?=
                                    $room->object_name ?>" disabled readonly>
                        </td>
                        <td>
                            <textarea type="textarea" id="rbs_room_description_<?= $room->id
                                ?>" class="form-control form-control-sm" rows="5" disabled readonly><?=
                                    $room->info ?></textarea>
                        </td><?php
    if ($_is_admin):
?>

                        <td class="align-middle text-center">
                            <div class="form-check form-switch d-inline-block">
                                <input type="checkbox" id="rbs_room_not_for_booking_<?=
                                        $room->id ?>" class="form-check-input"<?=
                                        ($room->not_for_booking == '1' ? ' checked' : '') ?> disabled readonly>
                                <label for="rbs_room_not_for_booking_<?= $room->id ?>"></label>
                            </div>
                        </td>
                        <td>
                            <div class="mt-3"><?= $room->updated ?></div>
                        </td>
                        <td class="align-middle text-center">
                            <button type="button" id="rbs_room_save_change_<?= $room->id
                                ?>" class="btn btn-sm btn-primary invisible"><?=
                                    rbs_get_icon(['name' => 'save']) ?></button>
                            <div class="btn-group btn-group-sm">
                                <button type="button" id="rbs_room_edit_<?= $room->id
                                    ?>" class="btn btn-secondary"><?=
                                        rbs_get_icon(['name' => 'pencil-square']) ?></button>
                                <button type="button" <?=
                                        ($room->id == 1 ? 'disabled' : "id=\"rbs_room_delete_{$room->id}\"")
                                    ?> class="btn btn-<?= ($room->id == 1 ? 'outline-danger opacity-25' : 'danger') ?>"><?=
                                        rbs_get_icon(['name' => 'trash']) ?></button>
                            </div>
                        </td><?php
    endif;
?>

                    </trclass=><?php
endforeach;

if ($_is_admin):
?>

                    <input type="hidden" id="rbs_room_ids" value="<?= implode(',', $_ids) ?>"><?php
endif;    
?>

                </tbody>
            </table>
        </div>
    </div>
