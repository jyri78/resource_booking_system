<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

$_table_show_ids = rbs_get_setting('table_show_ids');

?>
    <div class="card mt-5">
        <div class="card-header d-flex justify-content-between">
            <h3 class="d-inline-block user-select-none text-muted">
                <?= rbs_get_icon(['name' => 'tag', 'size' => 32]) ?>

                &nbsp;Mudelid
            </h3>

            <button type="button" class="btn btn-outline-primary" id="add_model_modal">
                <?= rbs_get_icon(['name' => 'plus-square', 'size' => 20]) ?>

                &nbsp; Lisa mudel
            </button>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-striped mt-3">
                    <thead class="table-light">
                        <tr><?php
if ($_table_show_ids):
?>

                            <th>ID</th><?php
endif;
?>

                            <th>Mudeli nimi</th>
                            <th>Seotud seadmekompl. arv</th>
                            <!-- <th>Broneeringute arv</th> -->
                            <th>Viimati muudetud</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody id="models_table_body"><?php

$_ids = rbs_print_p_models_table(6);
?>

                        <input type="hidden" id="rbs_mmodel_ids" value="<?= implode(',', $_ids) ?>" readonly>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
