<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$_navbar = rbs_get_setting('page.navbar', 'light');
$_table_show_ids = rbs_get_setting('table_show_ids', false);
$_institution_name = rbs_get_setting('header.institution_name');
$_cal_opt = rbs_get_setting('calendar_options');
$_booking_opt = rbs_get_setting('booking_options');
?>
    <h1>Saidi seaded</h1>

    <div class="row row-cols-1 row-cols-xxl-2 mt-5">
        <div class="col mb-3">
            <div class="card border-primary">
                <h5 class="card-header user-select-none">
                    <?= rbs_get_icon(['name' => 'calendar-check', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                    &nbsp;Üldised kalendriseaded
                </h5>

                <div class="card-body">
                    <form id="rbs_admin_settings_calendar_options">
                        <div class="row row-cols-1 mb-3">
                            <div class="col">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="rbs_weekends" name="rbs_weekends" value="1"<?=
                                            ($_cal_opt['weekends'] ? ' checked' : '') ?>>
                                    <label class="form-check-label" for="rbs_weekends">&nbsp;näita kalendris ka nädalavahetusi</label>
                                </div>
                            </div>
                        </div>

                        <div class="row border-top border-bottom pt-3 mb-3">
                            <div class="col-12 col-lg-6 mb-2 fw-bold user-select-none">
                                Kalendri päeva algus- ja lõpuaeg
                            </div>

                            <div class="col-6 col-lg-3 mb-3">
                                <div class="input-group input-group-sm">
                                    <label class="input-group-text" for="rbs_slot_min_time">Algus</label>
                                    <input type="time" class="form-control" id="rbs_slot_min_time" name="rbs_slot_min_time" value="<?=
                                            $_cal_opt['slotMinTime'] ?>" step="1800">
                                </div>
                            </div>

                            <div class="col-6 col-lg-3 mb-3">
                                <div class="input-group input-group-sm">
                                    <label class="input-group-text" for="rbs_slot_max_time">Lõpp</label>
                                    <input type="time" class="form-control" id="rbs_slot_max_time" name="rbs_slot_max_time" value="<?=
                                            $_cal_opt['slotMaxTime'] ?>" step="1800">
                                </div>
                            </div>
                        </div>

                        <div class="row border-top border-bottom pt-3">
                            <div class="col-12 col-lg-6 mb-2 fw-bold user-select-none">
                                Lahtioleku algus- ja lõpuaeg
                                <small>
                                    <span class="small badge rounded-pill bg-warning" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true"
                                        title="<span class='small badge rounded-pill bg-danger fw-bold'>NB!</span> Lahtioleku aeg ei saa olla väiksem kalendri päeva algusajast ja suurem kalendri päeva lõpuajast.<br>Tundide ajad peavad jääma lahtioleku aja sisse">i</span>
                                </small>
                            </div>

                            <div class="col-6 col-lg-3 mb-3">
                                <div class="input-group input-group-sm">
                                    <label class="input-group-text" for="rbs_bh_start_time">Algus</label>
                                    <input type="time" class="form-control" id="rbs_bh_start_time" name="rbs_bh_start_time" value="<?=
                                            $_cal_opt['bhStartTime'] ?>" step="1800">
                                </div>
                            </div>

                            <div class="col-6 col-lg-3 mb-3">
                                <div class="input-group input-group-sm">
                                    <label class="input-group-text" for="rbs_bh_end_time">Lõpp</label>
                                    <input type="time" class="form-control" id="rbs_bh_end_time" name="rbs_bh_end_time" value="<?=
                                            $_cal_opt['bhEndTime'] ?>" step="1800">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <input type="button" class="btn btn-primary" id="rbs_settings_calendar_options_save" value="Salvesta">
                </div>
            </div>

            <div class="card border-success mt-4">
                <h5 class="card-header user-select-none">
                    <?= rbs_get_icon(['name' => 'calendar-week', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                    &nbsp;Broneeringu seaded
                </h5>

                <div class="card-body">
                    <form id="rbs_admin_settings_booking_options">
                        <div class="row">
                            <div class="col-8 fw-bold user-select-none">
                                Korduva broneeringu lubatud aeg
                                <small>
                                    <span class="small badge rounded-pill bg-info" data-bs-toggle="tooltip" data-bs-placement="top"
                                        title="Mitu nädalat saab tavakasutaja/õpetaja ruumi/seadet korduval broneerimisel ette broneerida.">i</span>
                                </small>
                            </div>

                            <div class="col-4">
                                <div class="input-group input-group-sm">
                                    <input type="number" class="form-control" id="rbs_allow_repeat" name="rbs_allow_repeat" value="<?=
                                            ($_booking_opt['allow_repeat'] ?? 4) ?>" min="2" max="20" size="1" step="1">
                                    <label class="input-group-text" for="rbs_allow_repeat">nädalat</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <input type="button" class="btn btn-primary" id="rbs_settings_booking_options_save" value="Salvesta">
                </div>
            </div>
        </div>

        <div class="col mb-3">
            <div class="card border-warning">
                <h5 class="card-header user-select-none">
                    <?= rbs_get_icon(['name' => 'clock-history', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                    &nbsp;Tundide ajad
                </h5>

                <div class="card-body">
                    <form id="rbs_admin_settings_lesson_times">
                        <table class="table my-0 text-center">
                            <tbody><?php
rbs_print_p_settings_lesson_times(8);
?>

                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <input type="button" class="btn btn-primary" id="rbs_settings_lesson_times_save" value="Salvesta">
                </div>
            </div>
        </div>

        <div class="col mt-2 mb-3">
            <div class="card border-danger">
                <h5 class="card-header user-select-none">
                    <?= rbs_get_icon(['name' => 'card-heading', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                    &nbsp;Päise brändi seaded
                </h5>

                <div class="card-body">
                    <form id="rbs_admin_settings_header">
                        <div class="row row-cols-1 mb-1">
                            <div class="col mb-2">
                                <div class="input-group input-group-sm">
                                    <label class="input-group-text fw-bold" for="rbs_settings_header_brand_institution_name">Asutuse nimi</label>
                                    <input type="text" class="form-control" id="rbs_settings_header_brand_institution_name" name="rbs_settings_header_brand_institution_name" value="<?=
                                            $_institution_name ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row row-cols-2">
                            <div class="col border-end">
                                <div class="input-group input-group-sm mb-2">
                                    <label class="input-group-text fw-bold" for="rbs_settings_header_brand_style">Taust</label><?php
rbs_print_p_settings_styles_selection(9);
?>

                                </div>

                                <div class="input-group input-group-sm">
                                    <label class="input-group-text fw-bold" for="rbs_settings_header_brand_color">Värv</label><?php
rbs_print_p_settings_colors_selection(9);
?>

                                </div>
                            </div>

                            <div class="col text-center py-3">
                                <small class="<?= rbs__get_header_brand_style()
                                    ?>" id="rbs_settings_header_brand_preview"><?= $_institution_name ?></small>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-footer d-flex justify-content-end">
                    <input type="button" class="btn btn-primary" id="rbs_settings_header_save" value="Salvesta">
                </div>
            </div>
        </div>

        <div class="col mt-2 mb-3">
            <div class="card">
                <h5 class="card-header user-select-none">
                    <?= rbs_get_icon(['name' => 'sliders', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                    &nbsp;Saidi seaded
                </h5>

                <div class="card-body">
                    <form id="rbs_admin_settings_site">
                        <input type="hidden" id="rbs_view_navbar_current" value="<?= $_navbar ?>">
                        <input type="hidden" id="rbs_table_show_ids_current" value="<?=
                                ($_table_show_ids ? '1' : '0') ?>">

                        <div class="row border-bottom mb-3">
                            <div class="col-12 col-sm-6 mb-2 fw-bold user-select-none">
                                Menüürea teema
                            </div>

                            <div class="col-6 col-sm-3 mb-3">
                                <div class="form-check">
                                    <input type="radio" role="button" class="form-check-input" id="rbs_view_navbar_light" name="rbs_view_navbar" value="light"<?=
                                            ($_navbar == 'light' ? ' checked' : '') ?>>
                                    <label class="form-check-label" role="button" for="rbs_view_navbar_light">&nbsp;hele</label>
                                </div>
                            </div>

                            <div class="col-6 col-sm-3 mb-3">
                                <div class="form-check">
                                    <input type="radio" role="button" class="form-check-input" id="rbs_view_navbar_dark" name="rbs_view_navbar" value="dark"<?=
                                            ($_navbar == 'dark' ? ' checked' : '') ?>>
                                    <label class="form-check-label" role="button" for="rbs_view_navbar_dark">&nbsp;tume</label>
                                </div>
                            </div>
                        </div>

                        <div class="row border-bottom mb-3">
                            <div class="col-12 mb-3">
                                <div class="form-check">
                                    <input type="checkbox" role="button" class="form-check-input" id="rbs_table_show_ids" name="rbs_table_show_ids"<?=
                                            ($_table_show_ids ? ' checked' : '') ?>>
                                    <label class="form-check-label" role="button" for="rbs_table_show_ids">Kuva tabelites ID-d <span class="text-muted">(v.a tavakasutajale)</span></label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6 my-2 fw-bold user-select-none">
                                JavaScripti POST-päringu võti
                            </div>

                            <div class="col-12 col-md-6">
                                <input type="text" class="form-control" value="<?=
                                        rbs_read_setting('view_post_key', RBS_DEFAULT_POST_KEY) ?>" disabled>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- <div class="card-footer d-flex justify-content-end">
                    <input type="button" class="btn btn-primary opacity-25" id="rbs_settings_site_save" value="Salvesta" disabled>
                </div> -->
            </div>
        </div>
    </div>
