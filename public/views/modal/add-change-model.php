<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$model = rbs_m_get_model();

if (!$model['id']) {
    $_icon = 'plus-square';
    $_title = 'Uue mudeli lisamine';
}
else {
    $_icon = 'check2-square';
    $_title = 'Muuda mudelit';
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $_icon, 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="model_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly><?php

if ($model['id']):
?>

                <input type="hidden" id="rbs_model_id" name="rbs_model_id" value="<?=
                        $model['id'] ?>" readonly><?php
endif;
?>

                <input type="text" class="d-none"><!-- hack to prevent browser submitting -->
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_model_name" name="rbs_model_name" value="<?=
                            $model['name'] ?>" placeholder="Toote mudel">
                    <label id="rbs_model_name_lbl" for="rbs_model_name" data-rbs-label="Toote mudel"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveModel()">Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>