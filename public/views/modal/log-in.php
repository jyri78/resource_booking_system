<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

?>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'person-circle', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 32]) ?>

                &nbsp;Logi sisse
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="login_form">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_user" name="rbs_user" placeholder="kasutajanimi/nimi@mail.ee">
                    <label id="rbs_user_lbl" for="rbs_user" data-rbs-label="Kasutajanimi või e-mail"></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="rbs_password" name="rbs_password" placeholder="Parool">
                    <label id="rbs_password_lbl" for="rbs_password" data-rbs-label="Parool"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="logIn()">Logi sisse</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>