<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'filter', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Filtreeri kalender
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="calendar_filter_form">
                <div class="form-floating mb-3">
                    <select class="form-select" id="rbs_resource_type" name="rbs_resource_type"><?php
$obj_types = rbs_print_m_calendar_filter_object_types(6);
?>

                    </select>
                    <label id="rbs_resource_type_lbl" for="rbs_resource_type" data-rbs-label="Ressursi tüüp"></label>
                </div>
                <div class="form-floating mb-3">
                    <select class="form-select" id="rbs_resource" name="rbs_resource"><?php
$objects = rbs_print_m_calendar_filter_objects(6);

$_SESSION['CALENDAR_FILTER_OBJECT_TYPES_ARRAY'] = $obj_types;
$_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY'] = $objects;
?>

                    </select>
                    <label id="rbs_resource_lbl" for="rbs_resource" data-rbs-label="Ressurss"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" id="rbs_filter_button" class="btn btn-primary" onclick="calendarFilter()">Filtreeri</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>