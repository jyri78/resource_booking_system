<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

$image = rbs_m_get_image();
$_title = "{$image['device_name']} &nbsp;<small class=\"text-muted\">({$image['image_filename']})</small>";


?>
<div class="modal-dialog modal-xl modal-fullscreen-lg-down">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'image', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <img class="img-fluid rounded mx-auto d-block" src="<?= RBS_WEB_URL . "?img_{$image['image_id']}" ?>" />
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>