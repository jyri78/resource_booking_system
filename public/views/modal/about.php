<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

?>
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'info-circle', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Rakendusest
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="card">
                <div class="card-header user-select-none d-flex justify-content-between">
                    <b>Ressursside broneerimise süsteem</b>
                    <small class="small text-muted fw-normal"><?= RBS_APP_VERSION ?></small>
                </div>

                <div class="card-body">
                    <p class="lh-base">
                        Tallinna Ülikooli Haapsalu kolledži <i>rakendusinformaatika</i> tudengite poolt
                        <b>valikpraktika</b> kursuse raames alustatud projekt, mille arendust jätkas üks meeskonnaliikmetest,
                        <b><i>Jüri Kormik</i></b> diplomitöö käigus.
                    </p>
                    <p class="lh-base text-decoration-underline">
                        Valikpraktika meeskonnaliikmed:
                    </p>
                    <ul>
                        <li>
                            <b>Mikk Herde</b> &nbsp;–&nbsp; prototüüpimine, küsitlus, testimine
                        </li>
                        <li>
                            <b>Jüri Kormik</b> &nbsp;–&nbsp; programmeerija (esirakendus, tagarakenduse
                            <abbr class="initialism" title="Asynchronous JavaScript and XML ehk tehnoloogia esirakenduse ja tagarakenduse vaheliseks suhtluseks">AJAX</abbr>-osa)
                        </li>
                        <li>
                            <b>Andrus Kull</b> &nbsp;–&nbsp; programmeerija (tagarakendus, andmebaas)
                        </li>
                        <li>
                            <b>Karmo Lugima</b> &nbsp;–&nbsp; kasutajalood, esirakendus (peamiselt
                            <abbr class="initialism" title="HyperText Markup Language">HTML</abbr> ja <abbr class="initialism" title="Cascading Style Sheets">CSS</abbr>), testimine
                        </li>
                    </ul>
                    <p class="mt-4 lh-base">
                        Rakendus on kaitstud GNU General Public litsentsiga (<a href="https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE" target="_blank">GPLv3</a>).
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Olgu</button>
        </div>
    </div>
</div>