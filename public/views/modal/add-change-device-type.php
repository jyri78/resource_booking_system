<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$device_type = rbs_m_get_device_type();

if (!$device_type['id']) {
    $_icon = 'plus-square';
    $_title = 'Uue seadmetüübi lisamine';
}
else {
    $_icon = 'check2-square';
    $_title = 'Muuda seadme tüüpi';
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $_icon, 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="device_type_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly><?php

if ($device_type['id']):
?>

                <input type="hidden" id="rbs_device_type_id" name="rbs_device_type_id" value="<?=
                        $device_type['id'] ?>" readonly><?php
endif;
?>

                <input type="text" class="d-none"><!-- hack to prevent browser submitting -->
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_device_type_name" name="rbs_device_type_name" value="<?=
                            $device_type['name'] ?>" placeholder="Seadme tüüp">
                    <label id="rbs_device_type_name_lbl" for="rbs_device_type_name" data-rbs-label="Seadme tüüp"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveDeviceType()">Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>