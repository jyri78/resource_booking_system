<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'plus-square', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Uue ruumi lisamine
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="room_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly>
                <input type="text" class="d-none"><!-- hack to prevent browser submitting -->
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_room_name" name="rbs_room_name" placeholder="Ruum">
                    <label id="rbs_room_name_lbl" for="rbs_room_name" data-rbs-label="Ruum"></label>
                </div>
                <div class="form-floating mb-3">
                    <textarea class="form-control" id="rbs_room_description" name="rbs_room_description" placeholder="Kirjeldus" style="height:10em"></textarea>
                    <label id="rbs_room_description_lbl" for="rbs_room_description" data-rbs-label="Kirjeldus (salvestamiseks 'Ctrl+Enter')"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveNewRoom()">Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>