<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

$device_object = rbs_m_get_device_object();
$device_ids = rbs_m_get_device_ids();
$first_device = rbs_m_get_device_by_id($device_ids[0]);

$_cnt_devices = count($device_ids) - 1;


?> 
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div id="rbs_loader" class="position-absolute w-100 h-100 opacity-25 bg-secondary d-none" style="z-index:999">
            <div class="spinner-grow text-dark position-absolute" style="width:11rem; height:11rem; top:40%; left:35%" role="status">
                <span class="visually-hidden">Laadimine...</span>
            </div>
        </div>

        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'list-check', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Üksikseadmete haldus
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body">
            <form id="devices_edit_form">
                <input type="hidden" id="rbs_device_object_id" name="rbs_device_object_id" value="<?= $device_object['id'] ?>" readonly>
                <input type="hidden" id="rbs_device_id" name="rbs_device_id" value="<?= $device_ids[0] ?>" readonly>
                <input type="hidden" id="rbs_device_ids_list" value="<?= implode(',', $device_ids) ?>" readonly>

                <div class="row mb-3">
                    <div class="col-7">
                        <div class="input-group input-group-sm mb-2">
                            <span class="input-group-text fw-bold">Kompl. nimetus</span>
                            <input type="text" class="form-control form-control-sm" value="<?=
                                    $device_object['device_name'] ?>" disabled></input>
                        </div>

                        <div class="input-group input-group-sm mb-2">
                            <span class="input-group-text fw-bold">Seadme tüüp</span><?php

$_cnt = rbs_print_m_device_types(
    'rbs_device_type', '', RBS_SELECT_OTPION_TITLE['device_type'],
    8, $device_object['device_type_id']
);

$cnt_disabled = $_cnt;
?>

                        </div>

                        <div class="input-group input-group-sm mb-2">
                            <span class="input-group-text fw-bold">Seadme tootja</span><?php

$_cnt = rbs_print_m_device_manufacturers(
    'rbs_device_manufacturer', '', RBS_SELECT_OTPION_TITLE['manufacturer'],
    8, $device_object['device_manufacturer_id']
);

$cnt_disabled += $_cnt;
?>

                        </div>

                        <div class="input-group input-group-sm mb-2">
                            <span class="input-group-text fw-bold">Seadme mudel</span><?php

$_cnt = rbs_print_m_device_models(
    'rbs_device_model', '', RBS_SELECT_OTPION_TITLE['device_model'],
    8, $device_object['device_model_id']
);

$cnt_disabled += $_cnt;
?>

                        </div>

                        <div class="input-group input-group-sm mb-2">
                            <span class="input-group-text fw-bold">Seadme asukoht</span><?php

$_cnt = rbs_print_m_device_locations(
    'rbs_device_location', '', RBS_SELECT_OTPION_TITLE['device_location'],
    8, $device_object['room_id'], false, true
);

$cnt_disabled += $_cnt;
?>

                        </div>
                    </div><!-- <div class="col-6"> -->

                    <div class="col-5 text-center pb-2 my-auto">
                        <img class="img-thumbnail rounded-3" src="<?= RBS_WEB_URL ."?preview_{$device_object['id']}" ?>" />
                    </div>
                </div><!-- <div class="row mb-3"> -->

                <div class="row mb-3">
                    <div class="col-9">
                        <div class="row g-3">
                            <div class="col-12">
                                <div class="input-group">
                                    <span class="input-group-text">Nimetus / Number</span>
                                    <input type="text" id="rbs_name" name="rbs_name" class="form-control" value="<?=
                                            $first_device['name'] ?>" data-rbs-next-input="rbs_asset_code"></input>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="input-group">
                                    <span class="input-group-text">Varakood</span>
                                    <input type="text" id="rbs_asset_code" name="rbs_asset_code" class="form-control" value="<?=
                                            $first_device['asset_code'] ?>" data-rbs-next-input="rbs_serial_number" data-rbs-not-required="1"></input>
                                </div>
                            </div>
                        </div><!-- <div class="row g-3 mb-3"> -->
                    </div><!-- <div class="col-8"> -->

                    <div class="col-3">&nbsp;</div>
                </div><!-- <div class="row"> -->

                <div class="row mb-3">
                    <div class="col-9">
                        <div class="input-group">
                            <span class="input-group-text">Seerianumber</span>
                            <input type="text" id="rbs_serial_number" name="rbs_serial_number" class="form-control" value="<?=
                                    $first_device['serial_number'] ?>" data-rbs-next-input="rbs_info" data-rbs-not-required="1"></input>
                        </div>
                    </div>

                    <div class="col-3 my-2">
                        <div class="form-check text-danger fw-bold">
                            <input type="checkbox" id="rbs_is_broken" name="rbs_is_broken" class="form-check-input" value="1" tabindex="-1"<?=
                                    ($first_device['is_broken']) ? ' checked' : '' ?>>
                            <label class="form-check-label" for="rbs_is_broken">katki</label>
                        </div>
                    </div>
                </div>

                <div class="input-group">
                    <span class="input-group-text">Lisainfo</span>
                    <textarea id="rbs_info" name="rbs_info" class="form-control" value="<?=
                            $first_device['info'] ?>" data-rbs-not-required="1"></textarea>
                </div>
            </form>
        </div><!-- <div class="modal-body"> -->

        <div class="modal-footer">
            <button type="button" id="rbs_previous_button" class="btn btn-outline-success" onclick="prevDevice()">Eelmine</button>
            <small class="border py-1 px-2 rounded">
                <span id="rbs_active_device"><?= (!$_cnt_devices ? '–' : '1')
                    ?></span>&thinsp;/&thinsp;<b id="rbs_cnt_devices"><?= $_cnt_devices ?></b>
            </small>
            <button type="button" id="rbs_next_button" class="btn btn-outline-success" onclick="nextDevice()">Järgmine</button>

            <div class="vr mx-3"></div>
            <button type="button" id="rbs_save_button" class="btn btn-primary" onclick="saveDevice()">Salvesta</button>
            <button type="button" id="rbs_delete_button" class="btn btn-outline-danger" onclick="deleteDevice()"<?=
                    (!$_cnt_devices ? ' disabled' : '') ?>>Kustuta</button>
            <div class="vr mx-3"></div>

            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>