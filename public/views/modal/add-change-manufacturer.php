<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$manufacturer = rbs_m_get_manufacturer();

if (!$manufacturer['id']) {
    $_icon = 'plus-square';
    $_title = 'Uue tootja lisamine';
}
else {
    $_icon = 'check2-square';
    $_title = 'Muuda tootja nime';
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $_icon, 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="manufacturer_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly><?php

if ($manufacturer['id']):
?>

                <input type="hidden" id="rbs_manufacturer_id" name="rbs_manufacturer_id" value="<?=
                        $manufacturer['id'] ?>" readonly><?php
endif;
?>

                <input type="text" class="d-none"><!-- hack to prevent browser submitting -->
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_manufacturer_name" name="rbs_manufacturer_name" value="<?=
                            $manufacturer['name'] ?>" placeholder="Tootja nimi">
                    <label id="rbs_manufacturer_name_lbl" for="rbs_manufacturer_name" data-rbs-label="Tootja nimi"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveManufacturer()">Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>