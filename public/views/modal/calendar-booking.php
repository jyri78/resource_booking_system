<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$lesson_times = rbs_get_setting('lesson_times');
$m_is_admin = rbs_is_admin();
$m_allow_repeat_weeks = ($m_is_admin ? 42 : (rbs_get_setting('booking_options.allow_repeat', 4)));
$m_new_booking = !isset($_POST['booking_id']);
$m_sel_device = rbs_get_sel_obj_type_id() > 1;

$_evt = ($m_new_booking ? $_POST : rbs_get_booking_data($_POST['booking_id']) );
$_ad = $_evt['all_day'] == 'true';

$date = $_evt['date'];
$_booked_times = rbs_get_booked_times($date);
// $_day_full = count($booked_lessons) == count($lesson_times);
$booked_lessons = $_booked_times['booked_lessons'];

$booked_devices = (!count($_booked_times['booked_devices']) ? 0 :
        max(array_values($_booked_times['booked_devices'])));  // TODO: find better solution

$start = ($_ad ? '00:00' : $_evt['start']);
$end = ($_ad ? '23:59' : $_evt['end']);

if ($m_new_booking) {
    $m_strings = ['icon' => 'calendar-plus', 'title'=> 'Broneerimine', 'button' => 'Broneeri'];
    $m_sel_user = '';
    $m_day_empty = !count($booked_lessons);
    $m_dev_cnt = 0;
    $m_dev_all = true;

    $title = '';
    $recur_date = date('Y-m-d', strtotime(rbs_convert_date($date) .'+7 DAYS'));
    $repeat = false;
    $week_days = [date('w', strtotime(rbs_convert_date($date)))];
    $all_day = $_ad && $m_day_empty;
}
else {
    $_t = explode('%%%', $_evt['title']);
    $m_strings = ['icon' => 'calendar-event', 'title'=> 'Muuda broneering', 'button' => 'Muuda'];
    $m_sel_user = $_t[1] ?? '';
    $m_day_empty = !count($booked_lessons) || $_ad;  // if selected booking is all day, than allow All Day checkbox
    $m_dev_cnt = $_evt['devices_count'];
    $m_dev_all = $_evt['devices_all'];

    $title = $_t[0];
    $recur_date = $_evt['recur_date'];
    $repeat = $_evt['repeat'];
    $week_days = $_evt['week_days'];
    $all_day = $_ad;
}


?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $m_strings['icon'], 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $m_strings['title'] ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="calendar_booking_form"><?php

if (!$m_new_booking):
?>

                <input type="hidden" id="rbs_booking_id" name="rbs_booking_id" value="<?=
                        $_POST['booking_id'] ?>"><?php
endif;

if (!$m_new_booking || $m_sel_device):
?>

                <input type="hidden" id="rbs_resource" name="rbs_resource" data-rbs-add-listener="0" value="<?=
                        rbs_get_sel_obj_id() ?>"><?php
endif;
?>

                <div class="input-group input-group-sm mb-3 opacity-75">
                    <span class="input-group-text w-25 fw-bold">Valitud kuupäev</span>
                    <input type="text" class="form-control" id="rbs_booking_date" name="rbs_booking_date" value="<?=
                            $date ?>" readonly>
                </div>
                <div class="input-group input-group-sm mb-1 opacity-75">
                    <span class="input-group-text w-25 fw-bold">Valitud ressursi tüüp</span>
                    <input type="text" class="form-control" value="<?= rbs_get_sel_obj_type_name() ?>" readonly>
                </div>
                <div class="input-group input-group-sm pb-3 mb-<?=
                        ($m_sel_device ? '0' : '4 border-bottom border-3 border-info') ?>">
                    <span class="input-group-text w-25 fw-bold">Valitud ressurss</span>
                    <select class="form-select" <?= ($m_new_booking && !$m_sel_device ?  // TODO: maybe to change logic
                            'id="rbs_resource" name="rbs_resource" data-rbs-add-listener="1"' :
                            'disabled') ?>><?php

$objects = rbs_print_m_calendar_filter_objects(6);

$_SESSION['CALENDAR_FILTER_SEL_OBJECTS_ARRAY'] = $objects;
?>

                    </select>
                </div><?php

if ($m_sel_device):
?>

                <div class="input-group input-group-sm pb-3 mb-4 border-bottom border-3 border-info">
                    <span class="input-group-text w-25 fw-bold">Seadmete arv</span>
                    <select class="form-select" id="rbs_devices_count" name="rbs_devices_count"<?=
                            ($m_dev_all && !$booked_devices ? ' disabled' : '') ?>><?php

$_cnt_devices = rbs_print_m_calendar_devices_count(6, $booked_devices, $m_dev_cnt, $m_dev_all);

?>

                    </select>
                    <div class="input-group-text">
                        <input type="checkbox" id="rbs_all_devices" name="rbs_all_devices" class="form-check-input mt-0" value="<?=
                                $_cnt_devices ?>"<?= ($booked_devices ?
                                    ' disabled' : ($m_dev_all ? ' checked' : '')) ?>>
                        <label for="rbs_all_devices">&nbsp;&nbsp;kogu komplekt</label>
                    </div>
                </div><?php
endif;
?>

                <div class="row row-cols-1">
                    <div class="col mb-2 px-4 py-1 bg-light">
                        <div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" role="switch" id="rbs_booking_all_day" name="rbs_booking_all_day"<?=
                                    (!$m_day_empty ? ' disabled' : ($all_day ? ' checked' : '')) ?> value="1">
                            <label class="form-check-label" for="rbs_booking_all_day">Terve päev</label>
                        </div>
                    </div>
                </div>
                <div id="rbs_select_booking_lessons" class="row row-cols-<?= count($lesson_times)
                    ?> mb-2 mx-1 py-2<?= ($all_day ? ' d-none' : '') ?>"><?php
$no_tabs = 5;

rbs_print_m_calendar_booking_lessons(
    $no_tabs, $lesson_times, $start, $end, $m_new_booking, $booked_lessons
);
?>

                </div>
                <div class="row row-cols-2 mx-1 pt-2">
                    <div class="col px-4 py-3 bg-light bg-opacity-75 border-start border-top">
                        <div class="form-check form-switch mt-1">
                            <input class="form-check-input" type="checkbox" role="switch" id="rbs_booking_recur" name="rbs_booking_recur" value="1"<?=
                                    ($repeat ? ' checked' : '') ?>>
                            <label class="form-check-label" for="rbs_booking_recur">Korda kuni &hellip;</label>
                        </div>
                    </div>
                    <div class="col px-4 py-2 bg-light border-top border-end">
                        <div id="rbs_select_booking_recur_date"<?= ($repeat ? '' : 'class="d-none"') ?>>
                            <!-- <div class="form-floating"> -->
                                <input type="date" class="form-control" id="rbs_booking_recur_date" name="rbs_booking_recur_date" min="<?=
                                        date('Y-m-d', strtotime(rbs_convert_date($date) .'+1 DAY')) ?>" max="<?=
                                        date('Y-m-d', strtotime(rbs_convert_date($date) ."+{$m_allow_repeat_weeks} WEEKS"))
                                    ?>" placeholder="Kuupäev" value="<?= $recur_date ?>">
                                <!-- <label id="rbs_booking_recur_date_lbl" for="rbs_booking_recur_date" data-rbs-label="Kuupäev"></label>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="border-bottom border-2 mb-3 px-3"><?php

rbs_print_m_calendar_booking_weekdays($no_tabs, $week_days, $repeat);
?>

                </div>
                <div class="form-floating mt-4 mb-3">
                    <input type="text" class="form-control" id="rbs_booking_title" name="rbs_booking_title" value="<?= $title ?>" placeholder="Sisesta pealkiri">
                    <label id="rbs_booking_title_lbl" for="rbs_booking_title" data-rbs-label="Sisesta pealkiri"></label>
                </div><?php

if ($m_is_admin):
?>

                <div class="row row-cols-2 ms-1">
                    <div class="col form-check form-switch pt-3">
                        <!-- <input class="form-check-input" type="checkbox" id="rbs_booking_lesson" name="rbs_booking_lesson" value="true" disabled>
                        <label class="form-check-label" for="rbs_booking_lesson">Tunniplaanijärgne tund</label> -->
                    </div>
                    <div class="col form-floating">
                        <select id="rbs_booking_teacher" name="rbs_booking_teacher" class="form-select">
                            <option value="0"<?= (!$m_sel_user ? ' selected' : '') ?>>- -</option><?php
rbs_print_m_calendar_booking_users(8, $m_sel_user);
?>

                        </select>
                        <label for="rbs_booking_teacher" data-rbs-label="Õpetaja / Broneerija"></label>
                    </div>
                </div><?php
endif;
?>

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveBooking()"<?=
                    // TODO: find better way to check availability of devices
                    (isset($_cnt_devices) && $_cnt_devices == $booked_devices ? ' disabled' : '')
                ?>><?= $m_strings['button'] ?></button><?php

if (!$m_new_booking):
?>

            <button type="button" class="btn btn-outline-danger" onclick="deleteBooking()">Kustuta</button><?php
endif;
?>

            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>