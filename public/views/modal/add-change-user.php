<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$user = rbs_m_get_user(true);

if (!$user['id']) {
    $_icon = 'person-plus';
    $_title = 'Uue kasutaja lisamine';
    $_next_input = ' data-rbs-next-input="rbs_password"';
}
else {
    $_icon = 'person-check';
    $_title = 'Muuda kasutaja andmed';
    $_next_input = '';
}

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $_icon, 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="user_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly><?php

if ($user['id']):
?>

                <input type="hidden" id="rbs_user_id" name="rbs_user_id" value="<?= $user['id'] ?>" readonly><?php
endif;
?>

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_firstname" name="rbs_firstname" value="<?=
                            $user['firstname'] ?>" placeholder="Eesnimi" data-rbs-next-input="rbs_lastname">
                    <label id="rbs_firstname_lbl" for="rbs_firstname" data-rbs-label="Eesnimi"></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_lastname" name="rbs_lastname" value="<?=
                            $user['lastname'] ?>" placeholder="Perenimi" data-rbs-next-input="rbs_username">
                    <label id="rbs_lastname_lbl" for="rbs_lastname" data-rbs-label="Perenimi"></label>
                </div>
                <hr>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_username" name="rbs_username" value="<?=
                            $user['username'] ?>" placeholder="kasutajanimi" data-rbs-next-input="rbs_email">
                    <label id="rbs_username_lbl" for="rbs_username" data-rbs-label="Kasutajanimi"></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="rbs_email" name="rbs_email" value="<?=
                            $user['email'] ?>" placeholder="nimi@mail.ee"<?= $_next_input ?>>
                    <label id="rbs_email_lbl" for="rbs_email" data-rbs-label="E-mail"></label>
                </div><?php
if (!$user['id']):
?>

                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="rbs_password" name="rbs_password" placeholder="Parool">
                    <label id="rbs_password_lbl" for="rbs_password" data-rbs-label="Parool"></label>
                </div><?php
endif;
?>

                <div class="form-floating">
                    <select class="form-select" id="rbs_role" name="rbs_role">
                        <option value="1"<?= ($user['role_id'] == 1 ? ' selected' : '') ?>>Admin</option>
                        <option value="2"<?= (!$user['id'] || $user['role_id'] == 2 ? ' selected' : '') ?>>Kasutaja</option>
                    </select>
                    <label for="rbs_role" data-rbs-label="Kasutajaroll"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="saveUser()">Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>