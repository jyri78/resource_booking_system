<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}


$user = rbs_m_get_user();
$_disable = (rbs_is_user_ok() ? '' : ' disabled');

?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'lock', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Parooli muutmine
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="change_password_form"><?php
if ($user['id']):
?>

                <input type="hidden" id="rbs_user_id" name="rbs_user_id" value="<?= $user['id'] ?>" readonly><?php
endif;
?>

                <div class="input-group input-group-sm mb-1 opacity-75">
                    <span class="input-group-text w-25 fw-bold">Kasutajanimi</span>
                    <input type="text" class="form-control" id="rbs_username" name="rbs_username" value="<?= $user['username'] ?>" readonly>
                </div>
                <div class="input-group input-group-sm mb-3 opacity-75">
                    <span class="input-group-text w-25 fw-bold">E-mail</span>
                    <input type="text" class="form-control" value="<?= $user['email'] ?>" readonly>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="rbs_password" name="rbs_password" placeholder="Parool" data-rbs-next-input="rbs_password_repeat"<?=
                            $_disable ?>>
                    <label id="rbs_password_lbl" for="rbs_password" data-rbs-label="Parool"></label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="rbs_password_repeat" name="rbs_password_repeat" placeholder="Parool uuesti"<?= $_disable ?>>
                    <label id="rbs_password_repeat_lbl" for="rbs_password_repeat" data-rbs-label="Parool uuesti"></label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="changePassword()"<?= $_disable ?>>Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>