<?php
// Copyright (C) 2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => 'question-circle', 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;Spikker
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="alert alert-warning">
                <?= rbs_get_icon(['name' => 'exclamation-triangle-fill', 'class' => 'opacity-50', 'size' => 28]) ?>
                &nbsp; Spikriinfo on alles loomisel
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Olgu</button>
        </div>
    </div>
</div>