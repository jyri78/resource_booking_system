<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


if (!defined('RBS_APP_VERSION')) {
    header('Location: ../../');
    exit;
}

$device = rbs_m_get_device_object();

if (!$device['id']) {
    $_icon = 'plus-square';
    $_title = 'Uue seadme(komplekti) lisamine';
    $_disabled = false;
}
else {
    $_icon = 'check2-square';
    $_title = 'Muuda seadet';
    $_disabled = true;
}

?> 
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div id="rbs_loader" class="position-absolute w-100 h-100 opacity-25 bg-secondary d-none" style="z-index:999">
            <div class="spinner-grow text-dark position-absolute" style="width:11rem; height:11rem; top:40%; left:35%" role="status">
                <span class="visually-hidden">Laadimine...</span>
            </div>
        </div>

        <div class="modal-header">
            <h5 class="modal-title user-select-none">
                <?= rbs_get_icon(['name' => $_icon, 'style' => 'color:rgba(0,0,0,.55)', 'size' => 28]) ?>

                &nbsp;<?= $_title ?>
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form id="device_add_form">
                <input type="hidden" id="rbs_show_table_ids" value="<?=
                        (rbs_get_setting('table_show_ids') ? '1' : '0') ?>" readonly><?php

if ($_disabled):
?>

                <input type="hidden" id="rbs_device_object_id" name="rbs_device_object_id" value="<?=
                        $device['id'] ?>" readonly><?php
else:
?>

                <input type="hidden" id="rbs" name="rbs" data-rbs-required-ids="rbs_device_type,rbs_device_manufacturer,rbs_device_model" readonly><?php
endif;
?>

                <input type="hidden" id="rbs__new_target" value="" readonly>

                <div class="row g-3 mb-4">
                    <div class="col-8 bg-light py-1 border-bottom d-none" id="rbs__new_input">
                        <div class="d-grid">
                            <input type="text" class="form-control form-control-sm" id="rbs__new_object">
                        </div>
                    </div>
                    <div class="col-4 bg-light py-1 border-bottom d-none" id="rbs__new_button">
                        <div class="d-grid">
                            <button type="button" class="btn btn-outline-primary btn-sm" onclick="addObject()">Salvesta</button>
                        </div>
                    </div>
                </div>

                <div class="input-group mb-4">
                    <span class="input-group-text">Komplekti nimetus</span>
                    <input type="text" id="rbs_device_name" name="rbs_device_name" class="form-control" value="<?=
                            $device['device_name'] ?>" data-rbs-next-input="<?=
                            ($_disabled ? 'rbs_device_location' : 'rbs_device_type') ?>"></input>

                    <div id="rbs_device_name_lbl" class="invalid-feedback"></div>
                </div>

				<div class="row g-<?= ($_disabled ? '2' : '4') ?> mb-4">
                    <div class="col-9">
                        <div class="form-object"><?php

$_cnt = rbs_print_m_device_types(
    'rbs_device_type', 'rbs_device_manufacturer',
    RBS_SELECT_OTPION_TITLE['device_type'], 7, $device['device_type_id']
);

$cnt_disabled = $_cnt;
?>

                        </div>
                    </div>
                    <div class="col-3">
                        <div class="d-grid">
                            <button type="button" class="btn btn-outline-primary rbs_add" id="rbs_device_type_add" tabindex="<?=
                                    (-1 + $_cnt) ?>" data-rbs-object-name="seadme tüübi" data-rbs-task="device_type" data-rbs-select-id="rbs_device_type"<?=
                                    (!$device['device_type_id'] ? '' : ' disabled') ?>>
                                Lisa &nbsp;<?=
                                        rbs_get_icon(['name' => 'usb-plug', 'size' => 12]) ?>

                            </button>
                        </div>
                    </div>

                    <div class="col-9">
                        <div class="form-object"><?php

$_cnt = rbs_print_m_device_manufacturers(
    'rbs_device_manufacturer', 'rbs_device_model',
    RBS_SELECT_OTPION_TITLE['manufacturer'], 7, $device['device_manufacturer_id']
);

$cnt_disabled += $_cnt;
?>

                        </div>
                    </div>
                    <div class="col-3">
                        <div class="d-grid">
                            <button type="button" class="btn btn-outline-primary rbs_add" id="rbs_device_manufacturer_add" tabindex="<?=
                                    (-1 + $_cnt) ?>" data-rbs-object-name="seadme tootja" data-rbs-task="manufacturer" data-rbs-select-id="rbs_device_manufacturer"<?=
                                    (!$device['device_manufacturer_id'] ? '' : ' disabled') ?>>
                                Lisa &nbsp;<?=
                                        rbs_get_icon(['name' => 'box2', 'size' => 12]) ?>

                            </button>
                        </div>
                    </div>

                    <div class="col-9">
                        <div class="form-object"><?php

$_cnt = rbs_print_m_device_models(
    'rbs_device_model', 'rbs_device_location',
    RBS_SELECT_OTPION_TITLE['device_model'], 7, $device['device_model_id']
);

$cnt_disabled += $_cnt;
?>

                        </div>
                    </div>
                    <div class="col-3">
                        <div class="d-grid">
                            <button type="button" class="btn btn-outline-primary rbs_add" id="rbs_device_model_add" tabindex="<?=
                                    (-1 + $_cnt) ?>" data-rbs-object-name="seadme mudeli" data-rbs-task="model" data-rbs-select-id="rbs_device_model"<?=
                                    (!$device['device_model_id'] ? '' : ' disabled') ?>>
                                Lisa &nbsp;<?=
                                        rbs_get_icon(['name' => 'tag', 'size' => 12]) ?>

                            </button>
                        </div>
                    </div>

                    <div class="col-9">
                        <div class="form-object"><?php

$_cnt = rbs_print_m_device_locations(
    'rbs_device_location', 'rbs_devices_count',
    RBS_SELECT_OTPION_TITLE['device_location'], 7, $device['room_id']
);

$cnt_disabled += $_cnt;
?>

                        </div>
                    </div>
                    <div class="col-3">
                        <div class="d-grid">
                            <button type="button" class="btn btn-outline-primary rbs_add" id="rbs_device_location_add" tabindex="<?=
                                    (-1 + $_cnt) ?>" data-rbs-object-name="seadme asukoha" data-rbs-task="new_room" data-rbs-select-id="rbs_device_location">
                                Lisa &nbsp;<?= rbs_get_icon(['name' => 'door-open', 'size' => 12]) ?>

                            </button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-9">
                        <div class="input-group mb-4">
                            <span class="input-group-text">Kogus</span>
                            <input type="number" id="rbs_devices_count" name="rbs_devices_count" class="form-control" min="1" value="<?=
                                    $device['objects_count'] ?>" data-rbs-next-input="rbs_info" />
                        </div>
                    </div>

                    <div class="col-3">&nbsp;</div>
                </div>

                <div class="input-group mb-4">
                    <span class="input-group-text">Lisainfo</span>
                    <textarea id="rbs_info" name="rbs_info" class="form-control" data-rbs-not-required="1" data-rbs-next-input="rbs_image"></textarea>
                </div>

                <div class="input-group">
                    <label for="rbs_image" class="input-group-text fw-bold" role="button" data-rbs-label="Pilt"></label>
                    <input type="file" accept=".jpg,.jpeg,.png,.gif,.webp" id="rbs_image" name="rbs_image" class="form-control form-control-file">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary<?= (!$cnt_disabled ? '' : ' opacity-25')
                ?>" id="rbs_save_button" onclick="saveDeviceObject()"<?= (!$cnt_disabled ? '' : ' disabled') ?>>Salvesta</button>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Sulge</button>
        </div>
    </div>
</div>