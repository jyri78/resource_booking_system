<?php
// Copyright (C) 2021-2022 Jüri Kormik
// 
// This software is released under the GNU v3 License.
// https://bitbucket.org/jyri78/resource_booking_system/src/master/LICENCE


include_once 'functions/_inc.php';

if (!($rbs_settings ?? false)) {
    header("{$_SERVER['SERVER_PROTOCOL']} 500 Internal Server Error");
    exit;
}


// If filter not selected yet, set default value
if (!isset($_SESSION['CALENDAR_FILTER_OBJECT_TYPE_ID'])) rbs_default_filter();


/* ============================================================================
 *  Process requests (GET, POST)
 * ============================================================================
 */

/// JavaScript file (depends on opened page)
if (isset($_GET['js'])) {
    $parts = explode('_', $_GET['js']);

    if ($parts[0] == RBS_JS_VERSION && array_key_exists($parts[1], RBS_JS_PAGES)) {
        $rbs_js_page = RBS_JS_PAGES[$parts[1]];
        require_once(RBS_CORE_DIR .'lib/js.php');
        exit;
    }
}

/// CSS file
elseif (isset($_GET['css']) && $_GET['css'] == RBS_CSS_VERSION) {
    require_once RBS_CORE_DIR .'lib/css.php';
    exit;
}

/// AJAX (post) requests (key is set in file config.ini)
elseif (isset($_POST['rbs']) && $_POST['rbs'] == RBS_AJAX_KEY) {  //! checks AJAX key correctness
    if (isset($_POST['task'])) {                                  //! "task" is mandatory
        switch(filter_var($_POST['task'])) {
            /*
             *  User actions (log in and out)
             */
            case 'open_login_modal':
                include RBS_VIEW_PATH .'modal/log-in.php';
                break;

            case 'open_about_modal':
                include RBS_VIEW_PATH .'modal/about.php';
                break;

            case 'open_help_modal':
                include RBS_VIEW_PATH .'modal/help.php';
                break;

            case 'user_login':
                header('Content-Type: application/json');
                echo json_encode( rbs__user_login(trim($_POST['rbs_user']), trim($_POST['rbs_password'])) );
                break;

            case 'open_change_password_modal':
                include RBS_VIEW_PATH .'modal/change-password.php';
                break;

            case 'user_change_password':
                header('Content-Type: application/json');
                echo json_encode(
                    rbs__user_change_password(
                        $_POST['rbs_user_id'], trim($_POST['rbs_password']), trim($_POST['rbs_password_repeat'])
                    )
                );
                break;

            case 'user_logout':
                $render_view->logOut();
                echo '';  // pole vaja midagi tagastada
                break;

            /*
             *  Calendar requests and actions
             */
            case 'open_calendar_filter_modal':
                include RBS_VIEW_PATH .'modal/calendar-filter.php';
                break;

            case 'calendar_filter':
                rbs__calendar_filter($_POST);
                echo '';
                break;

            case 'calendar_filter_get_objects':
                echo rbs__calendar_filter_get_objects($_POST['type_id']);
                break;

            case 'get_calendar_options':
                $settings = ['logged_in' => $rbs_settings['user']['logged_in']];
                if (isset($_SESSION['RBS_CALENDAR_GOTO_DATE'])) {
                    $settings['goto_date'] = $_SESSION['RBS_CALENDAR_GOTO_DATE'];
                    unset($_SESSION['RBS_CALENDAR_GOTO_DATE']);  // Go to the date only once
                }

                header('Content-Type: application/json');
                include RBS_VIEW_PATH .'.calendar-options.php';

                if (!isset($options)) {
                    // header("{$_SERVER['SERVER_PROTOCOL']} 500 Internal Server Error");
                    echo json_encode(['result' => false, 'error' => 'UPS... Kalendri laadimisel läks midagi viltu.']);
                }
                else echo json_encode( compact('settings', 'options') );
                break;

            case 'calendar_bookings_source':
                header('Content-Type: application/json');
                echo json_encode( rbs__calendar_bookings_source($_POST['start'], $_POST['end']) );
                break;

            case 'open_calendar_booking_modal':
                if (rbs_is_logged_in(false, true)) include RBS_VIEW_PATH .'modal/calendar-booking.php';
                break;

            case 'get_calendar_booked_times':
                header('Content-Type: application/json');
                if (rbs_is_logged_in(true))
                    echo json_encode( rbs__get_calendar_booked_times($_POST['date'], $_POST['object_id']) );
                break;

            case 'save_calendar_booking':
                header('Content-Type: application/json');
                if (rbs_is_logged_in(true)) echo json_encode( rbs__save_calendar_booking($_POST) );
                break;

            case 'open_calendar_change_modal':
                if (rbs_is_logged_in(false, true)) include RBS_VIEW_PATH .'modal/calendar-booking.php';
                break;

            case 'delete_calendar_booking':
                header('Content-Type: application/json');
                if (rbs_is_logged_in(true)) echo json_encode( rbs__delete_calendar_booking($_POST['booking_id']) );
                break;

            case 'filter_device_table':
                if (rbs_is_logged_in()) rbs_set_device_filter($_POST);
                break;


            /*
             *  Administrative (rooms, devices, site settings) actions
             */
            /// Rooms page
            case 'open_new_room_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-room.php';
                break;

            case 'save_new_room':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_new_room($_POST) );
                break;

            case 'save_room_change':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_room_change($_POST) );
                break;

            case 'delete_room':
                if (rbs_is_admin(false, true)) rbs__delete_room($_POST['rbs_room_id']);
                break;

            /// Devices page
            case 'open_add_change_device_object_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-device-object.php';
                break;

            case 'open_add_change_device_in_group_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-device-in-group.php';
                break;

            case 'view_image_modal':
                if (rbs_is_logged_in(true)) include RBS_VIEW_PATH .'modal/view-image.php';
                break;

            case 'open_add_change_device_type_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-device-type.php';
                break;

            case 'save_device_type':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_device_type($_POST) );
                break;

            case 'delete_device_type':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__delete_device_type($_POST['rbs_device_type_id']) );
                break;

            case 'open_add_change_manufacturer_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-manufacturer.php';
                break;

            case 'save_manufacturer':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_manufacturer($_POST) );
                break;

            case 'delete_manufacturer':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__delete_manufacturer($_POST['rbs_manufacturer_id']) );
                break;

            case 'open_add_change_model_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-model.php';
                break;

            case 'save_model':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_model($_POST) );
                break;

            case 'delete_model':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__delete_model($_POST['rbs_model_id']) );
                break;

            case 'save_image':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_image() );
                break;

            case 'save_device_object':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_device_object($_POST) );
                break;

            case 'get_device_data':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__get_device_data($_POST['device_id']) );
                break;

            case 'save_device':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_device($_POST) );
                break;

            case 'delete_device':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__delete_device($_POST) );
                break;


            /// Settings page
            case 'change_navbar_theme':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs_write_setting('view_navbar', $_POST['view_navbar']) );
                break;

            case 'change_ids_visibility':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs_write_setting('table_show_ids', $_POST['table_show_ids']) );
                break;

            case 'save_settings_lesson_times':
                header('Content-Type: application/json');
                if (rbs_is_admin(true)) echo json_encode( rbs__save_lesson_times($_POST) );
                break;

            case 'save_settings_calendar_options':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__save_settings_calendar_options($_POST) );
                break;

            case 'save_settings_booking_options':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__save_settings_booking_options($_POST) );
                break;

            case 'get_header_brand_style':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( ['style' => rbs__get_header_brand_style($_POST)] );
                break;

            case 'save_settings_header_brand':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__save_settings_header_brand($_POST) );
                break;

            /// Users page
            case 'open_add_change_user_modal':
                if (rbs_is_admin(false, true)) include RBS_VIEW_PATH .'modal/add-change-user.php';
                break;

            case 'save_user':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__save_user($_POST) );
                break;

            case 'delete_user':
                header('Content-Type: application/json');
                if (rbs_is_admin(true))
                    echo json_encode( rbs__delete_user($_POST['rbs_user_id']) );
                break;

            default:
                header("{$_SERVER['SERVER_PROTOCOL']} 400 Bad Request");
        }
    }
    else header("{$_SERVER['SERVER_PROTOCOL']} 400 Bad Request");

    exit;  //! all POST requests end here
}

/// Rooms info/settings page
elseif (isset($_GET['ruumid']) && rbs_is_user_ok()) {
    $rbs_settings['page']['name'] = 'rooms';
    $rbs_settings['page']['title'] = 'Ruumid';
    $rbs_settings['page']['js_ext'] = 'r';
	$rbs_rooms = rbs__get_rooms();
}

/// Devices info/settings page
elseif (isset($_GET['seadmed']) && rbs_is_user_ok()) {
    $rbs_settings['page']['name'] = 'devices';
    $rbs_settings['page']['title'] = 'Seadmed';
    $rbs_settings['page']['js_ext'] = 'd';
    // $rbs_devices = $render_view->getDevices();
}

/// Device types page
elseif (isset($_GET['seadmete_tyybid']) && rbs_is_admin()) {
    $rbs_settings['page']['name'] = 'device_types';
    $rbs_settings['page']['title'] = 'Seadmete tüübid';
    $rbs_settings['page']['js_ext'] = 't';
}

/// Manufacturers page
elseif (isset($_GET['tootjad']) && rbs_is_admin()) {
    $rbs_settings['page']['name'] = 'manufacturers';
    $rbs_settings['page']['title'] = 'Tootjad';
    $rbs_settings['page']['js_ext'] = 'm';
}

/// Models page
elseif (isset($_GET['mudelid']) && rbs_is_admin()) {
    $rbs_settings['page']['name'] = 'models';
    $rbs_settings['page']['title'] = 'Mudelid';
    $rbs_settings['page']['js_ext'] = 'o';
}

/// Settings page
elseif (isset($_GET['saidi_seaded']) && rbs_is_admin())
{
    $rbs_settings['page']['name'] = 'settings';
    $rbs_settings['page']['title'] = 'Saidi seaded';
    $rbs_settings['page']['js_ext'] = 's';
}

/// Users settings page
elseif (isset($_GET['kasutajad']) && rbs_is_admin())
{
    $rbs_settings['page']['name'] = 'users';
    $rbs_settings['page']['title'] = 'Kasutajad';
    $rbs_settings['page']['js_ext'] = 'u';
    $rbs_users = $render_view->getAllUsers()['result'];
}

/// Finally load image if asked or reset to home page
elseif (count($_REQUEST)) {
    $file = [];

    if (rbs_is_user_ok()) {
        foreach ($_GET as $key => $_) {
            $_k = explode('_', $key);

            if (count($_k) == 2 && filter_var($_k[1], FILTER_VALIDATE_INT)) {
                if ($_k[0] == 'thumb')
                    $file = [$_k[1], '_thumb'];
                elseif ($_k[0] == 'img')
                    $file = [$_k[1], ''];
                elseif (rbs_is_admin() && $_k[0] == 'preview')
                    $file = [$_k[1], '_PREV_'];
            }
        }
    }

    if (!count($file)) {
        $url = RBS_WEB_URL;
        header("Location: $url");
    }
    elseif ($file[1] == '_PREV_') rbs__output_image_preview($file[0]);
    else rbs_output_image($file);

    exit;
}